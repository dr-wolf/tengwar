unit uDict;

interface

uses
  uArray, uStringSet;

type
  TDict<T> = class(TObject)
  public
    constructor Create;
    destructor Destroy; override;
  private
    FItems: TArray<T>;
    FKeys: TStringSet;
    FIndex: Integer;
    function GetItem(I: string): T;
    function GetKey: string;
    function GetCount: Integer;
  public
    property Item[I: string]: T read GetItem; default;
    property Count: Integer read GetCount;
    property Key: string read GetKey;
    function Contains(I: string): Boolean;
    function IndexOf(I: string): Integer;
    function Next: Boolean;
    procedure Put(I: string; Value: T);
    procedure Delete(I: string);
    procedure Reset;
    procedure Clear;
  end;

implementation

{ TDict<T> }

constructor TDict<T>.Create;
begin
  FKeys := TStringSet.Create;
  Clear;
end;

destructor TDict<T>.Destroy;
begin
  FItems := nil;
  FKeys.Free;
  inherited;
end;

procedure TDict<T>.Clear;
begin
  FKeys.Clear;
  SetLength(FItems, 0);
end;

function TDict<T>.Contains(I: string): Boolean;
begin
  Result := IndexOf(I) >= 0;
end;

procedure TDict<T>.Delete(I: string);
var
  n: Integer;
begin
  n := FKeys.IndexOf(I);
  if n < 0 then
    Exit;
  while n < High(FItems) do
  begin
    FItems[n] := FItems[n + 1];
    Inc(n);
  end;
  SetLength(FItems, Length(FItems) - 1);
  FKeys.Delete(I);
end;

function TDict<T>.GetCount: Integer;
begin
  Result := FKeys.Count;
end;

function TDict<T>.GetItem(I: string): T;
begin
  Result := FItems[FKeys.IndexOf(I)];
end;

function TDict<T>.GetKey: string;
begin
  Result := FKeys[FIndex];
end;

function TDict<T>.IndexOf(I: string): Integer;
begin
  Result := FKeys.IndexOf(I);
end;

function TDict<T>.Next: Boolean;
begin
  Result := FIndex < FKeys.Count - 1;
  if Result then
    Inc(FIndex);
end;

procedure TDict<T>.Put(I: string; Value: T);
var
  p: Integer;
begin
  p := FKeys.IndexOf(I);
  if p < 0 then
  begin
    FKeys.Add(I);
    SetLength(FItems, Length(FItems) + 1);
    FItems[High(FItems)] := Value;
  end
  else
    FItems[p] := Value;
end;

procedure TDict<T>.Reset;
begin
  FIndex := -1;
end;

end.
