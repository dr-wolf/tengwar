unit uStack;

interface

uses
  uArray;

type
  TStack<T> = class(TObject)
  public
    constructor Create;
    destructor Destroy; override;
  private
    FItems: TArray<T>;
    FIndex: TArray<Integer>;
    function GetLayer: Integer;
    function GetIsEmpty: Boolean;
    procedure Normalize;
  public
    property IsEmpty: Boolean read GetIsEmpty;
    property Layer: Integer read GetLayer;
    function Peek: T;
    function Pop: T;
    function AddLayer: Integer;
    procedure Push(Value: T);
    procedure Clear(Layer: Integer);
  end;

implementation

{ TStack<T> }

constructor TStack<T>.Create;
begin
  SetLength(FIndex, 0);
  AddLayer;
  Normalize;
end;

destructor TStack<T>.Destroy;
begin
  FItems := nil;
  FIndex := nil;
  inherited;
end;

function TStack<T>.AddLayer: Integer;
begin
  SetLength(FIndex, Length(FIndex) + 1);
  if Length(FIndex) > 1 then
    FIndex[High(FIndex)] := FIndex[High(FIndex) - 1]
  else
    FIndex[High(FIndex)] := -1;
  Result := High(FIndex);
end;

procedure TStack<T>.Clear(Layer: Integer);
begin
  if Layer < Length(FIndex) then
    SetLength(FIndex, Layer);
  Normalize;
end;

function TStack<T>.GetIsEmpty: Boolean;
begin
  case Length(FIndex) of
    0:
      Result := False;
    1:
      Result := FIndex[High(FIndex)] < 0;
  else
    Result := FIndex[High(FIndex)] = FIndex[High(FIndex) - 1];
  end;
end;

function TStack<T>.GetLayer: Integer;
begin
  Result := High(FIndex);
end;

procedure TStack<T>.Normalize;
const
  BLOCK = 64;
var
  s: Integer;
begin
  if Length(FIndex) > 0 then
    s := FIndex[High(FIndex)]
  else
    s := 0;
  SetLength(FItems, (s div BLOCK + 1) * BLOCK);
end;

function TStack<T>.Peek: T;
begin
  Result := FItems[FIndex[High(FIndex)]];
end;

function TStack<T>.Pop: T;
begin
  if not GetIsEmpty then
  begin
    Result := FItems[FIndex[High(FIndex)]];
    Dec(FIndex[High(FIndex)]);
  end
  else
    Result := T(nil);
  Normalize;
end;

procedure TStack<T>.Push(Value: T);
begin
  Inc(FIndex[High(FIndex)]);
  Normalize;
  FItems[FIndex[High(FIndex)]] := Value;
end;

end.
