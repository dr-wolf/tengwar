unit uSourceParser;

interface

uses
  Classes, uArray;

type
  TKeyword = (kwAs, kwBind, kwBreak, kwCatch, kwConst, kwContinue, kwDo, kwEach, kwElse, kwEnd, kwExit, kwFunc, kwIf,
    kwIn, kwList, kwLoad, kwLog, kwOut, kwPipe, kwRun, kwSet, kwStruct, kwSync, kwThrow, kwTry, kwType, kwUse, kwVar,
    kwWhile, kwWith, kwError);

  TSymbol = (sPlus, sMinus, sStar, sSlash, sPerc, sPow, sAt, sExcl, sQuest, sTilde, sAmp, sPipe, sFunc, sEq, sLt, sGt,
    sComma, sColon, sSemicolon, sDot, sBrOp, sBrCl, sListBgn, sListEnd, sStructBgn, sStructEnd, sSetBgn, sSetEnd,
    sArrow, sSet, sNotEq, sLtEq, sGtEq, sNameSp, sErr);

  TTokenKind = (tkNone, tkIdentifier, tkKeyword, tkInteger, tkFloat, tkString, tkSymbol);

  TSymbolSet = set of TSymbol;

  TChars = set of Char;

  TCharSet = record
    Chars: TChars;
    ExceptGiven: Boolean;
  end;

  TToken = record
    Value: string;
    Kind: TTokenKind;
    Index: Integer;
  end;

  TRuleMarker = record
    State: string;
    CharSet: TCharSet;
  end;

  TRuleAction = record
    State: string;
    Sync: Boolean;
    Token: TTokenKind;
  end;

  TRule = record
    Marker: TRuleMarker;
    Action: TRuleAction;
  end;

  TPostProcessor = procedure(var Tokens: TArray<TToken>);

const
  CS_DIGITS = ['0' .. '9'];
  CS_HEX = ['0' .. '9', 'A' .. 'F', 'a' .. 'f'];
  CS_LETTERS = ['A' .. 'Z', 'a' .. 'z', '_'];
  CS_SYMBOLS = ['+', '-', '*', '/', '^', '%', '@', '!', '?', '''', '~', '&', '|', '$', '=', '<', '>', ',', ':', ';', '.',
    '[', ']', '(', ')', '{', '}'];
  CS_WHITE = [' ', #9, #13, #10];

  CS_STRING = ['"'];
  CS_ESCAPE = ['\'];
  CS_COMMENT = ['#'];
  CS_DECIMAL = ['.'];

type
  TTokenStream = class(TObject)
  public
    constructor Create;
    destructor Destroy; override;
  private
    FTokens: TArray<TToken>;
    FCursor: Integer;
  public
    function Empty: Boolean;
    function Pop: TToken;
    function Token(Shift: Integer = 0): TToken;
    function Skip: TTokenStream;
    function Fetch(Kind: TTokenKind): TToken;
    procedure Test(Keyword: TKeyword; Error: Integer); overload;
    procedure Test(Symbol: TSymbol; Error: Integer); overload;
    procedure Add(Token: TToken);
  end;

  TTokenizer = class(TObject)
  public
    constructor Create;
    destructor Destroy; override;
  private
    FRules: TArray<TRule>;
    FPostProcessors: TArray<TPostProcessor>;
  public
    procedure Process(Source: TStrings; Tokens: TTokenStream);
    procedure AddRule(State: string; CharSet: TCharSet; Sync: Boolean; NewState: string = '';
      Token: TTokenKind = tkNone);
    procedure RegisterPostProcessor(PostProcessor: TPostProcessor);
  end;

function Chars(Chars: TChars): TCharSet;
function CharsExcept(Chars: TChars): TCharSet;
function ToKword(Identifier: string): TKeyword;
function ToSmbl(Identifier: string): TSymbol;
function Eq(Token: TToken; Keyword: TKeyword): Boolean; overload;
function Eq(Token: TToken; Symbol: TSymbol): Boolean; overload;
function Eq(Token: TToken; Identifier: string): Boolean; overload;

implementation

uses SysUtils, uExceptions;

function Chars(Chars: TChars): TCharSet;
begin
  Result.Chars := Chars;
  Result.ExceptGiven := False;
end;

function CharsExcept(Chars: TChars): TCharSet;
begin
  Result.Chars := Chars;
  Result.ExceptGiven := True;
end;

function Token(Value: string; Kind: TTokenKind; Index: Integer): TToken;
begin
  Result.Value := Value;
  Result.Kind := Kind;
  Result.Index := Index;
end;

function ToKword(Identifier: string): TKeyword;
const
  L_KEYWORDS: array [TKeyword] of string = ('as', 'bind', 'break', 'catch', 'const', 'continue', 'do', 'each', 'else',
    'end', 'exit', 'func', 'if', 'in', 'list', 'load', 'log', 'out', 'pipe', 'run', 'set', 'struct', 'sync', 'throw',
    'try', 'type', 'use', 'var', 'while', 'with', '');
begin
  Result := Low(TKeyword);
  while (Result <> High(TKeyword)) and (Identifier <> L_KEYWORDS[Result]) do
    Result := Succ(Result);
end;

function ToSmbl(Identifier: string): TSymbol;
const
  L_SYMBOLS: array [TSymbol] of string = ('+', '-', '*', '/', '%', '^', '@', '!', '?', '~', '&', '|', '$', '=', '<',
    '>', ',', ':', ';', '.', '(', ')', '[', ']', '{', '}', '[:', ':]', '->', ':=', '<>', '<=', '>=', '::', '');
begin
  Result := Low(TSymbol);
  while (Result <> High(TSymbol)) and (Identifier <> L_SYMBOLS[Result]) do
    Result := Succ(Result);
end;

function Eq(Token: TToken; Keyword: TKeyword): Boolean; overload;
begin
  Result := (Token.Kind = tkKeyword) and (ToKword(Token.Value) = Keyword);
end;

function Eq(Token: TToken; Symbol: TSymbol): Boolean; overload;
begin
  Result := (Token.Kind = tkSymbol) and (ToSmbl(Token.Value) = Symbol);
end;

function Eq(Token: TToken; Identifier: string): Boolean; overload;
begin
  Result := (Token.Kind = tkIdentifier) and (Token.Value = Identifier);
end;

{ TTokenStream }

procedure TTokenStream.Add(Token: TToken);
begin
  SetLength(FTokens, Length(FTokens) + 1);
  FTokens[High(FTokens)] := Token;
end;

constructor TTokenStream.Create;
begin
  SetLength(FTokens, 0);
  FCursor := 0;
end;

destructor TTokenStream.Destroy;
begin
  FTokens := nil;
  inherited;
end;

function TTokenStream.Empty: Boolean;
begin
  Result := FCursor > High(FTokens);
end;

function TTokenStream.Fetch(Kind: TTokenKind): TToken;
begin
  Result := FTokens[FCursor];
  Skip;
  if Result.Kind <> Kind then
    case Kind of
      tkIdentifier:
        raise ECodeException.Create(E_EXPECTED_IDENTIFIER, '', Result.Index);
      tkString:
        raise ECodeException.Create(E_EXPECTED_STRING, '', Result.Index);
    else
      raise ECodeException.Create(E_SYNTAX_ERROR, '', Result.Index);
    end;
end;

function TTokenStream.Pop: TToken;
begin
  if not Empty then
  begin
    Result := FTokens[FCursor];
    Inc(FCursor);
  end
  else
    raise ECodeException.Create(E_UNEXPECTED_END, '', FTokens[High(FTokens)].Index);
end;

function TTokenStream.Skip: TTokenStream;
begin
  if not Empty then
    Inc(FCursor)
  else
    raise ECodeException.Create(E_UNEXPECTED_END, '', FTokens[High(FTokens)].Index);
  Result := Self;
end;

procedure TTokenStream.Test(Keyword: TKeyword; Error: Integer);
begin
  if not Eq(FTokens[FCursor], Keyword) then
    raise ECodeException.Create(Error, '', FTokens[FCursor].Index);
  Skip;
end;

procedure TTokenStream.Test(Symbol: TSymbol; Error: Integer);
begin
  if not Eq(FTokens[FCursor], Symbol) then
    raise ECodeException.Create(Error, '', FTokens[FCursor].Index);
  Skip;
end;

function TTokenStream.Token(Shift: Integer): TToken;
begin
  if FCursor + Shift <= High(FTokens) then
    Result := FTokens[FCursor + Shift]
  else
    raise ECodeException.Create(E_UNEXPECTED_END, '', FTokens[High(FTokens)].Index);
end;

{ TTokenizer }

procedure TTokenizer.AddRule(State: string; CharSet: TCharSet; Sync: Boolean; NewState: string; Token: TTokenKind);
begin
  SetLength(FRules, Length(FRules) + 1);
  FRules[High(FRules)].Marker.State := State;
  FRules[High(FRules)].Marker.CharSet := CharSet;
  if NewState = '' then
    FRules[High(FRules)].Action.State := State
  else
    FRules[High(FRules)].Action.State := NewState;
  FRules[High(FRules)].Action.Sync := Sync;
  FRules[High(FRules)].Action.Token := Token;
end;

constructor TTokenizer.Create;
begin
  SetLength(FRules, 0);
end;

destructor TTokenizer.Destroy;
begin
  FRules := nil;
  FPostProcessors := nil;
  inherited;
end;

procedure TTokenizer.Process(Source: TStrings; Tokens: TTokenStream);

  function Match(State: string; Symbol: Char; Marker: TRuleMarker): Boolean;
  begin
    Result := (Marker.State = State) and (CharInSet(Symbol, Marker.CharSet.Chars) xor Marker.CharSet.ExceptGiven)
  end;

var
  State, line: string;
  i, LineIndex, head, tail: Integer;
begin
  State := 'init';
  for LineIndex := 0 to Source.Count - 1 do
  begin
    head := 1;
    tail := 0;

    line := Source[LineIndex] + #13;
    while head <= Length(line) do
    begin
      i := 0;
      while (i <= High(FRules)) and not Match(State, line[head], FRules[i].Marker) do
        Inc(i);
      if i > High(FRules) then
        raise ECodeException.Create(E_UNEXPECTED_CHARACTER, line[head], LineIndex + 1);

      State := FRules[i].Action.State;
      if FRules[i].Action.Sync then
        tail := head;
      if FRules[i].Action.Token <> tkNone then
        Tokens.Add(Token(Copy(line, tail, head - tail), FRules[i].Action.Token, LineIndex + 1))
      else
        Inc(head);
    end;

    if State = 'string' then
      raise ECodeException.Create(E_UNTERMINATED_STRING, '', LineIndex + 1);
  end;
  for i := 0 to High(FPostProcessors) do
    FPostProcessors[i](Tokens.FTokens);
end;

procedure TTokenizer.RegisterPostProcessor(PostProcessor: TPostProcessor);
begin
  SetLength(FPostProcessors, Length(FPostProcessors) + 1);
  FPostProcessors[High(FPostProcessors)] := PostProcessor;
end;

end.
