unit uParserLogic;

interface

uses
  uSourceParser;

procedure FillRuleset(Tokenizer: TTokenizer);

implementation

uses SysUtils, uArray;

procedure MatchKeywords(var Tokens: TArray<TToken>);
var
  i: Integer;
begin
  for i := 0 to High(Tokens) do
    if (Tokens[i].Kind = tkIdentifier) and (ToKword(Tokens[i].Value) <> kwError) then
      Tokens[i].Kind := tkKeyword;
end;

procedure StripStrings(var Tokens: TArray<TToken>);
var
  i, n: Integer;
  s: string;
begin
  for i := 0 to High(Tokens) do
    if Tokens[i].Kind = tkString then
    begin
      s := '';
      n := 2;
      while n <= Length(Tokens[i].Value) - 1 do
      begin
        if Tokens[i].Value[n] = '\' then
        begin
          Inc(n);
          case Tokens[i].Value[n] of
            'r':
              s := s + #$D;
            'n':
              s := s + #$A;
            't':
              s := s + #$9;
          else
            s := s + Tokens[i].Value[n];
          end;
        end
        else
          s := s + Tokens[i].Value[n];
        Inc(n);
      end;
      Tokens[i].Value := s;
    end;
end;

procedure LowercaseIdentifiers(var Tokens: TArray<TToken>);
var
  i: Integer;
begin
  for i := 0 to High(Tokens) do
    if Tokens[i].Kind = tkIdentifier then
      Tokens[i].Value := LowerCase(Tokens[i].Value);
end;

procedure CombineSymbols(var Tokens: TArray<TToken>);
var
  i, j: Integer;
begin
  i := 0;
  while i < High(Tokens) do
  begin
    if (Tokens[i].Kind = tkSymbol) and (Tokens[i + 1].Kind = tkSymbol) and
      (ToSmbl(Tokens[i].Value + Tokens[i + 1].Value) in [sSetBgn, sSetEnd, sArrow, sSet, sNotEq, sLtEq,
      sGtEq, sNameSp]) then
    begin
      Tokens[i].Value := Tokens[i].Value + Tokens[i + 1].Value;
      for j := i + 1 to High(Tokens) - 1 do
        Tokens[j] := Tokens[j + 1];
      SetLength(Tokens, Length(Tokens) - 1);
    end;
    Inc(i);
  end;
end;

procedure FillRuleset(Tokenizer: TTokenizer);
begin
  with Tokenizer do
  begin
    AddRule('init', Chars(CS_WHITE), True);

    AddRule('init', Chars(CS_LETTERS), True, 'name');
    AddRule('name', Chars(CS_LETTERS + CS_DIGITS), False);
    AddRule('name', CharsExcept(CS_LETTERS + CS_DIGITS), False, 'init', tkIdentifier);

    AddRule('init', Chars(CS_SYMBOLS), True, 'symbol');
    AddRule('symbol', CharsExcept([]), False, 'init', tkSymbol);

    AddRule('init', Chars(CS_STRING), True, 'string');
    AddRule('string', Chars(CS_ESCAPE), False, 'string_escape');
    AddRule('string_escape', CharsExcept([]), False, 'string');
    AddRule('string', CharsExcept(CS_STRING), False);
    AddRule('string', Chars(CS_STRING), False, 'string_end');
    AddRule('string_end', CharsExcept(CS_STRING), False, 'init', tkString);

    AddRule('init', Chars(['0']), True, 'hex');
    AddRule('hex', Chars(CS_DIGITS), False, 'int');
    AddRule('hex', Chars(CS_DECIMAL), False, 'float');
    AddRule('hex', Chars(CS_WHITE + CS_SYMBOLS - CS_DECIMAL), False, 'init', tkInteger);
    AddRule('hex', Chars(['x']), False, 'hex_dig');
    AddRule('hex_dig', Chars(CS_HEX), False);
    AddRule('hex_dig', Chars(CS_WHITE + CS_SYMBOLS), False, 'init', tkInteger);

    AddRule('init', Chars(CS_DIGITS), True, 'int');
    AddRule('int', Chars(CS_DIGITS), False);
    AddRule('int', Chars(CS_DECIMAL), False, 'float');
    AddRule('int', Chars(CS_WHITE + CS_SYMBOLS - CS_DECIMAL), False, 'init', tkInteger);
    AddRule('float', Chars(CS_DIGITS), False);
    AddRule('float', Chars(CS_WHITE + CS_SYMBOLS), False, 'init', tkFloat);

    AddRule('init', Chars(CS_COMMENT), True, 'comment');
    AddRule('comment', Chars([#13, #10]), True, 'init');
    AddRule('comment', Chars(CS_COMMENT), False, 'comment_block');
    AddRule('comment', CharsExcept([#13, #10] + CS_COMMENT), False, 'comment_line');

    AddRule('comment_line', CharsExcept([#13, #10]), True);
    AddRule('comment_line', Chars([#13, #10]), True, 'init');

    AddRule('comment_block', CharsExcept(CS_COMMENT), True);
    AddRule('comment_block', Chars(CS_COMMENT), False, 'comment_block_end');
    AddRule('comment_block_end', Chars(CS_COMMENT), True, 'init');
    AddRule('comment_block_end', CharsExcept(CS_COMMENT), False, 'comment_block');

    RegisterPostProcessor(LowercaseIdentifiers);
    RegisterPostProcessor(MatchKeywords);
    RegisterPostProcessor(StripStrings);
    RegisterPostProcessor(CombineSymbols);
  end;
end;

end.
