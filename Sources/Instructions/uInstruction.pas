unit uInstruction;

interface

uses
  uArray, uOp;

const
  CC_NEXT = 0;
  CC_BREAK = 1;
  CC_CONTINUE = 2;
  CC_EXIT = 4;

type
  TInstruction = class(TObject)
  public
    function Compile: TArray<TOp>; virtual; abstract;
  end;

implementation

{ TInstruction }

end.
