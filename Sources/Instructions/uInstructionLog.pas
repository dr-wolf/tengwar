unit uInstructionLog;

interface

uses
  uArray, uInstruction, uExpression, uOp;

type
  TInstructionLog = class(TInstruction)
  public
    constructor Create(Expression: TExpression);
    destructor Destroy; override;
  private
    FExpression: TExpression;
  public
    function Compile: TArray<TOp>; override;
  end;

implementation

uses uExceptions, uOpLog, uTypeVoid;

{ TInstructionLog }

constructor TInstructionLog.Create(Expression: TExpression);
begin
  if Expression.ResultType is TTypeVoid then
    raise ECodeException.Create(E_NO_RESULT);
  FExpression := Expression;
end;

destructor TInstructionLog.Destroy;
begin
  FExpression.Free;
  inherited;
end;

function TInstructionLog.Compile: TArray<TOp>;
begin
  SetLength(Result, 0);
  AppendOp(Result, FExpression.Compile);
  AppendOp(Result, [TOpLog.Create(FExpression.ResultType.Hash)]);
end;

end.
