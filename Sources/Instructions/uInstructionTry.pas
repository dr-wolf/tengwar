unit uInstructionTry;

interface

uses
  uArray, uInstruction, uExpression, uType, uOp;

type
  TInstructionTry = class(TInstruction)
  public
    constructor Create(Instruction: TInstruction);
    destructor Destroy; override;
  private type
    TCatchClause = record
      Exception: TExpression;
      Instruction: TInstruction;
    end;
  private
    FInstruction: TInstruction;
    FCatchClauses: TArray<TCatchClause>;
    function FindClause(ExceptionType: TType): Integer;
  public
    function Compile: TArray<TOp>; override;
    function AddCatchCaluse(Exception: TExpression; Instruction: TInstruction): Boolean;
  end;

implementation

uses SysUtils, uData, uOpTry, uOpCatch, uOpJmp, uOpAssign;

{ TInstructionTry }

constructor TInstructionTry.Create(Instruction: TInstruction);
begin
  FInstruction := Instruction;
  SetLength(FCatchClauses, 0);
end;

destructor TInstructionTry.Destroy;
var
  i: Integer;
begin
  FInstruction.Free;
  for i := 0 to High(FCatchClauses) do
  begin
    FCatchClauses[i].Exception.Free;
    FCatchClauses[i].Instruction.Free;
  end;
  FCatchClauses := nil;
  inherited;
end;

function TInstructionTry.Compile: TArray<TOp>;
var
  i, l, p: Integer;
  a: array of TArray<TOp>;
  c: TArray<Integer>;
  e: TArray<IData>;
begin
  SetLength(Result, 0);
  AppendOp(Result, [TOpTry.Create]);
  AppendOp(Result, FInstruction.Compile);

  SetLength(a, Length(FCatchClauses));
  SetLength(c, Length(FCatchClauses));
  SetLength(e, Length(FCatchClauses));
  l := 1;
  for i := High(a) downto 0 do
  begin
    SetLength(a[i], 0);
    AppendOp(a[i], FCatchClauses[i].Exception.Compile);
    AppendOp(a[i], [TOpAssign.Create]);
    AppendOp(a[i], FCatchClauses[i].Instruction.Compile);
    if l > 1 then
      AppendOp(a[i], [TOpJmp.Create(l)]);
    Inc(l, Length(a[i]));

    e[i] := FCatchClauses[i].Exception.ResultType.Default;
  end;

  AppendOp(Result, [TOpJmp.Create(l + 1), nil]);
  p := High(Result);
  for i := 0 to High(a) do
  begin
    c[i] := High(Result) - p + 1;
    AppendOp(Result, a[i]);
  end;

  Result[p] := TOpCatch.Create(e, c);
end;

function TInstructionTry.AddCatchCaluse(Exception: TExpression; Instruction: TInstruction): Boolean;
begin
  Result := FindClause(Exception.ResultType) < 0;
  if Result then
  begin
    SetLength(FCatchClauses, Length(FCatchClauses) + 1);
    FCatchClauses[High(FCatchClauses)].Exception := Exception;
    FCatchClauses[High(FCatchClauses)].Instruction := Instruction;
  end;
end;

function TInstructionTry.FindClause(ExceptionType: TType): Integer;
begin
  Result := High(FCatchClauses);
  while Result >= 0 do
  begin
    if FCatchClauses[Result].Exception.ResultType.Compatible(ExceptionType) then
      Break;
    Dec(Result);
  end;
end;

end.
