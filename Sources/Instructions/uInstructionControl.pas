unit uInstructionControl;

interface

uses
  uArray, uInstruction, uOp;

type
  TInstructionControl = class(TInstruction)
  public
    constructor Create(Code: Byte);
    destructor Destroy; override;
  private
    FCode: Byte;
  public
    function Compile: TArray<TOp>; override;
  end;

implementation

uses uExceptions, uOpJmp;

{ TInstructionControl }

constructor TInstructionControl.Create(Code: Byte);
begin
  if not(Code in [CC_BREAK, CC_CONTINUE, CC_EXIT]) then
    raise ECodeException.Create(E_SYNTAX_ERROR);
  FCode := Code;
end;

destructor TInstructionControl.Destroy;
begin
  inherited;
end;

function TInstructionControl.Compile: TArray<TOp>;
begin
  SetLength(Result, 1);
  case FCode of
    CC_BREAK:
      Result[0] := TOpJmp.Create(1);
    CC_CONTINUE:
      Result[0] := TOpJmp.Create(-1);
    CC_EXIT:
      Result[0] := TOpJmp.Create(0);
  end;
end;

end.
