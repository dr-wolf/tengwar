unit uInstructionWhile;

interface

uses
  uArray, uInstruction, uExpression, uOp;

type
  TInstructionWhile = class(TInstruction)
  public
    constructor Create(Condition: TExpression; Instruction: TInstruction);
    destructor Destroy; override;
  private
    FCondition: TExpression;
    FInstruction: TInstruction;
  public
    function Compile: TArray<TOp>; override;
  end;

implementation

uses uExceptions, uTypeBool, uOpJmp, uOpJit;

{ TInstructionWhile }

constructor TInstructionWhile.Create(Condition: TExpression; Instruction: TInstruction);
begin
  if not(Condition.ResultType is TTypeBool) then
    raise ECodeException.Create(E_EXPECTED_BOOL);
  FCondition := Condition;
  FInstruction := Instruction;
end;

destructor TInstructionWhile.Destroy;
begin
  FCondition.Free;
  FInstruction.Free;
  inherited;
end;

function TInstructionWhile.Compile: TArray<TOp>;
var
  a: TArray<TOp>;
  i: Integer;
begin
  SetLength(a, 0);
  AppendOp(a, FInstruction.Compile);
  SetLength(Result, 0);
  AppendOp(Result, FCondition.Compile);
  AppendOp(Result, [TOpJit.Create(True, Length(a) + 2)]);

  for i := 0 to High(a) do
    if (a[i] is TOpJmp) and (Abs((a[i] as TOpJmp).Delta) = 1) then
      if ((a[i] as TOpJmp).Delta > 0) then
        (a[i] as TOpJmp).Delta := Length(a) - i + 1
      else
        (a[i] as TOpJmp).Delta := -i - Length(Result);

  AppendOp(a, [TOpJmp.Create(-Length(a) - Length(Result))]);
  AppendOp(Result, a);
end;

end.
