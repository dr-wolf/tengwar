unit uInstructionEach;

interface

uses
  uArray, uInstruction, uExpression, uOp;

type
  TInstructionEach = class(TInstruction)
  public
    constructor Create(List, Item: TExpression; Instruction: TInstruction);
    destructor Destroy; override;
  private
    FList: TExpression;
    FItem: TExpression;
    FInstruction: TInstruction;
  public
    function Compile: TArray<TOp>; override;
  end;

implementation

uses uExceptions, uTypeList, uOpAssign, uOpCheck, uOpJit, uOpJmp, uOpPop, uOpSplit;

{ TInstructionEach }

constructor TInstructionEach.Create(List, Item: TExpression; Instruction: TInstruction);
begin
  if not(List.ResultType is TTypeList) then
    raise ECodeException.Create(E_EXPECTED_ITERABLE);
  if Item.Immutable then
    raise ECodeException.Create(E_CONSTANT_ASSIGNMENT);
  if not(List.ResultType.NestedType().Compatible(Item.ResultType)) then
    raise ECodeException.Create(E_TYPE_UNCOMPATIBLE, List.ResultType.NestedType().Hash);
  FList := List;
  FItem := Item;
  FInstruction := Instruction;
end;

destructor TInstructionEach.Destroy;
begin
  FList.Free;
  FItem.Free;
  FInstruction.Free;
  inherited;
end;

function TInstructionEach.Compile: TArray<TOp>;
var
  i, s: Integer;
begin
  SetLength(Result, 0);
  AppendOp(Result, FList.Compile);
  AppendOp(Result, [TOpSplit.Create]);

  s := High(Result);

  AppendOp(Result, [TOpCheck.Create, nil]);
  AppendOp(Result, FItem.Compile);
  AppendOp(Result, [TOpAssign.Create]);
  AppendOp(Result, FInstruction.Compile);
  AppendOp(Result, [TOpJmp.Create(-Length(Result) + s + 1), TOpPop.Create(True)]);

  Result[s + 2] := TOpJit.Create(False, Length(Result) - s - 3);

  for i := s to High(Result) do
    if (Result[i] is TOpJmp) and (Abs((Result[i] as TOpJmp).Delta) = 1) then
      if ((Result[i] as TOpJmp).Delta > 0) then
        (Result[i] as TOpJmp).Delta := Length(Result) - i - 1
      else
        (Result[i] as TOpJmp).Delta := -i + s + 1;
end;

end.
