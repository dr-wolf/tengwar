unit uInstructionBind;

interface

uses
  uArray, uInstruction, uExpression, uOp;

type
  TInstructionBind = class(TInstruction)
  public
    constructor Create(Expression: TExpression);
    destructor Destroy; override;
  private
    FExpression: TExpression;
  public
    function Compile: TArray<TOp>; override;
  end;

implementation

uses uExceptions, uType, uTypeFactory, uOpBind;

{ TInstructionBind }

constructor TInstructionBind.Create(Expression: TExpression);
var
  t: TType;
begin
  t := TypeFactory.GetFunc(TypeFactory.GetStruct([], []), TypeFactory.GetVoid, False);
  if not t.Compatible(Expression.ResultType) then
    raise ECodeException.Create(E_TYPE_UNCOMPATIBLE, t.Hash);
  FExpression := Expression;
end;

destructor TInstructionBind.Destroy;
begin
  FExpression.Free;
  inherited;
end;

function TInstructionBind.Compile: TArray<TOp>;
begin
  SetLength(Result, 0);
  AppendOp(Result, FExpression.Compile);
  AppendOp(Result, [TOpBind.Create]);
end;

end.
