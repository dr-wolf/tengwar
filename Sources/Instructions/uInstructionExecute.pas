unit uInstructionExecute;

interface

uses
  uArray, uInstruction, uExpression, uOp;

type
  TInstructionExecute = class(TInstruction)
  public
    constructor Create(Expression: TExpression; Async: Boolean);
    destructor Destroy; override;
  private
    FExpression: TExpression;
    FAsync: Boolean;
  public
    function Compile: TArray<TOp>; override;
  end;

implementation

uses uExceptions, uTypeVoid, uOpRun;

{ TInstructionExecute }

constructor TInstructionExecute.Create(Expression: TExpression; Async: Boolean);
begin
  if not(Expression.ResultType is TTypeVoid) then
    raise ECodeException.Create(E_EXPECTED_STATEMENT);
  FExpression := Expression;
  FAsync := Async;
end;

function TInstructionExecute.Compile: TArray<TOp>;
begin
  SetLength(Result, 0);
  AppendOp(Result, FExpression.Compile);
  if FAsync then
  begin
    SetLength(Result, Length(Result) + 1);
    Result[High(Result)] := Result[High(Result) - 1];
    Result[High(Result) - 1] := TOpRun.Create;
  end;
end;

destructor TInstructionExecute.Destroy;
begin
  FExpression.Free;
  inherited;
end;

end.
