unit uInstructionAssign;

interface

uses
  uArray, uInstruction, uExpression, uOp;

type
  TInstructionAssign = class(TInstruction)
  public
    constructor Create(Variable: TExpression; Value: TExpression);
    destructor Destroy; override;
  private
    FVariable: TExpression;
    FValue: TExpression;
  public
    function Compile: TArray<TOp>; override;
  end;

implementation

uses uExceptions, uExpressionStruct, uTypeVoid, uOpAssign;

{ TInstructionAssign }

constructor TInstructionAssign.Create(Variable: TExpression; Value: TExpression);
begin
  FVariable := Variable;
  FValue := Value;
  if (Variable.ResultType is TTypeVoid) or (Value.ResultType is TTypeVoid) then
    raise ECodeException.Create(E_NO_RESULT);
  if not FVariable.ResultType.Compatible(FValue.ResultType) then
    raise ECodeException.Create(E_TYPE_UNCOMPATIBLE, FVariable.ResultType.Hash);
  if (FVariable is TExpressionStruct) and not TExpressionStruct(FVariable).Nameless then
    raise ECodeException.Create(E_CONSTANT_ASSIGNMENT);
  if FVariable.Immutable then
    raise ECodeException.Create(E_CONSTANT_ASSIGNMENT);
end;

function TInstructionAssign.Compile: TArray<TOp>;
begin
  SetLength(Result, 0);
  AppendOp(Result, FValue.Compile);
  AppendOp(Result, FVariable.Compile);
  AppendOp(Result, [TOpAssign.Create]);
end;

destructor TInstructionAssign.Destroy;
begin
  FVariable.Free;
  FValue.Free;
  inherited;
end;

end.
