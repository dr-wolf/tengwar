unit uInstructionIf;

interface

uses
  uArray, uInstruction, uExpression, uOp;

type
  TInstructionIf = class(TInstruction)
  public
    constructor Create(Condition: TExpression; IfBranch, ElseBranch: TInstruction);
    destructor Destroy; override;
  private
    FCondition: TExpression;
    FIfBranch: TInstruction;
    FElseBranch: TInstruction;
  public
    function Compile: TArray<TOp>; override;
  end;

implementation

uses uExceptions, uTypeBool, uOpJmp, uOpJit;

{ TInstructionIf }

constructor TInstructionIf.Create(Condition: TExpression; IfBranch, ElseBranch: TInstruction);
begin
  if not(Condition.ResultType is TTypeBool) then
    raise ECodeException.Create(E_EXPECTED_BOOL);
  FCondition := Condition;
  FIfBranch := IfBranch;
  FElseBranch := ElseBranch;
end;

destructor TInstructionIf.Destroy;
begin
  FCondition.Free;
  FIfBranch.Free;
  FElseBranch.Free;
  inherited;
end;

function TInstructionIf.Compile: TArray<TOp>;
var
  f, t: TArray<TOp>;
begin
  SetLength(t, 0);
  AppendOp(t, FIfBranch.Compile);
  SetLength(f, 0);
  AppendOp(f, FElseBranch.Compile);
  SetLength(Result, 0);
  AppendOp(Result, FCondition.Compile);
  if Length(f) > 0 then
  begin
    AppendOp(Result, [TOpJit.Create(True, Length(t) + 2)]);
    AppendOp(Result, t);
    AppendOp(Result, [TOpJmp.Create(Length(f) + 1)]);
    AppendOp(Result, f);
  end else begin
    AppendOp(Result, [TOpJit.Create(True, Length(t) + 1)]);
    AppendOp(Result, t);
  end;
end;

end.
