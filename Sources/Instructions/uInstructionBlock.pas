unit uInstructionBlock;

interface

uses
  uArray, uInstruction, uOp;

type
  TInstructionBlock = class(TInstruction)
  public
    constructor Create; overload;
    constructor Create(Instructions: TArray<TInstruction>); overload;
    destructor Destroy; override;
  private
    FInstructions: TArray<TInstruction>;
  public
    function Compile: TArray<TOp>; override;
    procedure AddInstruction(Instruction: TInstruction);
  end;

implementation

{ TInstructionBlock }

constructor TInstructionBlock.Create;
begin
  SetLength(FInstructions, 0);
end;

constructor TInstructionBlock.Create(Instructions: TArray<TInstruction>);
var
  i: Integer;
begin
  SetLength(FInstructions, Length(Instructions));
  for i := 0 to High(FInstructions) do
    FInstructions[i] := Instructions[i];
end;

destructor TInstructionBlock.Destroy;
var
  i: Integer;
begin
  for i := 0 to High(FInstructions) do
    FInstructions[i].Free;
  FInstructions := nil;
  inherited;
end;

function TInstructionBlock.Compile: TArray<TOp>;
var
  i: Integer;
begin
  SetLength(Result, 0);
  for i := 0 to High(FInstructions) do
    AppendOp(Result, FInstructions[i].Compile);
end;

procedure TInstructionBlock.AddInstruction(Instruction: TInstruction);
begin
  SetLength(FInstructions, Length(FInstructions) + 1);
  FInstructions[High(FInstructions)] := Instruction;
end;

end.
