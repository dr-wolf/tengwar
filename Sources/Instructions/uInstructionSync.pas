unit uInstructionSync;

interface

uses
  uArray, uInstruction, uOp;

type
  TInstructionSync = class(TInstruction)
  public
    constructor Create(Section: string; Instruction: TInstruction);
    destructor Destroy; override;
  private
    FSection: string;
    FInstruction: TInstruction;
  public
    function Compile: TArray<TOp>; override;
  end;

implementation

uses uOpSync;

{ TInstructionSync }

constructor TInstructionSync.Create(Section: string; Instruction: TInstruction);
begin
  FSection := Section;
  FInstruction := Instruction;
end;

destructor TInstructionSync.Destroy;
begin
  FInstruction.Free;
  inherited;
end;

function TInstructionSync.Compile: TArray<TOp>;
begin
  SetLength(Result, 0);
  AppendOp(Result, [TOpSync.Create(FSection, True)]);
  AppendOp(Result, FInstruction.Compile);
  AppendOp(Result, [TOpSync.Create(FSection, False)]);
end;

end.
