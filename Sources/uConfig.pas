unit uConfig;

interface

uses
  uArray;

const
  {$I build.inc};

type
  TConfigParam = (cpLibPath, cpOutput, cpHelp, cpLog, pErr);
  TConfigParamSet = set of TConfigParam;

type
  TConfig = class(TObject)
  public
    constructor Create(AcceptedParams: TConfigParamSet);
  private
    FAcceptedParams: TConfigParamSet;
    FLibPathList: TArray<string>;
    FScript: string;
    FOutput: string;
    FBasePath: string;
    FHelp: Boolean;
    FLog: Boolean;
    function GetLibPathCount: Integer;
    function ParsePathList(Text: string): TArray<string>;
    function GetLibPath(I: Integer): string;
    procedure ParseParamStr(Param: string);
  public
    property Script: string read FScript;
    property Output: string read FOutput;
    property LibPath[I: Integer]: string read GetLibPath;
    property LibPathCount: Integer read GetLibPathCount;
    property Help: Boolean read FHelp;
    property Log: Boolean read FLog;
  end;

implementation

uses SysUtils;

function IsRelativePath(const Path: string): Boolean;
var
  L: Integer;
begin
  L := Length(Path);
  Result := (L > 0) and (Path[1] <> PathDelim)
{$IFDEF MSWINDOWS} and (L > 1) and (Path[2] <> ':'){$ENDIF MSWINDOWS};
end;

{ TConfig }

constructor TConfig.Create(AcceptedParams: TConfigParamSet);
var
  I: Integer;
  s: string;
begin
  FAcceptedParams := AcceptedParams;

  FOutput := '';
  FHelp := False;
  FLog := False;
  FBasePath := ExtractFilePath(ParamStr(0));

  I := 1;
  while I <= ParamCount() do
  begin
    s := ParamStr(I);
    if (Length(s) > 2) and (s[1] + s[2] = '--') then
    begin
      ParseParamStr(Copy(s, 3));
      Inc(I);
    end
    else
      Break;
  end;

  if I <= ParamCount() then
    FScript := ParamStr(I)
  else
    FHelp := True;
end;

function TConfig.GetLibPath(I: Integer): string;
begin
  Result := FLibPathList[I];
end;

function TConfig.GetLibPathCount: Integer;
begin
  Result := Length(FLibPathList);
end;

procedure TConfig.ParseParamStr(Param: string);

  function DetectParam(ParamName: string): TConfigParam;
  const
    PARAM_KWORDS: array [TConfigParam] of string = ('libpath', 'out', 'help', 'log', '');
  begin
    Result := Low(TConfigParam);
    while (Result < High(TConfigParam)) and (PARAM_KWORDS[Result] <> ParamName) do
      Result := Succ(Result);
  end;

var
  pname: string;
  p: TConfigParam;
  I: Integer;
begin
  pname := Copy(Param, 1, Pos('=', Param + '=') - 1);
  p := DetectParam(pname);
  if p in FAcceptedParams then
    case p of
      cpHelp:
        FHelp := True;
      cpLog:
        FLog := True;
      cpLibPath:
        begin
          FLibPathList := ParsePathList(Copy(Param, Pos('=', Param + '=') + 1));
          for I := 0 to High(FLibPathList) do
            if IsRelativePath(FLibPathList[I]) then
              FLibPathList[I] := IncludeTrailingPathDelimiter(ExpandFileName(FBasePath + FLibPathList[I]));
        end;
      cpOutput:
        FOutput := Copy(Param, Pos('=', Param + '=') + 1);
    else
      FHelp := True;
    end
  else
    FHelp := True;
end;

function TConfig.ParsePathList(Text: string): TArray<string>;
const
  PATH_SEP = {$IFDEF UNIX}':'{$ELSE}';'{$ENDIF};
var
  p: Integer;
begin
  SetLength(Result, 0);
  if Text <> '' then
    repeat
      p := Pos(PATH_SEP, Text + PATH_SEP);
      SetLength(Result, Length(Result) + 1);
      Result[High(Result)] := Copy(Text, 1, p - 1);
      Delete(Text, 1, p);
    until (Text = '');
end;

end.
