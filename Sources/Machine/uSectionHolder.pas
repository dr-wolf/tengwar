unit uSectionHolder;

interface

uses
  SyncObjs, uDict;

type
  TSectionHolder = class(TObject)
  public
    constructor Create;
    destructor Destroy; override;
  private
    FSections: TDict<TCriticalSection>;
  public
    function Get(Section: string): TCriticalSection;
  end;

implementation

{ TSectionHolder }

constructor TSectionHolder.Create;
begin
  FSections := TDict<TCriticalSection>.Create;
end;

destructor TSectionHolder.Destroy;
begin
  FSections.Reset;
  while FSections.Next do
    FSections[FSections.Key].Free;
  FSections.Free;
  inherited;
end;

function TSectionHolder.Get(Section: string): TCriticalSection;
begin
  if FSections.Contains(Section) then
    Result := FSections[Section]
  else
  begin
    Result := TCriticalSection.Create;
    FSections.Put(Section, Result);
  end;
end;

end.
