unit uMachine;

interface

uses
  uArray, uOp, uState, uRunner, uSectionHolder, uOpCache;

type
  TMachine = class(TObject)
  public
    constructor Create;
    destructor Destroy; override;
  private
    FOps: TArray<TOp>;
    FRunners: TArray<TRunner>;
    FSectionHolder: TSectionHolder;
    function GetSize: Integer;
    procedure SpawnRunner(State: TState);
  public
    property Size: Integer read GetSize;
    procedure AddAll(Ops: array of TOp);
    procedure SetStartOp(Op: TOp);
    procedure Run;
    procedure Serialize(OpCache: TOpCache);
    procedure Load(OpCache: TOpCache);
    procedure Log(Path: string);
  end;

implementation

uses SysUtils, Classes, uOpCall, uData, uDataArray, uDataString, uOpLoader;

{ TMachine }

constructor TMachine.Create;
begin
  SetLength(FOps, 2);
  FOps[1] := TOpCall.Create([]);
  FSectionHolder := TSectionHolder.Create;
  SetLength(FRunners, 0);
end;

destructor TMachine.Destroy;
var
  i: Integer;
begin
  for i := 0 to High(FOps) do
    FOps[i].Free;
  FOps := nil;

  for i := 0 to High(FRunners) do
    FRunners[i].Free;
  FRunners := nil;
  FSectionHolder.Free;
  inherited;
end;

function TMachine.GetSize: Integer;
begin
  Result := Length(FOps);
end;

procedure TMachine.AddAll(Ops: array of TOp);
begin
  AppendOp(FOps, Ops);
end;

procedure TMachine.Load(OpCache: TOpCache);
var
  i: Integer;
begin
  SetLength(FOps, OpCache.OpStream.ReadInteger);
  for i := 0 to High(FOps) do
    FOps[i] := LoadOp(OpCache) as TOp;
end;

procedure TMachine.Log(Path: string);
var
  i: Integer;
begin
  with TStringList.Create do
  begin
    for i := 0 to High(FOps) do
      Add('0x' + IntToHex(i, 8) + ': ' + FOps[i].Log);
    SaveToFile(Path + '.log');
    Free;
  end;
end;

procedure TMachine.Run;
var
  a: TArray<IData>;
  i: Integer;
  s: TState;
begin
  SetLength(a, ParamCount);
  for i := 1 to ParamCount do
    a[i - 1] := TDataString.Create(ParamStr(i));

  s := TState.Create(SpawnRunner, FSectionHolder);
  s.Stack.Push(TDataArray.Create([TDataArray.Create(a)]));
  SpawnRunner(s);

  i := 0;
  while i <= High(FRunners) do
  begin
    FRunners[i].WaitFor;
    Inc(i);
  end;
end;

procedure TMachine.Serialize(OpCache: TOpCache);
var
  i: Integer;
begin
  OpCache.OpStream.WriteInteger(Length(FOps));
  for i := 0 to High(FOps) do
    FOps[i].Serialize(OpCache.OpStream, OpCache);
end;

procedure TMachine.SetStartOp(Op: TOp);
begin
  FOps[0] := Op;
end;

procedure TMachine.SpawnRunner(State: TState);
begin
  SetLength(FRunners, Length(FRunners) + 1);
  FRunners[High(FRunners)] := TRunner.Create(FOps, State);
  Sleep(10);
end;

end.
