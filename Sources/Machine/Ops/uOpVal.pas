unit uOpVal;

interface

uses
  uState, uOp, uData, uOpCache;

type
  TOpVal = class(TOp)
  public
    constructor Create(Data: IData);
  private
    FData: IData;
  public
    function Log: string; override;
    procedure Execute(State: TState); override;
    procedure Serialize(Stream: TClassStream; Cache: TOpCache); override;
  end;

implementation

uses uOpCode;

{ TOpVal }

constructor TOpVal.Create(Data: IData);
begin
  FOpUid := UID_OP_VAL;
  FData := Data;
end;

function TOpVal.Log: string;
begin
  Result := 'val ' + FData.Log;
end;

procedure TOpVal.Execute(State: TState);
begin
  State.Stack.Push(FData);
  inherited;
end;

procedure TOpVal.Serialize(Stream: TClassStream; Cache: TOpCache);
begin
  inherited;
  Stream.WriteInteger(Cache.Save(FData as TData, (FData as TData).Serialize));
end;

end.
