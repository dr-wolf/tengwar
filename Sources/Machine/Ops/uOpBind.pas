unit uOpBind;

interface

uses
  uState, uOp;

type
  TOpBind = class(TOp)
  public
    constructor Create;
  public
    function Log: string; override;
    procedure Execute(State: TState); override;
  end;

implementation

uses uOpCode;

{ TOpBind }

constructor TOpBind.Create;
begin
  FOpUid := UID_OP_BIND;
end;

function TOpBind.Log: string;
begin
  Result := 'bind';
end;

procedure TOpBind.Execute(State: TState);
begin
  State.Binds.Push(State.Stack.Pop);
  inherited;
end;

end.
