unit uOpCatch;

interface

uses
  uArray, uState, uOp, uData, uOpCache;

type
  TOpCatch = class(TOp)
  public
    constructor Create(Exceptions: TArray<IData>; Catches: TArray<Integer>);
  private
    FCatches: TArray<Integer>;
    FExceptions: TArray<IData>;
  public
    function Log: string; override;
    procedure Execute(State: TState); override;
    procedure Serialize(Stream: TClassStream; Cache: TOpCache); override;
  end;

implementation

uses uExceptions, uOpCode;

{ TOpCatch }

constructor TOpCatch.Create(Exceptions: TArray<IData>; Catches: TArray<Integer>);
var
  i: Integer;
begin
  FOpUid := UID_OP_CATCH;
  SetLength(FExceptions, Length(Exceptions));
  for i := 0 to High(FExceptions) do
    FExceptions[i] := Exceptions[i];
  SetLength(FCatches, Length(Catches));
  for i := 0 to High(FCatches) do
    FCatches[i] := Catches[i];
end;

function TOpCatch.Log: string;
begin
  Result := 'catch';
end;

procedure TOpCatch.Execute(State: TState);
var
  E: IData;
  i: Integer;
begin
  E := State.Stack.Pop;
  State.PopLayer(ltException);

  for i := 0 to High(FExceptions) do
  begin
    try
      FExceptions[i].Assign(E);
    except
      Continue;
    end;
    State.Jump(FCatches[i]);
    State.PopLayer(ltSafe);
    State.Stack.Push(E);
    Exit;
  end;
  State.Jump();
  raise ERuntimeException.Create(E_RUNTIME_ERROR, E);
end;

procedure TOpCatch.Serialize(Stream: TClassStream; Cache: TOpCache);
var
  i: Integer;
begin
  inherited;
  with Stream do
  begin
    WriteInteger(Length(FCatches));
    for i := 0 to High(FCatches) do
    begin
      WriteInteger(FCatches[i]);
      WriteInteger(Cache.Save(FExceptions[i] as TData, (FExceptions[i] as TData).Serialize));
    end;
  end;
end;

end.
