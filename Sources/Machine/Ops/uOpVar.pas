unit uOpVar;

interface

uses
  uState, uOp, uOpCache;

type
  TOpVar = class(TOp)
  public
    constructor Create(DataId: Integer);
  private
    FDataId: Integer;
  public
    function Log: string; override;
    procedure Execute(State: TState); override;
    procedure Serialize(Stream: TClassStream; Cache: TOpCache); override;
  end;

implementation

uses uOpCode;

{ TOpLoadVar }

constructor TOpVar.Create(DataId: Integer);
begin
  FOpUid := UID_OP_VAR;
  FDataId := DataId;
end;

function TOpVar.Log: string;
begin
  Result := 'var ' + Int(FDataId);
end;

procedure TOpVar.Execute(State: TState);
begin
  State.Stack.Push(State.Context.Resolve(FDataId));
  inherited;
end;

procedure TOpVar.Serialize(Stream: TClassStream; Cache: TOpCache);
begin
  inherited;
  Stream.WriteInteger(FDataId);
end;

end.
