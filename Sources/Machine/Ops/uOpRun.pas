unit uOpRun;

interface

uses
  uState, uOp;

type
  TOpRun = class(TOp)
  public
    constructor Create;
  public
    function Log: string; override;
    procedure Execute(State: TState); override;
  end;

implementation

uses uData, uOpCode;

{ TOpRun }

constructor TOpRun.Create;
begin
  FOpUid := UID_OP_RUN;
end;

function TOpRun.Log: string;
begin
  Result := 'run';
end;

procedure TOpRun.Execute(State: TState);
var
  s: TState;
  d: IData;
begin
  State.Jump();
  s := State.Derive;
  d := State.Stack.Pop;
  s.Stack.Push(State.Stack.Pop);
  s.Stack.Push(d);
  State.Spawn(s);
  inherited;
end;

end.
