unit uOpPop;

interface

uses
  uState, uOp, uOpCache;

type
  TOpPop = class(TOp)
  public
    constructor Create(Clear: Boolean);
  private
    FClear: Boolean;
  public
    function Log: string; override;
    procedure Execute(State: TState); override;
    procedure Serialize(Stream: TClassStream; Cache: TOpCache); override;
  end;

implementation

uses uOpCode;

{ TOpPop }

constructor TOpPop.Create(Clear: Boolean);
begin
  FOpUid := UID_OP_POP;
  FClear := Clear;
end;

function TOpPop.Log: string;
begin
  Result := 'pop';
  if FClear then
    Result := Result + ' layer';
end;

procedure TOpPop.Execute(State: TState);
begin
  if FClear then
    State.PopLayer(ltEach)
  else
    State.Stack.Pop;
  inherited;
end;

procedure TOpPop.Serialize(Stream: TClassStream; Cache: TOpCache);
begin
  inherited;
  Stream.WriteByte(Byte(FClear));
end;

end.
