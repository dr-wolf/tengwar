unit uOpTail;

interface

uses
  uState, uOp;

type
  TOpTail = class(TOp)
  public
    constructor Create;
  public
    function Log: string; override;
    procedure Execute(State: TState); override;
  end;

implementation

uses uData, uDataPipe, uOpCode;

{ TOpTail }

constructor TOpTail.Create;
begin
  FOpUid := UID_OP_TAIL;
end;

function TOpTail.Log: string;
begin
  Result := 'tail';
end;

procedure TOpTail.Execute(State: TState);
var
  d: IData;
begin
  d := State.Stack.Pop;
  State.Stack.Push((d as TDataPipe).Tail);
  inherited;
end;

end.
