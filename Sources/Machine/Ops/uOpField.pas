unit uOpField;

interface

uses
  uState, uOp, uOpCache;

type
  TOpField = class(TOp)
  public
    constructor Create(Field: Integer);
  private
    FField: Integer;
  public
    function Log: string; override;
    procedure Execute(State: TState); override;
    procedure Serialize(Stream: TClassStream; Cache: TOpCache); override;
  end;

implementation

uses uOpCode;

{ TOpField }

constructor TOpField.Create(Field: Integer);
begin
  FOpUid := UID_OP_FIELD;
  FField := Field;
end;

function TOpField.Log: string;
begin
  Result := 'field ' + Int(FField);
end;

procedure TOpField.Execute(State: TState);
begin
  State.Stack.Push(State.Stack.Pop.Item(FField));
  inherited;
end;

procedure TOpField.Serialize(Stream: TClassStream; Cache: TOpCache);
begin
  inherited;
  Stream.WriteInteger(FField);
end;

end.
