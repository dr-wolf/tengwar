unit uOpCall;

interface

uses
  uArray, uState, uOp, uOpCache;

type
  TOpCall = class(TOp)
  public
    constructor Create(ParamMap: array of Integer);
  private
    FParamMap: TArray<Integer>;
  public
    function Log: string; override;
    function ParamsCount: Integer;
    procedure Execute(State: TState); override;
    procedure Serialize(Stream: TClassStream; Cache: TOpCache); override;
  end;

implementation

uses uDataFunc, uOpCode;

{ TOpCall }

constructor TOpCall.Create(ParamMap: array of Integer);
var
  i: Integer;
begin
  FOpUid := UID_OP_CALL;
  SetLength(FParamMap, Length(ParamMap));
  for i := 0 to High(FParamMap) do
    FParamMap[i] := ParamMap[i];
end;

function TOpCall.Log: string;
begin
  Result := 'call';
end;

function TOpCall.ParamsCount: Integer;
begin
  Result := Length(FParamMap);
end;

procedure TOpCall.Execute(State: TState);
begin
  State.Jump();
  (State.Stack.Pop as TDataFunc).Call(State, FParamMap);
end;

procedure TOpCall.Serialize(Stream: TClassStream; Cache: TOpCache);
var
  i: Integer;
begin
  inherited;
  with Stream do
  begin
    WriteInteger(Length(FParamMap));
    for i := 0 to High(FParamMap) do
      WriteInteger(FParamMap[i]);
  end;
end;

end.
