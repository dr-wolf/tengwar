unit uOpAssign;

interface

uses
  uState, uOp;

type
  TOpAssign = class(TOp)
  public
    constructor Create;
  public
    function Log: string; override;
    procedure Execute(State: TState); override;
  end;

implementation

uses uData, uOpCode;

{ TOpAssign }

constructor TOpAssign.Create;
begin
  FOpUid := UID_OP_ASSIGN;
end;

procedure TOpAssign.Execute(State: TState);
var
  d: IData;
begin
  d := State.Stack.Pop;
  d.Assign(State.Stack.Pop.Unwrap);
  inherited;
end;

function TOpAssign.Log: string;
begin
  Result := 'assign';
end;

end.
