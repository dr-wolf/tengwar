unit uOpLog;

interface

uses
  uState, uOp, uOpCache;

type
  TOpLog = class(TOp)
  public
    constructor Create(TypeHint: string);
  private
    FTypeHint: string;
  public
    function Log: string; override;
    procedure Execute(State: TState); override;
    procedure Serialize(Stream: TClassStream; Cache: TOpCache); override;
  end;

implementation

uses SysUtils, uOpCode;

{ TOpLog }

constructor TOpLog.Create(TypeHint: string);
begin
  FOpUid := UID_OP_LOG;
  FTypeHint := TypeHint;
end;

function TOpLog.Log: string;
begin
  Result := 'log';
end;

procedure TOpLog.Execute(State: TState);
begin
  Writeln('[' + FormatDateTime('hh:mm:ss', Now) + ']: ' + FTypeHint + ' := ' + State.Stack.Pop.Log);
  inherited;
end;

procedure TOpLog.Serialize(Stream: TClassStream; Cache: TOpCache);
begin
  inherited;
  Stream.WriteString(FTypeHint);
end;

end.
