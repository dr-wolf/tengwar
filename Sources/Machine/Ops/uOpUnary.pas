unit uOpUnary;

interface

uses
  uState, uOperations, uOp, uOpCache;

type
  TOpUnary = class(TOp)
  public
    constructor Create(Operation: TUnaryOp);
  private
    FOperation: TUnaryOp;
  public
    function Log: string; override;
    procedure Execute(State: TState); override;
    procedure Serialize(Stream: TClassStream; Cache: TOpCache); override;
  end;

implementation

uses uOpCode;

{ TOpUnary }

constructor TOpUnary.Create(Operation: TUnaryOp);
begin
  FOpUid := UID_OP_UNARY;
  FOperation := Operation;
end;

function TOpUnary.Log: string;
begin
  Result := 'apply u ' + Int(Byte(FOperation));
end;

procedure TOpUnary.Execute(State: TState);
begin
  State.Stack.Push(State.Stack.Pop.Apply(FOperation));
  inherited;
end;

procedure TOpUnary.Serialize(Stream: TClassStream; Cache: TOpCache);
begin
  inherited;
  Stream.WriteByte(Byte(FOperation));
end;

end.
