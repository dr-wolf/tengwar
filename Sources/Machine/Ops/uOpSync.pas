unit uOpSync;

interface

uses
  uState, uOp, uOpCache;

type
  TOpSync = class(TOp)
  public
    constructor Create(Section: string; Enter: Boolean);
  private
    FSection: string;
    FEnter: Boolean;
  public
    function Log: string; override;
    procedure Execute(State: TState); override;
    procedure Serialize(Stream: TClassStream; Cache: TOpCache); override;
  end;

implementation

uses uOpCode;

{ TOpSync }

constructor TOpSync.Create(Section: string; Enter: Boolean);
begin
  FOpUid := UID_OP_SYNC;
  FEnter := Enter;
  FSection := Section;
end;

function TOpSync.Log: string;
begin
  Result := 'sync';
  if FEnter then
    Result := Result + ' in'
  else
    Result := Result + ' out';
end;

procedure TOpSync.Execute(State: TState);
begin
  if FEnter then
    State.Sections.Get(FSection).Enter
  else
    State.Sections.Get(FSection).Leave;
  inherited;
end;

procedure TOpSync.Serialize(Stream: TClassStream; Cache: TOpCache);
begin
  inherited;
  Stream.WriteByte(Byte(FEnter));
  Stream.WriteString(FSection);
end;

end.
