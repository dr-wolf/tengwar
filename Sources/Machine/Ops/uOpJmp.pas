unit uOpJmp;

interface

uses
  uState, uOp, uOpCache;

type
  TOpJmp = class(TOp)
  public
    constructor Create(Delta: Integer);
  private
    FDelta: Integer;
  public
    property Delta: Integer read FDelta write FDelta;
    function Log: string; override;
    procedure Execute(State: TState); override;
    procedure Serialize(Stream: TClassStream; Cache: TOpCache); override;
  end;

implementation

uses uOpCode;

{ TOpJmp }

constructor TOpJmp.Create(Delta: Integer);
begin
  FOpUid := UID_OP_JMP;
  FDelta := Delta;
end;

procedure TOpJmp.Execute(State: TState);
begin
  State.Jump(FDelta);
end;

function TOpJmp.Log: string;
begin
  Result := 'jmp ' + SignedInt(FDelta);
end;

procedure TOpJmp.Serialize(Stream: TClassStream; Cache: TOpCache);
begin
  inherited;
  Stream.WriteInteger(FDelta);
end;

end.
