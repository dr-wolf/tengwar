unit uOpCheck;

interface

uses
  uState, uOp;

type
  TOpCheck = class(TOp)
  public
    constructor Create;
  public
    function Log: string; override;
    procedure Execute(State: TState); override;
  end;

implementation

uses uDataBool, uOpCode;

{ TOpCheck }

constructor TOpCheck.Create;
begin
  FOpUid := UID_OP_CHECK;
end;

function TOpCheck.Log: string;
begin
  Result := 'check';
end;

procedure TOpCheck.Execute(State: TState);
var
  f: Boolean;
begin
  f := State.Stack.IsEmpty;
  State.Stack.Push(TDataBool.Create(f));
  inherited;
end;

end.
