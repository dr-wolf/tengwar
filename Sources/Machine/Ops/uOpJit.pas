unit uOpJit;

interface

uses
  uState, uOp, uOpCache;

type
  TOpJit = class(TOp)
  public
    constructor Create(Invert: Boolean; Delta: Integer);
  private
    FInvert: Boolean;
    FDelta: Integer;
  public
    function Log: string; override;
    procedure Execute(State: TState); override;
    procedure Serialize(Stream: TClassStream; Cache: TOpCache); override;
  end;

implementation

uses uData, uDataBool, uOpCode;

{ TOpJit }

constructor TOpJit.Create(Invert: Boolean; Delta: Integer);
begin
  FOpUid := UID_OP_JIT;
  FInvert := Invert;
  FDelta := Delta;
end;

procedure TOpJit.Execute(State: TState);
var
  d: IData;
begin
  d := State.Stack.Pop;
  if AsBool(d) xor FInvert then
    State.Jump(FDelta)
  else
    State.Jump();
end;

function TOpJit.Log: string;
begin
  if FInvert then
    Result := 'jif ' + SignedInt(FDelta)
  else
    Result := 'jit ' + SignedInt(FDelta);
end;

procedure TOpJit.Serialize(Stream: TClassStream; Cache: TOpCache);
begin
  inherited;
  Stream.WriteByte(Byte(FInvert));
  Stream.WriteInteger(FDelta);
end;

end.
