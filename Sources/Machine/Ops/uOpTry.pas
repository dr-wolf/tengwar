unit uOpTry;

interface

uses
  uState, uOp;

type
  TOpTry = class(TOp)
  public
    constructor Create;
  public
    function Log: string; override;
    procedure Execute(State: TState); override;
  end;

implementation

uses uOpCode;

{ TOpTry }

constructor TOpTry.Create;
begin
  FOpUid := UID_OP_TRY;
end;

function TOpTry.Log: string;
begin
  Result := 'try';
end;

procedure TOpTry.Execute(State: TState);
begin
  State.PushLayer(ltSafe);
  inherited;
end;

end.
