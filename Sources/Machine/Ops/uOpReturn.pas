unit uOpReturn;

interface

uses
  uArray, uState, uOp, uOpCache;

type
  TOpReturn = class(TOp)
  public
    constructor Create(Results: TArray<Integer>);
  private
    FResults: TArray<Integer>;
  public
    function Log: string; override;
    procedure Execute(State: TState); override;
    procedure Serialize(Stream: TClassStream; Cache: TOpCache); override;
  end;

implementation

uses uExceptions, uData, uDataArray, uDataFunc, uOpCode;

{ TOpReturn }

constructor TOpReturn.Create(Results: TArray<Integer>);
var
  i: Integer;
begin
  FOpUid := UID_OP_RETURN;
  SetLength(FResults, Length(Results));
  for i := 0 to High(FResults) do
    FResults[i] := Results[i];
end;

function TOpReturn.Log: string;
begin
  Result := 'ret';
end;

procedure TOpReturn.Execute(State: TState);
var
  i: Integer;
  a: TArray<IData>;
  d: IData;
begin
  if not State.Binds.IsEmpty then
  begin
    State.Stack.Push(TDataArray.Create([]));
    (State.Binds.Pop as TDataFunc).Call(State, nil);
    Exit;
  end
  else
    State.Binds.Clear(State.Binds.Layer);

  if State.Layer[ltFunc] < State.Layer[ltException] then
  begin
    d := State.Stack.Pop;
    State.PopLayer(ltFunc);
    State.Return;
    raise ERuntimeException.Create(E_RUNTIME_ERROR, d);
  end;

  State.PopLayer(ltFunc);
  if Length(FResults) = 1 then
    State.Stack.Push(State.Context.Resolve(FResults[0]).Clone)
  else if Length(FResults) > 1 then
  begin
    SetLength(a, Length(FResults));
    for i := 0 to High(FResults) do
      a[i] := State.Context.Resolve(FResults[i]).Clone;
    State.Stack.Push(TDataArray.Create(a));
  end;
  State.Return;
end;

procedure TOpReturn.Serialize(Stream: TClassStream; Cache: TOpCache);
var
  i: Integer;
begin
  inherited;
  with Stream do
  begin
    WriteInteger(Length(FResults));
    for i := 0 to High(FResults) do
      WriteInteger(FResults[i]);
  end;
end;

end.
