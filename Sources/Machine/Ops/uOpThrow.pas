unit uOpThrow;

interface

uses
  uState, uOp;

type
  TOpThrow = class(TOp)
  public
    constructor Create;
  public
    function Log: string; override;
    procedure Execute(State: TState); override;
  end;

implementation

uses uExceptions, uOpCode;

{ TOpThrow }

constructor TOpThrow.Create;
begin
  FOpUid := UID_OP_THROW;
end;

function TOpThrow.Log: string;
begin
  Result := 'throw';
end;

procedure TOpThrow.Execute(State: TState);
begin
  raise ERuntimeException.Create(E_RUNTIME_ERROR, State.Stack.Pop);
  inherited;
end;

end.
