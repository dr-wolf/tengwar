unit uOpRange;

interface

uses
  uState, uOp;

type
  TOpRange = class(TOp)
  public
    constructor Create;
  public
    function Log: string; override;
    procedure Execute(State: TState); override;
  end;

implementation

uses uArray, uData, uDataArray, uDataNumeric, uOpCode;

{ TOpRange }

constructor TOpRange.Create;
begin
  FOpUid := UID_OP_RANGE;
end;

function TOpRange.Log: string;
begin
  Result := 'range';
end;

procedure TOpRange.Execute(State: TState);
var
  Index, Source: IData;
  Items: TArray<IData>;
  i: Integer;
begin
  Index := State.Stack.Pop;
  Source := State.Stack.Pop;
  SetLength(Items, Index.Count);
  for i := 0 to High(Items) do
    Items[i] := Source.Item(TranslateIndex(AsInt(Index.Item(i)), Source.Count));
  State.Stack.Push(TDataArray.Create(Items));
  inherited;
end;

end.
