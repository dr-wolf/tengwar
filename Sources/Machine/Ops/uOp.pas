unit uOp;

interface

uses
  uArray, uState, uOpCache, uOpCode;

type
  TOp = class(TObject)
  protected
    FOpUid: TUID;
  public
    property OpUid: TUID read FOpUid;
    function Log: string; virtual; abstract;
    procedure Execute(State: TState); virtual;
    procedure Serialize(Stream: TClassStream; Cache: TOpCache); virtual;
  end;

function Int(I: Integer): string;
function SignedInt(I: Integer): string;
procedure AppendOp(var A: TArray<TOp>; B: array of TOp);

implementation

uses SysUtils;

function Int(I: Integer): string;
begin
  Result := IntToStr(I);
end;

function SignedInt(I: Integer): string;
begin
  if I > 0 then
    Result := '+' + IntToStr(I)
  else
    Result := IntToStr(I);
end;

procedure AppendOp(var A: TArray<TOp>; B: array of TOp);
var
  S, I: Integer;
begin
  S := High(A) + 1;
  SetLength(A, Length(A) + Length(B));
  for I := 0 to High(B) do
    A[S + I] := B[I];
end;

{ TOp }

procedure TOp.Execute(State: TState);
begin
  State.Jump();
end;

procedure TOp.Serialize(Stream: TClassStream; Cache: TOpCache);
begin
  Stream.WriteByte(FOpUid);
end;

end.
