unit uOpSplit;

interface

uses
  uState, uOp;

type
  TOpSplit = class(TOp)
  public
    constructor Create;
  public
    function Log: string; override;
    procedure Execute(State: TState); override;
  end;

implementation

uses uData, uOpCode;

{ TOpSplit }

constructor TOpSplit.Create;
begin
  FOpUid := UID_OP_SPLIT;
end;

function TOpSplit.Log: string;
begin
  Result := 'split';
end;

procedure TOpSplit.Execute(State: TState);
var
  i: Integer;
  a: IData;
begin
  a := State.Stack.Pop;
  State.PushLayer(ltEach);
  for i := a.Count - 1 downto 0 do
    State.Stack.Push(a.Item(i));
  inherited;
end;

end.
