unit uOpCast;

interface

uses
  uState, uOp, uData, uOpCache;

type
  TOpCast = class(TOp)
  public
    constructor Create(Data: IData; TestCompatOnly: Boolean);
  private
    FTestCompatOnly: Boolean;
    FData: IData;
  public
    function Log: string; override;
    procedure Execute(State: TState); override;
    procedure Serialize(Stream: TClassStream; Cache: TOpCache); override;
  end;

implementation

uses uExceptions, uDataBool, uOpCode;

{ TOpCast }

constructor TOpCast.Create(Data: IData; TestCompatOnly: Boolean);
begin
  FOpUid := UID_OP_CAST;
  FTestCompatOnly := TestCompatOnly;
  FData := Data;
end;

function TOpCast.Log: string;
begin
  Result := 'cast ' + FData.Log;
end;

procedure TOpCast.Execute(State: TState);
var
  d: IData;
begin
  d := FData.Clone;
  try
    d.Assign(State.Stack.Pop.Unwrap);
    if FTestCompatOnly then
      State.Stack.Push(TDataBool.Create(True))
    else
      State.Stack.Push(d);
  except
    if FTestCompatOnly then
      State.Stack.Push(TDataBool.Create(False))
    else
      raise ERuntimeException.Create(E_TYPE_CAST, nil);
  end;
  inherited;
end;

procedure TOpCast.Serialize(Stream: TClassStream; Cache: TOpCache);
begin
  inherited;
  Stream.WriteInteger(Cache.Save(FData as TData, (FData as TData).Serialize));
end;

end.
