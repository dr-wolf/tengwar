unit uOpFunc;

interface

uses
  uArray, uState, uOp, uOpCache;

type
  TOpFunc = class(TOp)
  public
    constructor Create(Closures: TArray<Integer>);
  private
    FClosures: TArray<Integer>;
  public
    function Log: string; override;
    procedure Execute(State: TState); override;
    procedure Serialize(Stream: TClassStream; Cache: TOpCache); override;
  end;

implementation

uses uData, uDataFunc, uOpCode;

{ TOpFunc }

constructor TOpFunc.Create(Closures: TArray<Integer>);
var
  i: Integer;
begin
  FOpUid := UID_OP_FUNC;
  SetLength(FClosures, Length(Closures));
  for i := 0 to High(FClosures) do
    FClosures[i] := Closures[i];
end;

function TOpFunc.Log: string;
begin
  Result := 'mkfunc';
end;

procedure TOpFunc.Execute(State: TState);
var
  f: IData;
  c: TArray<IData>;
  i: Integer;
begin
  SetLength(c, Length(FClosures));
  for i := 0 to High(c) do
    c[i] := State.Context.Resolve(FClosures[i]);
  f := State.Stack.Pop.Clone;
  (f as TDataFunc).AddClosures(c);
  (f as TDataFunc).RefreshSelf;
  State.Stack.Push(f);
  inherited;
end;

procedure TOpFunc.Serialize(Stream: TClassStream; Cache: TOpCache);
var
  i: Integer;
begin
  inherited;
  with Stream do
  begin
    WriteInteger(Length(FClosures));
    for i := 0 to High(FClosures) do
      WriteInteger(FClosures[i]);
  end;
end;

end.
