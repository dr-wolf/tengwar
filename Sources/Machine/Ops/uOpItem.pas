unit uOpItem;

interface

uses
  uState, uOp;

type
  TOpItem = class(TOp)
  public
    constructor Create;
  public
    function Log: string; override;
    procedure Execute(State: TState); override;
  end;

implementation

uses uData, uDataArray, uDataNumeric, uOpCode;

{ TOpItem }

constructor TOpItem.Create;
begin
  FOpUid := UID_OP_ITEM;
end;

function TOpItem.Log: string;
begin
  Result := 'item';
end;

procedure TOpItem.Execute(State: TState);
var
  Index, Data: IData;
begin
  Index := State.Stack.Pop;
  Data := State.Stack.Pop;
  State.Stack.Push(Data.Item(TranslateIndex(AsInt(Index), Data.Count)));
  inherited;
end;

end.
