unit uOpStack;

interface

uses
  uState, uOp, uOpCache;

type
  TOpStack = class(TOp)
  public
    constructor Create(BuildSet: Boolean; Size: Integer);
  private
    FBuildSet: Boolean;
    FSize: Integer;
  public
    function Log: string; override;
    procedure Execute(State: TState); override;
    procedure Serialize(Stream: TClassStream; Cache: TOpCache); override;
  end;

implementation

uses uArray, uData, uDataArray, uDataSet, uOpCode;

{ TOpStack }

constructor TOpStack.Create(BuildSet: Boolean; Size: Integer);
begin
  FOpUid := UID_OP_STACK;
  FBuildSet := BuildSet;
  FSize := Size;
end;

function TOpStack.Log: string;
begin
  Result := 'stack ' + Int(FSize);
end;

procedure TOpStack.Execute(State: TState);
var
  i: Integer;
  a: TArray<IData>;
begin
  SetLength(a, FSize);
  for i := 0 to FSize - 1 do
    a[i] := State.Stack.Pop;
  if FBuildSet then
    State.Stack.Push(TDataSet.Create(a))
  else
    State.Stack.Push(TDataArray.Create(a));
  inherited;
end;

procedure TOpStack.Serialize(Stream: TClassStream; Cache: TOpCache);
begin
  inherited;
  Stream.WriteByte(Byte(FBuildSet));
  Stream.WriteInteger(FSize);
end;

end.
