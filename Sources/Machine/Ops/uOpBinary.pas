unit uOpBinary;

interface

uses
  uState, uOperations, uOp, uOpCache;

type
  TOpBinary = class(TOp)
  public
    constructor Create(Operation: TBinaryOp);
  private
    FOperation: TBinaryOp;
  public
    function Log: string; override;
    procedure Execute(State: TState); override;
    procedure Serialize(Stream: TClassStream; OpCache: TOpCache); override;
  end;

implementation

uses uData, uOpCode;

{ TOpBinary }

constructor TOpBinary.Create(Operation: TBinaryOp);
begin
  FOpUid := UID_OP_BINARY;
  FOperation := Operation;
end;

function TOpBinary.Log: string;
begin
  Result := 'apply b ' + Int(Byte(FOperation));
end;

procedure TOpBinary.Execute(State: TState);
var
  D: IData;
begin
  D := State.Stack.Pop;
  State.Stack.Push(State.Stack.Pop.Apply(FOperation, D));
  inherited;
end;

procedure TOpBinary.Serialize(Stream: TClassStream; OpCache: TOpCache);
begin
  inherited;
  Stream.WriteByte(Byte(FOperation));
end;

end.
