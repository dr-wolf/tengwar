unit uRunner;

interface

uses
  Classes, uArray, uState, uOp;

type
  TRunner = class(TThread)
  public
    constructor Create(Ops: TArray<TOp>; State: TState);
    destructor Destroy; override;
  private
    FOps: TArray<TOp>;
    FState: TState;
  public
    procedure Execute; override;
  end;

implementation

uses SysUtils, uExceptions, uOpCode, uDataString;

{ TRunner }

constructor TRunner.Create(Ops: TArray<TOp>; State: TState);
begin
  inherited Create(False);
  FreeOnTerminate := False;
  FOps := Ops;
  FState := State;
end;

destructor TRunner.Destroy;
begin
  FState.Free;
  inherited;
end;

procedure TRunner.Execute;

  procedure PushException(e: Exception);
  begin
    FState.PushLayer(ltException);
    if e is ERuntimeException then
      FState.Stack.Push((e as ERuntimeException).Data)
    else
      FState.Stack.Push(TDataString.Create(e.Message));
  end;

var
  tl: Integer;
begin
  while not FState.Completed do
    try
      FOps[FState.OpIndex].Execute(FState);
    except
      on e: Exception do
        if FState.Layer[ltSafe] > -1 then
        begin
          tl := 0;
          repeat
            case FOps[FState.OpIndex].OpUid of
              UID_OP_TRY:
                Inc(tl);
              UID_OP_CATCH:
                if tl <= 0 then
                begin
                  PushException(e);
                  Break;
                end
                else
                  Dec(tl);
              UID_OP_RETURN:
                begin
                  PushException(e);
                  Break;
                end;
            end;
            FState.Jump();
          until False;
        end else begin
          if e is ERuntimeException then
            Writeln('RUNTIME ERROR: ' + e.Message)
          else
            Writeln('UNEXPECTED ERROR: ' + e.Message);
          Readln;
          Exit;
        end;
    end;
end;

end.
