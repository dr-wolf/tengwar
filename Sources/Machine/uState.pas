unit uState;

interface

uses
  uArray, uStack, uContext, uSectionHolder, uData;

type
  TLayerType = (ltFunc, ltSafe, ltException, ltEach);

type
  TState = class(TObject)
  public type
    TRunnerSpawnEvent = procedure(State: TState) of object;
  public
    constructor Create(Spawn: TRunnerSpawnEvent; SectionHolder: TSectionHolder);
    destructor Destroy; override;
  private
    FCompleted: Boolean;
    FOpIndex: Integer;
    FStack: TStack<IData>;
    FBinds: TStack<IData>;
    FAddress: TStack<Integer>;
    FContext: TStack<TContext>;
    FSections: TSectionHolder;
    FStackLayers: array [TLayerType] of TArray<Integer>;
    FRunnerSpawn: TRunnerSpawnEvent;
    function GetContext: TContext;
    function GetLayer(LayerType: TLayerType): Integer;
  public
    property Completed: Boolean read FCompleted;
    property Context: TContext read GetContext;
    property OpIndex: Integer read FOpIndex;
    property Stack: TStack<IData> read FStack;
    property Binds: TStack<IData> read FBinds;
    property Sections: TSectionHolder read FSections;
    property Layer[LayerType: TLayerType]: Integer read GetLayer;
    property Spawn: TRunnerSpawnEvent read FRunnerSpawn;
    function Derive: TState;
    procedure PushLayer(LayerType: TLayerType);
    procedure PopLayer(LayerType: TLayerType);
    procedure Jump(Delta: Integer = 1);
    procedure Call(Address: Integer; Context: TContext);
    procedure Return;
  end;

implementation

{ TState }

constructor TState.Create(Spawn: TRunnerSpawnEvent; SectionHolder: TSectionHolder);
begin
  FOpIndex := 0;
  FCompleted := False;
  FStack := TStack<IData>.Create;
  FBinds := TStack<IData>.Create;
  FAddress := TStack<Integer>.Create;
  FContext := TStack<TContext>.Create;
  FSections := SectionHolder;
  FRunnerSpawn := Spawn;
end;

destructor TState.Destroy;
begin
  FBinds.Free;
  FStack.Free;
  FAddress.Free;
  FContext.Free;
  inherited;
end;

function TState.GetContext: TContext;
begin
  Result := FContext.Peek;
end;

function TState.GetLayer(LayerType: TLayerType): Integer;
begin
  if Length(FStackLayers[LayerType]) > 0 then
    Result := FStackLayers[LayerType][High(FStackLayers[LayerType])]
  else
    Result := -1;
end;

function TState.Derive: TState;
begin
  Result := TState.Create(FRunnerSpawn, FSections);
  Result.FOpIndex := FOpIndex;
  Result.FContext.Push(FContext.Peek.Recreate);
end;

procedure TState.Call(Address: Integer; Context: TContext);
begin
  FAddress.Push(FOpIndex);
  FContext.Push(Context);
  PushLayer(ltFunc);
  FBinds.AddLayer;
  FOpIndex := Address;
end;

procedure TState.Jump(Delta: Integer);
begin
  FOpIndex := FOpIndex + Delta;
end;

procedure TState.PopLayer(LayerType: TLayerType);
var
  l: TLayerType;
  Layer: Integer;
begin
  Layer := GetLayer(LayerType);
  FStack.Clear(Layer);
  for l := Low(l) to High(l) do
    while (Length(FStackLayers[l]) > 0) and (FStackLayers[l][High(FStackLayers[l])] >= Layer) do
      SetLength(FStackLayers[l], Length(FStackLayers[l]) - 1);
end;

procedure TState.PushLayer(LayerType: TLayerType);
begin
  SetLength(FStackLayers[LayerType], Length(FStackLayers[LayerType]) + 1);
  FStackLayers[LayerType][High(FStackLayers[LayerType])] := FStack.AddLayer;
end;

procedure TState.Return;
begin
  FOpIndex := FAddress.Pop;
  FContext.Pop.Free;
  if FAddress.IsEmpty then
    FCompleted := True;
end;

end.
