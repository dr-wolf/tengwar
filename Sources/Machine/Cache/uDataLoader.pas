unit uDataLoader;

interface

uses
  uOpCache;

procedure LoadData(Uid: Byte; Stream: TClassStream; Cache: TOpCache; var Result: TObject);

implementation

uses
  SysUtils, uArray, uData, uDataAny, uDataArray, uDataBool, uDataFunc, uDataNumeric, uDataPipe, uDataSet, uDataString,
  uFunction, uOpCode;

procedure LoadData(Uid: Byte; Stream: TClassStream; Cache: TOpCache; var Result: TObject);

  function LoadArray(Stream: TClassStream; Cache: TOpCache): TDataArray;
  var
    d: IData;
    I: Integer;
    a: TArray<IData>;
  begin
    d := Cache.Load(Stream.ReadInteger) as TData;
    SetLength(a, Stream.ReadInteger);
    if Length(a) > 0 then
    begin
      for I := 0 to High(a) do
        a[I] := Cache.Load(Stream.ReadInteger) as TData;
      Result := TDataArray.Create(a, d);
    end
    else
      Result := TDataArray.Create(d);
  end;

  procedure LoadFunc(Stream: TClassStream; Cache: TOpCache; var Result: TObject);
  var
    I: Integer;
    a: TArray<IData>;
  begin
    Result := TDataFunc.Create;
    (Result as TDataFunc).SetFunc(Cache.Load(Stream.ReadInteger) as TFunction);
    SetLength(a, Stream.ReadInteger);
    for I := 0 to High(a) do
      a[I] := Cache.Load(Stream.ReadInteger) as TData;
    (Result as TDataFunc).AddClosures(a);
  end;

  function LoadNumeric(Stream: TClassStream; Cache: TOpCache): TDataNumeric;
  var
    s: Boolean;
    I: Int64;
    e: Double;
  begin
    s := Boolean(Stream.ReadByte);
    I := Stream.ReadInteger;
    e := Stream.ReadDouble;
    Result := TDataNumeric.Create(I, e, s);
  end;

  function LoadPipe(Stream: TClassStream; Cache: TOpCache): TDataPipe;
  var
    d: IData;
  begin
    d := Cache.Load(Stream.ReadInteger) as TData;
    Result := TDataPipe.Create(d);
  end;

  function LoadSet(Stream: TClassStream; Cache: TOpCache): TDataSet;
  var
    I: Integer;
    a: TArray<IData>;
  begin
    SetLength(a, Stream.ReadInteger);
    for I := 0 to High(a) do
      a[I] := Cache.Load(Stream.ReadInteger) as TData;
    Result := TDataSet.Create(a);
  end;

begin
  case Uid of
    UID_DATA_ARRAY:
      Result := LoadArray(Stream, Cache);
    UID_DATA_BOOL:
      Result := TDataBool.Create(Boolean(Stream.ReadByte));
    UID_DATA_FUNC:
      LoadFunc(Stream, Cache, Result);
    UID_DATA_NUMERIC:
      Result := LoadNumeric(Stream, Cache);
    UID_DATA_PIPE:
      Result := LoadPipe(Stream, Cache);
    UID_DATA_SET:
      Result := LoadSet(Stream, Cache);
    UID_DATA_STRING:
      Result := TDataString.Create(Stream.ReadString);
    UID_DATA_ANY:
      Result := TDataAny.Create(Cache.Load(Stream.ReadInteger) as TData);
  else
    raise Exception.Create('Stream read error');
  end;
end;

end.
