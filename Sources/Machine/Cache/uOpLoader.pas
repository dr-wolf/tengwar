unit uOpLoader;

interface

uses
  uOpCache;

function LoadOp(Cache: TOpCache): TObject;

implementation

uses SysUtils, uArray, uData, uOpCode, uOperations, uOpAssign, uOpBinary, uOpBind, uOpCall, uOpCatch, uOpCheck,
  uOpField, uOpFunc, uOpItem, uOpJit, uOpJmp, uOpLog, uOpPop, uOpRange, uOpReturn, uOpRun, uOpSplit, uOpStack, uOpSync,
  uOpTail, uOpCast, uOpThrow, uOpTry, uOpUnary, uOpVal, uOpVar;

function LoadOp(Cache: TOpCache): TObject;

  function LoadIntArray(Cache: TOpCache): TArray<Integer>;
  var
    i: Integer;
  begin
    SetLength(Result, Cache.OpStream.ReadInteger);
    for i := 0 to High(Result) do
      Result[i] := Cache.OpStream.ReadInteger;
  end;

  function LoadCast(Cache: TOpCache): TOpCast;
  var
    f: Boolean;
    d: IData;
  begin
    f := Boolean(Cache.OpStream.ReadByte);
    d := Cache.Load(Cache.OpStream.ReadInteger) as TData;
    Result := TOpCast.Create(d, f);
  end;

  function LoadCatch(Cache: TOpCache): TOpCatch;
  var
    i: Integer;
    e: TArray<IData>;
    c: TArray<Integer>;
  begin
    SetLength(c, Cache.OpStream.ReadInteger);
    SetLength(e, Length(c));
    for i := 0 to High(c) do
    begin
      c[i] := Cache.OpStream.ReadInteger;
      e[i] := Cache.Load(Cache.OpStream.ReadInteger) as TData;
    end;
    Result := TOpCatch.Create(e, c);
  end;

  function LoadJit(Cache: TOpCache): TOpJit;
  var
    f: Boolean;
    d: Integer;
  begin
    f := Boolean(Cache.OpStream.ReadByte);
    d := Cache.OpStream.ReadInteger;
    Result := TOpJit.Create(f, d);
  end;

  function LoadStack(Cache: TOpCache): TOpStack;
  var
    f: Boolean;
    d: Integer;
  begin
    f := Boolean(Cache.OpStream.ReadByte);
    d := Cache.OpStream.ReadInteger;
    Result := TOpStack.Create(f, d);
  end;

  function LoadSync(Cache: TOpCache): TOpSync;
  var
    f: Boolean;
    s: string;
  begin
    f := Boolean(Cache.OpStream.ReadByte);
    s := Cache.OpStream.ReadString;
    Result := TOpSync.Create(s, f);
  end;

begin
  case Cache.OpStream.ReadByte of
    UID_OP_ASSIGN:
      Result := TOpAssign.Create;
    UID_OP_BINARY:
      Result := TOpBinary.Create(TBinaryOp(Cache.OpStream.ReadByte));
    UID_OP_BIND:
      Result := TOpBind.Create;
    UID_OP_CALL:
      Result := TOpCall.Create(LoadIntArray(Cache));
    UID_OP_CAST:
      Result := LoadCast(Cache);
    UID_OP_CATCH:
      Result := LoadCatch(Cache);
    UID_OP_CHECK:
      Result := TOpCheck.Create;
    UID_OP_FIELD:
      Result := TOpField.Create(Cache.OpStream.ReadInteger);
    UID_OP_FUNC:
      Result := TOpFunc.Create(LoadIntArray(Cache));
    UID_OP_ITEM:
      Result := TOpItem.Create;
    UID_OP_JIT:
      Result := LoadJit(Cache);
    UID_OP_JMP:
      Result := TOpJmp.Create(Cache.OpStream.ReadInteger);
    UID_OP_LOG:
      Result := TOpLog.Create(Cache.OpStream.ReadString);
    UID_OP_POP:
      Result := TOpPop.Create(Boolean(Cache.OpStream.ReadByte));
    UID_OP_RANGE:
      Result := TOpRange.Create;
    UID_OP_RETURN:
      Result := TOpReturn.Create(LoadIntArray(Cache));
    UID_OP_RUN:
      Result := TOpRun.Create;
    UID_OP_SPLIT:
      Result := TOpSplit.Create();
    UID_OP_STACK:
      Result := LoadStack(Cache);
    UID_OP_SYNC:
      Result := LoadSync(Cache);
    UID_OP_TAIL:
      Result := TOpTail.Create;
    UID_OP_THROW:
      Result := TOpThrow.Create;
    UID_OP_TRY:
      Result := TOpTry.Create;
    UID_OP_UNARY:
      Result := TOpUnary.Create(TUnaryOp(Cache.OpStream.ReadByte));
    UID_OP_VAL:
      Result := TOpVal.Create(Cache.Load(Cache.OpStream.ReadInteger) as TData);
    UID_OP_VAR:
      Result := TOpVar.Create(Cache.OpStream.ReadInteger);
  else
    raise Exception.Create('File is corrupted');
  end;
end;

end.
