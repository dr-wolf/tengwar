unit uFunctionLoader;

interface

uses
  uOpCache, uRepositoryFactory;

procedure LoadFunction(Uid: Byte; Stream: TClassStream; Cache: TOpCache; Repositories: TRepositoryFactory;
  var Result: TObject);

implementation

uses SysUtils, uArray, uTypeFactory, uContext, uEnvironment, uCustomFunc, uOpCode,
  uFuncBitAnd, uFuncBitMod, uFuncBitNot, uFuncBitOr, uFuncBitShl, uFuncBitShr, uFuncBitXor,
  uFuncDateDecode, uFuncDateEncode, uFuncDateFmt, uFuncDateParse,
  uFuncFileCopy, uFuncFileGetPos, uFuncFileSetPos, uFuncFileSize, uFuncFileTrunc, uFuncFileReadInt, uFuncFileReadFloat,
  uFuncFileReadString, uFuncFileWriteInt, uFuncFileWriteFloat, uFuncFileWriteString,
  uFuncFSPwd, uFuncFSDelete, uFuncFSChDir, uFuncFSMkDir, uFuncFSRmDir, uFuncFSStat, uFuncFSAbsPath, uFuncFSScan,
  uFuncFSOpen, uFuncFSClose,
  uFuncIOGet, uFuncIOPause, uFuncIOPut,
  uFuncMathAbs, uFuncMathCos, uFuncMathFrac, uFuncMathFloor, uFuncMathLog, uFuncMathRand, uFuncMathSin,
  uFuncOSSleep, uFuncOSExec, uFuncOSLsof, uFuncOSLsos, uFuncOSGetEnv, uFuncOSSetEnv, uFuncOSTime,
  uFuncRexMatch, uFuncRexFind, uFuncRexReplace, uFuncRexSplit,
  uFuncStrFmt, uFuncStrHex, uFuncStrLen, uFuncStrSub, uFuncStrPos, uFuncStrParse, uFuncStrAscii, uFuncStrText,
  uFuncStrLower, uFuncStrUpper,
  uFuncTCPConnect, uFuncTCPClose, uFuncTCPSend, uFuncTCPReceive, uFuncTCPListen, uFuncTCPAccept, uFuncTCPRemote,
  uFuncUDPBind, uFuncUDPClose, uFuncUDPReceive, uFuncUDPSend, uFuncUDPBroadcast;

procedure LoadFunction(Uid: Byte; Stream: TClassStream; Cache: TOpCache; Repositories: TRepositoryFactory;
  var Result: TObject);

  procedure LoadCustom(Stream: TClassStream; Cache: TOpCache; var Result: TObject);

    function ReadArray(Stream: TClassStream): TArray<Integer>;
    var
      i: Integer;
    begin
      SetLength(Result, Stream.ReadInteger);
      for i := 0 to High(Result) do
        Result[i] := Stream.ReadInteger;
    end;

  var
    cx: TContext;
    p, r, c: TArray<Integer>;
    a: Integer;
  begin
    a := Stream.ReadInteger;
    p := ReadArray(Stream);
    r := ReadArray(Stream);
    c := ReadArray(Stream);
    cx := TContext.Create;
    Result := TCustomFunc.Create(cx, c, p, r);
    (Result as TCustomFunc).SetAddress(a);
    cx.Load(Stream, Cache);
  end;

var
  fid: Byte;

begin
  fid := Stream.ReadByte;
  case fid of
    FID_CUSTOM:
      LoadCustom(Stream, Cache, Result);

    FID_BIT_AND:
      Result := TFuncBitAnd.Create;
    FID_BIT_MOD:
      Result := TFuncBitMod.Create;
    FID_BIT_NOT:
      Result := TFuncBitNot.Create;
    FID_BIT_OR:
      Result := TFuncBitOr.Create;
    FID_BIT_SHL:
      Result := TFuncBitShl.Create;
    FID_BIT_SHR:
      Result := TFuncBitShr.Create;
    FID_BIT_XOR:
      Result := TFuncBitXor.Create;

    FID_DATE_DECODE:
      Result := TFuncDateDecode.Create(TypeFactory.GetPredefined(pdtDate));
    FID_DATE_ENCODE:
      Result := TFuncDateEncode.Create(TypeFactory.GetPredefined(pdtDate));
    FID_DATE_FMT:
      Result := TFuncDateFmt.Create;
    FID_DATE_PARSE:
      Result := TFuncDateParse.Create;

    FID_FILE_COPY:
      Result := TFuncFileCopy.Create(Repositories.StreamRepository);
    FID_FILE_GETPOS:
      Result := TFuncFileGetPos.Create(Repositories.StreamRepository);
    FID_FILE_READFLOAT:
      Result := TFuncFileReadFloat.Create(Repositories.StreamRepository);
    FID_FILE_READINT:
      Result := TFuncFileReadInt.Create(Repositories.StreamRepository);
    FID_FILE_READSTRING:
      Result := TFuncFileReadString.Create(Repositories.StreamRepository);
    FID_FILE_SETPOS:
      Result := TFuncFileSetPos.Create(Repositories.StreamRepository);
    FID_FILE_SIZE:
      Result := TFuncFileSize.Create(Repositories.StreamRepository);
    FID_FILE_TRUNC:
      Result := TFuncFileTrunc.Create(Repositories.StreamRepository);
    FID_FILE_WRITEFLOAT:
      Result := TFuncFileWriteFloat.Create(Repositories.StreamRepository);
    FID_FILE_WRITEINT:
      Result := TFuncFileWriteInt.Create(Repositories.StreamRepository);
    FID_FILE_WRITESTRING:
      Result := TFuncFileWriteString.Create(Repositories.StreamRepository);

    FID_FS_ABSPATH:
      Result := TFuncFSAbsPath.Create;
    FID_FS_CHDIR:
      Result := TFuncFSChDir.Create;
    FID_FS_CLOSE:
      Result := TFuncFSClose.Create(Repositories.StreamRepository);
    FID_FS_DELETE:
      Result := TFuncFSDelete.Create;
    FID_FS_MKDIR:
      Result := TFuncFSMkDir.Create;
    FID_FS_OPEN:
      Result := TFuncFSOpen.Create(Repositories.StreamRepository);
    FID_FS_PWD:
      Result := TFuncFSPwd.Create;
    FID_FS_RMDIR:
      Result := TFuncFSRmDir.Create;
    FID_FS_SCAN:
      Result := TFuncFSScan.Create;
    FID_FS_STAT:
      Result := TFuncFSStat.Create(TypeFactory.GetPredefined(pdtFileAttr));

    FID_IO_GET:
      Result := TFuncIOGet.Create;
    FID_IO_PAUSE:
      Result := TFuncIOPause.Create;
    FID_IO_PUT:
      Result := TFuncIOPut.Create;

    FID_MATH_ABS:
      Result := TFuncMathAbs.Create;
    FID_MATH_COS:
      Result := TFuncMathCos.Create;
    FID_MATH_FLOOR:
      Result := TFuncMathFloor.Create;
    FID_MATH_FRAC:
      Result := TFuncMathFrac.Create;
    FID_MATH_LOG:
      Result := TFuncMathLog.Create;
    FID_MATH_RAND:
      Result := TFuncMathRand.Create;
    FID_MATH_SIN:
      Result := TFuncMathSin.Create;

    FID_OS_EXEC:
      Result := TFuncOSExec.Create(Environment.Variables);
    FID_OS_GETENV:
      Result := TFuncOSGetEnv.Create(Environment.Variables);
    FID_OS_LSOF:
      Result := TFuncOSLsof.Create(Repositories.StreamRepository);
    FID_OS_LSOS:
      Result := TFuncOSLsos.Create(Repositories.TCPSocketRepository, Repositories.UDPSocketRepository);
    FID_OS_SETENV:
      Result := TFuncOSSetEnv.Create(Environment.Variables);
    FID_OS_SLEEP:
      Result := TFuncOSSleep.Create;
    FID_OS_TIME:
      Result := TFuncOSTime.Create;

    FID_REX_FIND:
      Result := TFuncRexFind.Create(TypeFactory.GetPredefined(pdtRex));
    FID_REX_MATCH:
      Result := TFuncRexMatch.Create;
    FID_REX_REPLACE:
      Result := TFuncRexReplace.Create;
    FID_REX_SPLIT:
      Result := TFuncRexSplit.Create;

    FID_STR_ASCII:
      Result := TFuncStrAscii.Create;
    FID_STR_FMT:
      Result := TFuncStrFmt.Create;
    FID_STR_HEX:
      Result := TFuncStrHex.Create;
    FID_STR_LEN:
      Result := TFuncStrLen.Create;
    FID_STR_LOWER:
      Result := TFuncStrLower.Create;
    FID_STR_PARSE:
      Result := TFuncStrParse.Create;
    FID_STR_POS:
      Result := TFuncStrPos.Create;
    FID_STR_SUB:
      Result := TFuncStrSub.Create;
    FID_STR_TEXT:
      Result := TFuncStrText.Create;
    FID_STR_UPPER:
      Result := TFuncStrUpper.Create;

    FID_TCP_ACCEPT:
      Result := TFuncTCPAccept.Create(Repositories.TCPSocketRepository);
    FID_TCP_CLOSE:
      Result := TFuncTCPClose.Create(Repositories.TCPSocketRepository);
    FID_TCP_CONNECT:
      Result := TFuncTCPConnect.Create(Repositories.TCPSocketRepository);
    FID_TCP_LISTEN:
      Result := TFuncTCPListen.Create(Repositories.TCPSocketRepository);
    FID_TCP_RECEIVE:
      Result := TFuncTCPReceive.Create(Repositories.TCPSocketRepository, Repositories.StreamRepository);
    FID_TCP_REMOTE:
      Result := TFuncTCPRemote.Create(Repositories.TCPSocketRepository);
    FID_TCP_SEND:
      Result := TFuncTCPSend.Create(Repositories.TCPSocketRepository, Repositories.StreamRepository);

    FID_UDP_BIND:
      Result := TFuncUDPBind.Create(Repositories.UDPSocketRepository);
    FID_UDP_BROADCAST:
      Result := TFuncUDPBroadcast.Create(Repositories.StreamRepository);
    FID_UDP_CLOSE:
      Result := TFuncUDPClose.Create(Repositories.UDPSocketRepository);
    FID_UDP_RECEIVE:
      Result := TFuncUDPReceive.Create(Repositories.UDPSocketRepository, Repositories.StreamRepository);
    FID_UDP_SEND:
      Result := TFuncUDPSend.Create(Repositories.StreamRepository);
  else
    raise Exception.Create('Error Message');
  end;
end;

end.
