unit uOpCache;

interface

uses
  Classes, uArray, uRepositoryFactory;

type
  TFileHeader = array [0 .. 3] of Byte;

const
  HEADER: TFileHeader = ($0, $0, $0, $0);

type
  TClassStream = class(TObject)
  public
    constructor Create; overload;
    constructor Create(Stream: TStream; Size: Int64); overload;
    destructor Destroy; override;
  private
    FStream: TStream;
    function GetSize: Int64;
  public
    property Size: Int64 read GetSize;
    function ReadByte: Byte;
    function ReadInteger: Int64;
    function ReadDouble: Double;
    function ReadString: string;
    procedure WriteByte(B: Byte);
    procedure WriteInteger(I: Int64);
    procedure WriteDouble(E: Double);
    procedure WriteString(S: string);
    procedure SaveTo(Stream: TStream);
  end;

  TOpCache = class(TObject)
  public
    constructor Create(Repositories: TRepositoryFactory);
    destructor Destroy; override;
  private type
    TClass = record
      Instance: TObject;
      Stream: TClassStream;
    end;

    TSerializer = function(const Cache: TOpCache): TClassStream of object;
  private
    FTable: TArray<TClass>;
    FOpStream: TClassStream;
    FRepositories: TRepositoryFactory;
    function FindId(Instance: TObject): Integer;
    function GetDataSize: Int64;
  public
    property OpStream: TClassStream read FOpStream;
    property DataSize: Int64 read GetDataSize;
    function Load(Id: Integer): TObject;
    function Save(Instance: TObject; Serialize: TSerializer): Integer;
    procedure SaveToFile(Path: string);
    procedure LoadFromFile(Path: string);
  end;

implementation

uses SysUtils, uDataLoader, uFunctionLoader, uOpCode;

function HeaderCompare(const H1, H2: TFileHeader): Boolean;
var
  I: Integer;
begin
  Result := True;
  for I := 0 to High(H1) do
  begin
    Result := Result and (H1[I] = H2[I]);
    if not Result then
      Exit;
  end;
end;

{ TOpCache.TClassStream }

constructor TClassStream.Create;
begin
  FStream := TMemoryStream.Create;
end;

constructor TClassStream.Create(Stream: TStream; Size: Int64);
begin
  FStream := TMemoryStream.Create;
  FStream.CopyFrom(Stream, Size);
  FStream.Position := 0;
end;

destructor TClassStream.Destroy;
begin
  FStream.Free;
  inherited;
end;

function TClassStream.GetSize: Int64;
begin
  Result := FStream.Size;
end;

function TClassStream.ReadByte: Byte;
begin
  FStream.ReadBuffer(Result, SizeOf(Result));
end;

function TClassStream.ReadDouble: Double;
begin
  FStream.ReadBuffer(Result, SizeOf(Result));
end;

function TClassStream.ReadInteger: Int64;
var
  h, l: Byte;
begin
  FStream.ReadBuffer(h, SizeOf(h));
  l := h and $F0 shr 4;
  Result := 0;
  FStream.ReadBuffer(Result, l);
  if h and $0F <> 0 then
    Result := -Result;
end;

function TClassStream.ReadString: string;
var
  bytes: TBytes;
  l: Integer;
begin
  l := ReadInteger;
  SetLength(bytes, l);
  FStream.ReadBuffer(Pointer(bytes)^, l);
  Result := StringOf(bytes);
end;

procedure TClassStream.SaveTo(Stream: TStream);
begin
  Stream.CopyFrom(FStream, 0);
end;

procedure TClassStream.WriteByte(B: Byte);
begin
  FStream.WriteBuffer(B, SizeOf(B));
end;

procedure TClassStream.WriteDouble(E: Double);
begin
  FStream.WriteBuffer(E, SizeOf(E));
end;

procedure TClassStream.WriteInteger(I: Int64);

  function SigBytes(V: Int64): Byte;
  begin
    if V > 0 then
    begin
      Result := 8;
      while V and $FF00000000000000 = 0 do
      begin
        V := V shl 8;
        Dec(Result);
      end;
    end
    else
      Result := 0;
  end;

var
  h, l: Byte;
begin
  l := SigBytes(Abs(I));
  h := (Byte(I < 0) and $0F) or (l and $0F shl 4);
  I := Abs(I);
  FStream.WriteBuffer(h, SizeOf(h));
  FStream.WriteBuffer(I, l);
end;

procedure TClassStream.WriteString(S: string);
var
  bytes: TBytes;
begin
  bytes := BytesOf(S);
  WriteInteger(Length(bytes));
  FStream.WriteBuffer(Pointer(bytes)^, Length(bytes));
end;

{ TOpCache }

constructor TOpCache.Create(Repositories: TRepositoryFactory);
begin
  SetLength(FTable, 0);
  FRepositories := Repositories;
  FOpStream := TClassStream.Create;
end;

destructor TOpCache.Destroy;
var
  I: Integer;
begin
  for I := 0 to High(FTable) do
    FTable[I].Stream.Free;
  FTable := nil;
  FOpStream.Free;
  inherited;
end;

function TOpCache.FindId(Instance: TObject): Integer;
begin
  Result := High(FTable);
  while (Result >= 0) and (FTable[Result].Instance <> Instance) do
    Dec(Result);
end;

function TOpCache.GetDataSize: Int64;
var
  I: Integer;
begin
  Result := 0;
  for I := 0 to High(FTable) do
    Inc(Result, FTable[I].Stream.Size);
end;

procedure TOpCache.LoadFromFile(Path: string);
var
  FileStream: TFileStream;
  Table: TClassStream;
  Sizes: TArray<Int64>;
  h: TFileHeader;
  l: Int64;
  I: Integer;
begin
  FileStream := TFileStream.Create(Path, fmOpenReadWrite);
  FileStream.ReadBuffer(h, SizeOf(h));

  if not HeaderCompare(h, HEADER) then
    raise Exception.Create('This file is not Tengwar executable file');

  FileStream.ReadBuffer(l, SizeOf(l));
  FOpStream.Free;
  FOpStream := TClassStream.Create(FileStream, l);

  FileStream.ReadBuffer(l, SizeOf(l));
  Table := TClassStream.Create(FileStream, l);
  SetLength(Sizes, Table.ReadInteger);
  for I := 0 to High(Sizes) do
    Sizes[I] := Table.ReadInteger;
  Table.Free;
  SetLength(FTable, Length(Sizes));
  for I := 0 to High(FTable) do
  begin
    FTable[I].Instance := nil;
    FTable[I].Stream := TClassStream.Create(FileStream, Sizes[I]);
  end;
  FileStream.Free;
end;

function TOpCache.Save(Instance: TObject; Serialize: TSerializer): Integer;
begin
  Result := FindId(Instance);
  if Result < 0 then
  begin
    SetLength(FTable, Length(FTable) + 1);
    Result := High(FTable);
    FTable[Result].Instance := Instance;
    FTable[Result].Stream := Serialize(Self);
  end;
end;

procedure TOpCache.SaveToFile(Path: string);
var
  FileStream: TFileStream;
  Table: TClassStream;
  l: Int64;
  I: Integer;
begin
  if FileExists(Path) then
    FileStream := TFileStream.Create(Path, fmOpenReadWrite)
  else
    FileStream := TFileStream.Create(Path, fmCreate or fmOpenReadWrite);
  FileStream.WriteBuffer(HEADER, Length(HEADER));

  l := FOpStream.Size;
  FileStream.WriteBuffer(l, SizeOf(l));
  FOpStream.SaveTo(FileStream);

  Table := TClassStream.Create;
  Table.WriteInteger(Length(FTable));
  for I := 0 to High(FTable) do
    Table.WriteInteger(FTable[I].Stream.Size);

  l := Table.Size;
  FileStream.WriteBuffer(l, SizeOf(l));
  Table.SaveTo(FileStream);

  Table.Free;

  for I := 0 to High(FTable) do
    FTable[I].Stream.SaveTo(FileStream);

  FileStream.Size := FileStream.Position;
  FileStream.Free;
end;

function TOpCache.Load(Id: Integer): TObject;
var
  Uid: Byte;
begin
  if Id < 0 then
  begin
    Result := nil;
    Exit;
  end;
  if FTable[Id].Instance = nil then
  begin
    Uid := FTable[Id].Stream.ReadByte;
    case Uid and $F0 of
      UID_DATA:
        LoadData(Uid, FTable[Id].Stream, Self, FTable[Id].Instance);
      UID_FUNCTION:
        LoadFunction(Uid, FTable[Id].Stream, Self, FRepositories, FTable[Id].Instance);
    else
      raise Exception.Create('Error Message');
    end;
  end;
  Result := FTable[Id].Instance;
end;

end.
