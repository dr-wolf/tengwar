unit uFuncUDPSend;

interface

uses
  uContext, uStreamRepository, uFuncUDP;

type
  TFuncUDPSend = class(TFuncUDP)
  public
    constructor Create(StreamRepository: TStreamRepository);
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Read bytes from file and sends them to remote IP and port.
    @type func(ip: string, port: int, file: int)
    @in ip string # Remote IP
    @in port int # Remote port
    @in file int # File handle
    * }

implementation

uses SysUtils, Classes, BlckSock, uTypeFactory, uDataNumeric, uExceptions, uOpCode;

{ TFuncUDPSend }

constructor TFuncUDPSend.Create(StreamRepository: TStreamRepository);
begin
  inherited Create([TypeFactory.GetString, TypeFactory.GetInt, TypeFactory.GetInt], ['ip', 'port', 'file'], 3);
  FFuncUid := FID_UDP_SEND;
  FStreamRepository := StreamRepository;
end;

procedure TFuncUDPSend.Execute(Context: TContext);
var
  ip: string;
  port: Int64;
  sock: TUDPBlockSocket;
  stream: TStream;
begin
  ip := RetrieveIP('ip', Context);
  port := RetrievePort('port', Context);
  stream := RetrieveStream('file', Context);
  stream.Seek(0, soFromBeginning);
  sock := TUDPBlockSocket.Create;
  with sock do
  begin
    CreateSocket;
    Connect(ip, IntToStr(port));
    SendStreamRaw(stream);
    if LastError <> 0 then
      raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataNumeric.Create(sock.LastError));
    CloseSocket;
    Free;
  end;
end;

end.
