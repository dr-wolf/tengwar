unit uFuncUDPClose;

interface

uses
  uContext, uSocketRepository, uFuncUDP, BlckSock;

type
  TFuncUDPClose = class(TFuncUDP)
  public
    constructor Create(SocketRepository: TSocketRepository<TUDPBlockSocket>);
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Unbinds and closes a socket.
    @type func(socket: int)
    @in socket int # Socket handle
    * }

implementation

uses uTypeFactory, uDataNumeric, uOpCode;

{ TFuncUDPClose }

constructor TFuncUDPClose.Create(SocketRepository: TSocketRepository<TUDPBlockSocket>);
begin
  inherited Create([TypeFactory.GetInt], ['socket'], 1);
  FFuncUid := FID_UDP_CLOSE;
  FSocketRepository := SocketRepository;
end;

procedure TFuncUDPClose.Execute(Context: TContext);
begin
  FSocketRepository.Close(AsInt(Context.Data['socket']));
end;

end.
