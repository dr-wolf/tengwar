unit uFuncUDPBind;

interface

uses
  uContext, uSocketRepository, uFuncUDP, BlckSock;

type
  TFuncUDPBind = class(TFuncUDP)
  public
    constructor Create(SocketRepository: TSocketRepository<TUDPBlockSocket>);
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Opens a listening socket on given port and interface.
    @type func(ip: string, port: int -> int)
    @in ip string # Interface to listen to
    @in port int # Listening port
    @out - int # Handle of a socket
    * }

implementation

uses SysUtils, uTypeFactory, uDataNumeric, uDataString, uExceptions, uOpCode;

{ TFuncUDPBind }

constructor TFuncUDPBind.Create(SocketRepository: TSocketRepository<TUDPBlockSocket>);
begin
  inherited Create([TypeFactory.GetString, TypeFactory.GetInt, TypeFactory.GetInt], ['ip', 'port', 'socket'], 2);
  FFuncUid := FID_UDP_BIND;
  FSocketRepository := SocketRepository;
end;

procedure TFuncUDPBind.Execute(Context: TContext);
var
  ip: string;
  port: Int64;
  sock: TUDPBlockSocket;
begin
  ip := RetrieveIP('ip', Context);
  port := RetrievePort('port', Context);
  sock := TUDPBlockSocket.Create;
  with sock do
  begin
    CreateSocket;
    SetLinger(true, 10000);
    Bind(ip, IntToStr(port));
  end;
  if sock.LastError <> 0 then
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create(sock.LastErrorDesc));

  Context.Data['socket'] := TDataNumeric.Create(FSocketRepository.Add(sock));
end;

end.
