unit uFuncUDPReceive;

interface

uses
  uContext, uSocketRepository, uStreamRepository, uFuncUDP, BlckSock;

type
  TFuncUDPReceive = class(TFuncUDP)
  public
    constructor Create(SocketRepository: TSocketRepository<TUDPBlockSocket>; StreamRepository: TStreamRepository);
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Receives a stream from a socket adn saves it ot file.
    @type func(socket: int, file: int, timeout: int)
    @in socket int # Socket handle
    @in file int # File handle
    @in timeout int # Read timeout
    * }

implementation

uses Classes, uTypeFactory, uDataNumeric, uDataString, uExceptions, uOpCode;

{ TFuncUDPReceive }

constructor TFuncUDPReceive.Create(SocketRepository: TSocketRepository<TUDPBlockSocket>;
  StreamRepository: TStreamRepository);
begin
  inherited Create([TypeFactory.GetInt, TypeFactory.GetInt, TypeFactory.GetInt], ['socket', 'file', 'timeout'], 3);
  FFuncUid := FID_UDP_RECEIVE;
  FSocketRepository := SocketRepository;
  FStreamRepository := StreamRepository;
end;

procedure TFuncUDPReceive.Execute(Context: TContext);
var
  sock: TUDPBlockSocket;
  stream: TStream;
  oldp: Int64;
begin
  sock := RetrieveSocket('socket', Context);
  stream := RetrieveStream('file', Context);
  oldp := stream.Position;
  sock.RecvStreamRaw(stream, AsInt(Context.Data['timeout']));
  if (sock.LastError <> 0) and (oldp = stream.Position) then
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create(sock.LastErrorDesc));
end;

end.
