unit uFuncUDP;

interface

uses
  uStandardFunc, uContext, uSocketRepository, uStreamRepository, BlckSock, Classes;

type
  TFuncUDP = class(TStandardFunc)
  protected
    FSocketRepository: TSocketRepository<TUDPBlockSocket>;
    FStreamRepository: TStreamRepository;
    function RetrieveSocket(VarName: string; Context: TContext): TUDPBlockSocket;
    function RetrieveStream(VarName: string; Context: TContext): TStream;
    function RetrieveIP(VarName: string; Context: TContext): string;
    function RetrievePort(VarName: string; Context: TContext): Int64;
  end;

implementation

uses SynAIP, uDataString, uDataNumeric, uExceptions;

{ TFuncUDP }

function TFuncUDP.RetrieveIP(VarName: string; Context: TContext): string;
begin
  Result := AsString(Context.Data[VarName]);
  if not IsIP(Result) and not IsIP6(Result) then
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create('Invalid IP address!'));
end;

function TFuncUDP.RetrievePort(VarName: string; Context: TContext): Int64;
begin
  Result := AsInt(Context.Data[VarName]);
  if (Result < 0) or (Result > 65535) then
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create('Invalid port number!'));
end;

function TFuncUDP.RetrieveSocket(VarName: string; Context: TContext): TUDPBlockSocket;
begin
  Result := FSocketRepository.Sock[AsInt(Context.Data[VarName])];
  if Result = nil then
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create('Invalid socket handle!'));
end;

function TFuncUDP.RetrieveStream(VarName: string; Context: TContext): TStream;
begin
  Result := FStreamRepository.Stream[AsInt(Context.Data[VarName])];
  if Result = nil then
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create('Invalid file handle!'));
end;

end.
