unit uFuncUDPBroadcast;

interface

uses
  uContext, uStreamRepository, uFuncUDP;

type
  TFuncUDPBroadcast = class(TFuncUDP)
  public
    constructor Create(StreamRepository: TStreamRepository);
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Read bytes from file and sends them as a broadcast message.
    @type func(ip: string, port: int, file: int)
    @in port int # Remote port
    @in file int # File handle
    * }

implementation

uses SysUtils, Classes, BlckSock, uTypeFactory, uDataNumeric, uExceptions, uOpCode;

{ TFuncUDPBroadcast }

constructor TFuncUDPBroadcast.Create(StreamRepository: TStreamRepository);
begin
  inherited Create([TypeFactory.GetInt, TypeFactory.GetInt], ['port', 'file'], 2);
  FFuncUid := FID_UDP_BROADCAST;
  FStreamRepository := StreamRepository;
end;

procedure TFuncUDPBroadcast.Execute(Context: TContext);
var
  port: Int64;
  sock: TUDPBlockSocket;
  stream: TStream;
begin
  port := RetrievePort('port', Context);
  stream := RetrieveStream('file', Context);
  stream.Seek(0, soFromBeginning);
  sock := TUDPBlockSocket.Create;
  with sock do
  begin
    CreateSocket;
    EnableBroadcast(True);
    Connect(cBroadcast, IntToStr(port));
    SendStreamRaw(stream);
    if LastError <> 0 then
      raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataNumeric.Create(sock.LastError));
    CloseSocket;
    Free;
  end;
end;

end.
