unit uFuncStrAscii;

interface

uses
  uStandardFunc, uContext;

type
  TFuncStrAscii = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Explodes string into list of ascii char codes.
    @type func(text: string -> list(int))
    @in text string # Incoming string
    @out - list(int) # Returned list of ints
    * }

implementation

uses uArray, uTypeFactory, uData, uDataArray, uDataNumeric, uDataString, uOpCode;

{ TFuncStrAscii }

constructor TFuncStrAscii.Create;
begin
  inherited Create([TypeFactory.GetString, TypeFactory.GetList(TypeFactory.GetInt)], ['text', 'result'], 1);
  FFuncUid := FID_STR_ASCII;
end;

procedure TFuncStrAscii.Execute(Context: TContext);
var
  a: TArray<IData>;
  i: Integer;
  s: string;
begin
  s := AsString(Context.Data['text']);
  SetLength(a, Length(s));
  for i := 1 to Length(s) do
    a[i - 1] := TDataNumeric.Create(Ord(s[i]));
  Context.Data['result'] := TDataArray.Create(a);
end;

end.
