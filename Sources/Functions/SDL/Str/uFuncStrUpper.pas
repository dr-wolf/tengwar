unit uFuncStrUpper;

interface

uses
  uStandardFunc, uContext;

type
  TFuncStrUpper = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Converts string to upper case.
    @type func(text: string -> string)
    @in text string # Initial text
    @out - string # Text in upper case
    * }

implementation

uses Character, uTypeFactory, uDataString, uOpCode;

{ TFuncStrUpper }

constructor TFuncStrUpper.Create;
begin
  inherited Create([TypeFactory.GetString, TypeFactory.GetString], ['text', 'result'], 1);
  FFuncUid := FID_STR_UPPER;
end;

procedure TFuncStrUpper.Execute(Context: TContext);
begin
  Context.Data['result'] := TDataString.Create(ToUpper(AsString(Context.Data['text'])));
end;

end.
