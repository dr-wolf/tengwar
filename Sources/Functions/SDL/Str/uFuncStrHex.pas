unit uFuncStrHex;

interface

uses
  uStandardFunc, uContext;

type
  TFuncStrHex = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Converts int into hexadecimal string representation.
    @type func(value: int, width: int -> string)
    @in value int # Initial value
    @in width int # Hex digit width
    @out - string # Hexadecimal representation
    * }

implementation

uses SysUtils, uTypeFactory, uDataNumeric, uDataString, uOpCode;

{ TFuncStrHex }

constructor TFuncStrHex.Create;
begin
  inherited Create([TypeFactory.GetInt, TypeFactory.GetInt, TypeFactory.GetString], ['value', 'width', 'result'], 2);
  FFuncUid := FID_STR_HEX;
end;

procedure TFuncStrHex.Execute(Context: TContext);
begin
  Context.Data['result'] := TDataString.Create(IntToHex(AsInt(Context.Data['value']), AsInt(Context.Data['width'])));
end;

end.
