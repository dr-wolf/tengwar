unit uFuncStrSub;

interface

uses
  uStandardFunc, uContext;

type
  TFuncStrSub = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Returns a substring from a string by given position and length.
    @type func(text: string, pos: int, len: int -> string)
    @in text string # Initial text
    @in pos int # Position of a substring
    @in len int # Length of a substring
    @out - string # Substring
    * }

implementation

uses uTypeFactory, uDataString, uDataNumeric, uOpCode;

{ TFuncStrSub }

constructor TFuncStrSub.Create;
begin
  inherited Create([TypeFactory.GetString, TypeFactory.GetInt, TypeFactory.GetInt, TypeFactory.GetString],
    ['text', 'pos', 'len', 'result'], 3);
  FFuncUid := FID_STR_SUB;
end;

procedure TFuncStrSub.Execute(Context: TContext);
begin
  Context.Data['result'] := TDataString.Create(Copy(AsString(Context.Data['text']), AsInt(Context.Data['pos']) + 1,
    AsInt(Context.Data['len'])));
end;

end.
