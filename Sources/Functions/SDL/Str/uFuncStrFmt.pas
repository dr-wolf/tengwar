unit uFuncStrFmt;

interface

uses
  uStandardFunc, uContext;

type
  TFuncStrFmt = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Converts float value into string using given string template.
    @type func(format: string, values: list(any) -> string)
    @in format string # Format template
    @in values list(any) # List of values
    @out - string # Formatted value
    * }

implementation

uses SysUtils, uTypeFactory, uDataNumeric, uDataString, uOpCode;

{ TFuncStrFmt }

constructor TFuncStrFmt.Create;
begin
  inherited Create([TypeFactory.GetString, TypeFactory.GetList(TypeFactory.GetAny), TypeFactory.GetString],
    ['format', 'values', 'result'], 2);
  FFuncUid := FID_STR_FMT;
end;

procedure TFuncStrFmt.Execute(Context: TContext);

  function Chop(p: Integer; var s: string): string;
  begin
    Result := Copy(s, 1, p);
    Delete(s, 1, p);
  end;

  function ChopStr(var s: string): string;
  var
    p: Integer;
  begin
    p := Pos('%', s + '%');
    Result := Chop(p - 1, s);
    Delete(s, 1, 1);
  end;

  function ChopTmp(var s, t: string; var p: Integer): Boolean;

    function ChopNmb(var s: string): string;
    var
      p: Integer;
    begin
      p := 1;
      while (p <= Length(s)) and (s[p] in ['0' .. '9']) do
        Inc(p);
      Result := Chop(p - 1, s);
    end;

    function ChopSmb(var s: string; c: Char): string;
    begin
      if (s <> '') and (s[1] = c) then
      begin
        Result := Copy(s, 1, 1);
        Delete(s, 1, 1);
      end;
    end;

  var
    b, n: string;
  begin
    b := s;
    n := ChopNmb(s);
    if ChopSmb(s, ':') = ':' then
      p := StrToInt(n)
    else
      t := t + n;
    t := t + ChopSmb(s, '-') + ChopNmb(s) + ChopSmb(s, '.') + ChopNmb(s);
    if s <> '' then
    begin
      Result := s[1] in ['d', 'e', 'f', 'g', 'm', 'n', 's', 'u', 'x'];
      t := t + s[1];
      Delete(s, 1, 1);
    end
    else
      Result := False;
    if not Result then
      s := b;
  end;

var
  i, n: Integer;
  f, r, t: string;
begin
  f := AsString(Context.Data['format']);
  r := '';
  i := 0;
  while f <> '' do
  begin
    r := r + ChopStr(f);
    if f = '' then
      Break;
    if f[1] = '%' then
    begin
      r := r + '%';
      Delete(f, 1, 1);
      Continue;
    end;
    t := '%';
    n := i;
    Inc(i);
    if ChopTmp(f, t, n) then
      case t[Length(t)] of
        'd', 'u', 'x':
          r := r + Format(t, [AsInt(Context.Data['values'].Item(n))]);
        'e', 'f', 'g', 'm', 'n':
          r := r + Format(t, [AsFloat(Context.Data['values'].Item(n))]);
        's':
          r := r + Format(t, [AsString(Context.Data['values'].Item(n))]);
      end;
  end;
  Context.Data['result'] := TDataString.Create(r);
end;

end.
