unit uFuncStrPos;

interface

uses
  uStandardFunc, uContext;

type
  TFuncStrPos = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Finds position oof subtext in given text, returns -1 if no subtext found.
    @type func(subtext: string, text: string -> int)
    @in subtext string # Subtext to find in text
    @in text string # Initial text
    @out - int # Position of subtext
    * }

implementation

uses uTypeFactory, uDataNumeric, uDataString, uOpCode;

{ TFuncStrPos }

constructor TFuncStrPos.Create;
begin
  inherited Create([TypeFactory.GetString, TypeFactory.GetString, TypeFactory.GetInt],
    ['subtext', 'text', 'result'], 2);
  FFuncUid := FID_STR_POS;
end;

procedure TFuncStrPos.Execute(Context: TContext);
begin
  Context.Data['result'] := TDataNumeric.Create(Pos(AsString(Context.Data['subtext']),
    AsString(Context.Data['text'])) - 1);
end;

end.
