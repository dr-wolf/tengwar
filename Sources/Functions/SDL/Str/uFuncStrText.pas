unit uFuncStrText;

interface

uses
  uStandardFunc, uContext;

type
  TFuncStrText = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Builds a string from a list of int ascii codes. Opposite function to str::ascii.
    @type func(bytes: list(int) -> string)
    @in bytes list(int) # List of ascii codes
    @out - string # String
    * }

implementation

uses uTypeFactory, uDataArray, uDataNumeric, uDataString, uOpCode;

{ TFuncStrText }

constructor TFuncStrText.Create;
begin
  inherited Create([TypeFactory.GetList(TypeFactory.GetInt), TypeFactory.GetString], ['bytes', 'result'], 1);
  FFuncUid := FID_STR_TEXT;
end;

procedure TFuncStrText.Execute(Context: TContext);
var
  a: TDataArray;
  i: Integer;
  s: string;
begin
  s := '';
  a := Context.Data['bytes'] as TDataArray;
  for i := 0 to a.Count - 1 do
    s := s + Chr(AsInt(a.Item(i)));
  Context.Data['result'] := TDataString.Create(s);
end;

end.
