unit uFuncStrLen;

interface

uses
  uStandardFunc, uContext;

type
  TFuncStrLen = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Retruns length of a string.
    @type func(text: string -> int)
    @in text string # String value
    @out - int # String length
    * }

implementation

uses uTypeFactory, uDataNumeric, uDataString, uOpCode;

{ TFuncStrLen }

constructor TFuncStrLen.Create;
begin
  inherited Create([TypeFactory.GetString, TypeFactory.GetInt], ['text', 'len'], 1);
  FFuncUid := FID_STR_LEN;
end;

procedure TFuncStrLen.Execute(Context: TContext);
begin
  Context.Data['len'] := TDataNumeric.Create(Length(AsString(Context.Data['text'])));
end;

end.
