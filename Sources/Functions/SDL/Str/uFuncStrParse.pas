unit uFuncStrParse;

interface

uses
  uStandardFunc, uContext;

type
  TFuncStrParse = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Parses float from string, throws exception if no float value is found.
    @type func(text: string -> float)
    @in text string # String representstion of float value
    @out - float # Parsed value
    * }

implementation

uses uTypeFactory, uDataNumeric, uDataString, uOpCode;

{ TFuncStrParse }

constructor TFuncStrParse.Create;
begin
  inherited Create([TypeFactory.GetString, TypeFactory.GetFloat], ['text', 'number'], 1);
  FFuncUid := FID_STR_PARSE;
end;

procedure TFuncStrParse.Execute(Context: TContext);
begin
  Context.Data['number'] := TDataNumeric.Create(AsString(Context.Data['text']));
end;

end.
