unit uFuncStrLower;

interface

uses
  uStandardFunc, uContext;

type
  TFuncStrLower = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Converts string to lower case.
    @type func(text: string -> string)
    @in text string # Initial text
    @out - string # Text in lower case
    * }

implementation

uses Character, uTypeFactory, uDataString, uOpCode;

{ TFuncStrLower }

constructor TFuncStrLower.Create;
begin
  inherited Create([TypeFactory.GetString, TypeFactory.GetString], ['text', 'result'], 1);
  FFuncUid := FID_STR_LOWER;
end;

procedure TFuncStrLower.Execute(Context: TContext);
begin
  Context.Data['result'] := TDataString.Create(ToLower(AsString(Context.Data['text'])));
end;

end.
