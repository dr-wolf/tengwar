unit uFuncFSRmDir;

interface

uses
  uStandardFunc, uContext;

type
  TFuncFSRmDir = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Deletes empty directory.
    @type func(path: string)
    @in path string # Path of directory to be deleted
    * }

implementation

uses SysUtils, uTypeFactory, uDataString, uOpCode;

{ TFuncFSRmDir }

constructor TFuncFSRmDir.Create;
begin
  inherited Create([TypeFactory.GetString], ['path'], 1);
  FFuncUid := FID_FS_RMDIR;
end;

procedure TFuncFSRmDir.Execute(Context: TContext);
begin
  if not RemoveDir(ExpandFileName(AsString(Context.Data['path']))) then
    RaiseLastOSError;
end;

end.
