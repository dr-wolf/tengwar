unit uFuncFSMkDir;

interface

uses
  uStandardFunc, uContext;

type
  TFuncFSMkDir = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Creates directories by given path.
    @type func(path: string)
    @in path string # Path to create
    * }

implementation

uses SysUtils, uTypeFactory, uDataString, uOpCode;

{ TFuncFSMkDir }

constructor TFuncFSMkDir.Create;
begin
  inherited Create([TypeFactory.GetString], ['path'], 1);
  FFuncUid := FID_FS_MKDIR;
end;

procedure TFuncFSMkDir.Execute(Context: TContext);
begin
  ForceDirectories(ExpandFileName(AsString(Context.Data['path'])));
end;

end.
