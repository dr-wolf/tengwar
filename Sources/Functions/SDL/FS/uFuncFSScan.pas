unit uFuncFSScan;

interface

uses
  uStandardFunc, uContext;

type
  TFuncFSScan = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr List files and directories inside the specified path.
    @type func(path: string -> dirs: list(string), files: list(string))
    @in path string # Path to be scanned
    @out dirs list(string) # List of directories
    @out files list(string) # List of files
    * }

implementation

uses SysUtils, uArray, uExceptions, uTypeFactory, uData, uDataArray, uDataString, uOpCode;

{ TFuncFSScan }

constructor TFuncFSScan.Create;
begin
  inherited Create([TypeFactory.GetString, TypeFactory.GetList(TypeFactory.GetString),
    TypeFactory.GetList(TypeFactory.GetString)], ['path', 'dirs', 'files'], 1);
  FFuncUid := FID_FS_SCAN;
end;

procedure TFuncFSScan.Execute(Context: TContext);

  procedure AppendA(var A: TArray<IData>; D: IData);
  begin
    SetLength(A, Length(A) + 1);
    A[High(A)] := D;
  end;

var
  Path: string;
  dirs, files: TArray<IData>;
  r: TSearchRec;
begin
  Path := AsString(Context.Data['path']);
  if not DirectoryExists(Path) then
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create('Directory "' + Path + '" does not exists!'));
  SetLength(dirs, 0);
  SetLength(files, 0);
  if FindFirst(Path + '/*', faAnyFile, r) = 0 then
    repeat
      if r.Attr and faDirectory = faDirectory then
        AppendA(dirs, TDataString.Create(r.Name))
      else
        AppendA(files, TDataString.Create(r.Name));
    until FindNext(r) <> 0;
  FindClose(r);
  Context.Data['dirs'] := TDataArray.Create(dirs);
  Context.Data['files'] := TDataArray.Create(files);
end;

end.
