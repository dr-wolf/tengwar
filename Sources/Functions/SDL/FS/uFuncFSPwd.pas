unit uFuncFSPwd;

interface

uses
  uStandardFunc, uContext;

type
  TFuncFSPwd = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

    { *
    @descr Returns current directory.
    @type func(-> string)
    @out - string # Current directory
    * }

implementation

uses SysUtils, uTypeFactory, uDataString, uOpCode;

{ TFuncFSPwd }

constructor TFuncFSPwd.Create;
begin
  inherited Create([TypeFactory.GetString], ['result'], 0);
  FFuncUid := FID_FS_PWD;
end;

procedure TFuncFSPwd.Execute(Context: TContext);
begin
  Context.Data['result'] := TDataString.Create(GetCurrentDir);
end;

end.
