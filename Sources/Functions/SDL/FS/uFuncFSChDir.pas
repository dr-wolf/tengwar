unit uFuncFSChDir;

interface

uses
  uStandardFunc, uContext;

type
  TFuncFSChDir = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

    { *
    @descr Changes current directory.
    @type func(path: string)
    @in path string # New path
    * }

implementation

uses SysUtils, uTypeFactory, uDataString, uOpCode;

{ TFuncFSChDir }

constructor TFuncFSChDir.Create;
begin
  inherited Create([TypeFactory.GetString], ['path'], 1);
  FFuncUid := FID_FS_CHDIR;
end;

procedure TFuncFSChDir.Execute(Context: TContext);
begin
  if not SetCurrentDir(AsString(Context.Data['path'])) then
    RaiseLastOSError;
end;

end.
