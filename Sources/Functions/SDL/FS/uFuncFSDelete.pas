unit uFuncFSDelete;

interface

uses
  uStandardFunc, uContext;

type
  TFuncFSDelete = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Deletes file from filesystem.
    @type func(path: string)
    @in path string # Path to file to be deleted
    * }

implementation

uses SysUtils, uTypeFactory, uDataString, uOpCode;

{ TFuncFSDelete }

constructor TFuncFSDelete.Create;
begin
  inherited Create([TypeFactory.GetString], ['path'], 1);
  FFuncUid := FID_FS_DELETE;
end;

procedure TFuncFSDelete.Execute(Context: TContext);
begin
  if not DeleteFile(ExpandFileName(AsString(Context.Data['path']))) then
    RaiseLastOSError;
end;

end.
