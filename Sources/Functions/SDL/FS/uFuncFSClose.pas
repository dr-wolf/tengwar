unit uFuncFSClose;

interface

uses
  uStandardFunc, uContext, uStreamRepository;

type
  TFuncFSClose = class(TStandardFunc)
  public
    constructor Create(StreamRepository: TStreamRepository);
  private
    FStreamRepository: TStreamRepository;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Closes file handle.
    @type func(handle: int)
    @in handle int # File handle
    * }

implementation

uses uTypeFactory, uDataNumeric, uOpCode;

{ TFuncFSClose }

constructor TFuncFSClose.Create(StreamRepository: TStreamRepository);
begin
  inherited Create([TypeFactory.GetInt], ['handle'], 1);
  FFuncUid := FID_FS_CLOSE;
  FStreamRepository := StreamRepository;
end;

procedure TFuncFSClose.Execute(Context: TContext);
begin
  FStreamRepository.Close(AsInt(Context.Data['handle']));
end;

end.
