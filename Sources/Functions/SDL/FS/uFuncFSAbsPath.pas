unit uFuncFSAbsPath;

interface

uses
  uStandardFunc, uContext;

type
  TFuncFSAbsPath = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Returns canonicalized absolute pathname from given string.
    @type func(path: string -> string)
    @in path string # Initial path
    @out - string # Absolute path
    * }

implementation

uses SysUtils, uTypeFactory, uDataString, uOpCode;

{ TFuncFSAbsPath }

constructor TFuncFSAbsPath.Create;
begin
  inherited Create([TypeFactory.GetString, TypeFactory.GetString], ['path', 'result'], 1);
  FFuncUid := FID_FS_ABSPATH;
end;

procedure TFuncFSAbsPath.Execute(Context: TContext);
begin
  Context.Data['result'] := TDataString.Create(ExpandFileName(AsString(Context.Data['path'])));
end;

end.
