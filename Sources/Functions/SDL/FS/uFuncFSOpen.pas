unit uFuncFSOpen;

interface

uses
  uStandardFunc, uContext, uStreamRepository;

type
  TFuncFSOpen = class(TStandardFunc)
  public
    constructor Create(StreamRepository: TStreamRepository);
  private
    FStreamRepository: TStreamRepository;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Opens file. If file does not exist it will be created. If no path specified in-memory file will be created.
    @type func(path: string -> int)
    @in path string # Path to the file, pass empty string to create in-memory file
    @out - int # File handle
    * }

implementation

uses SysUtils, Classes, uTypeFactory, uDataNumeric, uDataString, uOpCode;

{ TFuncFSOpen }

constructor TFuncFSOpen.Create(StreamRepository: TStreamRepository);
begin
  inherited Create([TypeFactory.GetString, TypeFactory.GetInt], ['filename', 'handle'], 1);
  FFuncUid := FID_FS_OPEN;
  FStreamRepository := StreamRepository;
end;

procedure TFuncFSOpen.Execute(Context: TContext);
var
  fname: string;
  stream: TStream;
begin
  fname := AsString(Context.Data['filename']);
  if fname <> '' then
  begin
    fname := ExpandFileName(fname);
    if FileExists(fname) then
      stream := TFileStream.Create(fname, fmOpenReadWrite)
    else
      stream := TFileStream.Create(fname, fmCreate or fmOpenReadWrite);
  end
  else
    stream := TMemoryStream.Create;
  Context.Data['handle'] := TDataNumeric.Create(FStreamRepository.Add(stream, fname));
end;

end.
