unit uFuncFSStat;

interface

uses
  uStandardFunc, uContext, uType;

type
  TFuncFSStat = class(TStandardFunc)
  public
    constructor Create(StatType: TType);
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Returns file attributes and size.
    @type func(path: string -> tfilestat)
    @in path string # Initial path
    @out - tfilestat # File size and attributes
    * }

implementation

uses SysUtils, uTypeFactory, uDataArray, uDataNumeric, uDataString, uOpCode;

function FileSize(Path: string): Int64;
var
  f: THandle;
begin
  f := FileOpen(Path, fmOpenRead);
  Result := FileSeek(f, Int64(0), 2);
  FileClose(f);
end;

{ TFuncFSStat }

constructor TFuncFSStat.Create(StatType: TType);
begin
  inherited Create([TypeFactory.GetString, StatType], ['path', 'stat'], 1);
  FFuncUid := FID_FS_STAT;
end;

procedure TFuncFSStat.Execute(Context: TContext);
var
  Path: string;
begin
  Path := ExpandFileName(AsString(Context.Data['path']));
  Context.Data['stat'] := TDataArray.Create([TDataNumeric.Create(FileSize(Path)),
    TDataNumeric.Create(FileGetAttr(Path))]);
end;

end.
