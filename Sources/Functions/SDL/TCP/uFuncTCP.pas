unit uFuncTCP;

interface

uses
  uStandardFunc, uContext, uSocketRepository, uStreamRepository, BlckSock, Classes;

type
  TFuncTCP = class(TStandardFunc)
  protected
    FSocketRepository: TSocketRepository<TTCPBlockSocket>;
    FStreamRepository: TStreamRepository;
    function RetrieveSocket(VarName: string; Context: TContext): TTCPBlockSocket;
    function RetrieveStream(VarName: string; Context: TContext): TStream;
    function RetrieveIP(VarName: string; Context: TContext): string;
    function RetrievePort(VarName: string; Context: TContext): Int64;
  end;

implementation

uses SynAIP, uDataNumeric, uDataString, uExceptions;

{ TFuncTCP }

function TFuncTCP.RetrieveIP(VarName: string; Context: TContext): string;
begin
  Result := AsString(Context.Data[VarName]);
  if not IsIP(Result) and not IsIP6(Result) then
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create('Invalid IP address!'));
end;

function TFuncTCP.RetrievePort(VarName: string; Context: TContext): Int64;
begin
  Result := AsInt(Context.Data[VarName]);
  if (Result < 0) or (Result > 65535) then
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create('Invalid port number!'));
end;

function TFuncTCP.RetrieveSocket(VarName: string; Context: TContext): TTCPBlockSocket;
begin
  Result := FSocketRepository.Sock[AsInt(Context.Data[VarName])];
  if Result = nil then
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create('Invalid socket handle!'));
end;

function TFuncTCP.RetrieveStream(VarName: string; Context: TContext): TStream;
begin
  Result := FStreamRepository.Stream[AsInt(Context.Data[VarName])];
  if Result = nil then
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create('Invalid file handle!'));
end;

end.
