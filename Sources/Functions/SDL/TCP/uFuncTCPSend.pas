unit uFuncTCPSend;

interface

uses
  uContext, uSocketRepository, uStreamRepository, uFuncTCP, BlckSock;

type
  TFuncTCPSend = class(TFuncTCP)
  public
    constructor Create(SocketRepository: TSocketRepository<TTCPBlockSocket>; StreamRepository: TStreamRepository);
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Read bytes from file and sends them to socket.
    @type func(socket: int, file: int)
    @in socket int # Socket handle
    @in file int # File handle
    * }

implementation

uses Classes, uTypeFactory, uDataString, uExceptions, uOpCode;

{ TFuncTCPSend }

constructor TFuncTCPSend.Create(SocketRepository: TSocketRepository<TTCPBlockSocket>;
  StreamRepository: TStreamRepository);
begin
  inherited Create([TypeFactory.GetInt, TypeFactory.GetInt], ['socket', 'file'], 2);
  FFuncUid := FID_TCP_SEND;
  FSocketRepository := SocketRepository;
  FStreamRepository := StreamRepository;
end;

procedure TFuncTCPSend.Execute(Context: TContext);
var
  sock: TTCPBlockSocket;
  stream: TStream;
begin
  sock := RetrieveSocket('socket', Context);
  stream := RetrieveStream('file', Context);
  stream.Seek(0, soFromBeginning);
  sock.SendStreamRaw(stream);
  if sock.LastError <> 0 then
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create(sock.LastErrorDesc));
end;

end.
