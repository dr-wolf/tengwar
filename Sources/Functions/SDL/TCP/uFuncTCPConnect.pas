unit uFuncTCPConnect;

interface

uses
  uContext, uSocketRepository, uFuncTCP, BlckSock;

type
  TFuncTCPConnect = class(TFuncTCP)
  public
    constructor Create(SocketRepository: TSocketRepository<TTCPBlockSocket>);
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Connects to remote IP abd port.
    @type func(ip: string, port: int -> int)
    @in ip string # Remote IP
    @in port int # Remote port
    @out - int # Handle of a socket
    * }

implementation

uses SysUtils, uTypeFactory, uDataNumeric, uDataString, uExceptions, uOpCode;

{ TFuncTCPConnect }

constructor TFuncTCPConnect.Create(SocketRepository: TSocketRepository<TTCPBlockSocket>);
begin
  inherited Create([TypeFactory.GetString, TypeFactory.GetInt, TypeFactory.GetInt], ['ip', 'port', 'socket'], 2);
  FFuncUid := FID_TCP_CONNECT;
  FSocketRepository := SocketRepository;
end;

procedure TFuncTCPConnect.Execute(Context: TContext);
var
  ip: string;
  port: Int64;
  sock: TTCPBlockSocket;
begin
  ip := RetrieveIP('ip', Context);
  port := RetrievePort('port', Context);
  sock := TTCPBlockSocket.Create;
  sock.Connect(ip, IntToStr(port));
  if sock.LastError <> 0 then
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create(sock.LastErrorDesc));
  Context.Data['socket'] := TDataNumeric.Create(FSocketRepository.Add(sock));
end;

end.
