unit uFuncTCPAccept;

interface

uses
  uContext, uSocketRepository, uFuncTCP, BlckSock;

type
  TFuncTCPAccept = class(TFuncTCP)
  public
    constructor Create(SocketRepository: TSocketRepository<TTCPBlockSocket>);
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Accepts incoming connection on listening socket.
    @type func(socket: int, timeout: int -> int)
    @in socket int # Listening socket handle
    @in timeout int # Time waiting for incoming connection
    @out - int # Handle of socket with connected client or 0
    * }

implementation

uses uTypeFactory, uDataNumeric, uDataString, uExceptions, SynSock, uOpCode;

{ TFuncTCPAccept }

constructor TFuncTCPAccept.Create(SocketRepository: TSocketRepository<TTCPBlockSocket>);
begin
  inherited Create([TypeFactory.GetInt, TypeFactory.GetInt, TypeFactory.GetInt], ['socket', 'timeout', 'client'], 2);
  FFuncUid := FID_TCP_ACCEPT;
  FSocketRepository := SocketRepository;
end;

procedure TFuncTCPAccept.Execute(Context: TContext);
var
  sock, client: TTCPBlockSocket;
  cs: TSocket;
  r: Integer;
begin
  sock := RetrieveSocket('socket', Context);
  if sock.CanRead(AsInt(Context.Data['timeout'])) then
  begin
    cs := sock.Accept;
    if sock.LastError = 0 then
    begin
      client := TTCPBlockSocket.Create;
      client.socket := cs;
      client.GetSins;
      r := FSocketRepository.Add(client);
    end
    else
      raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create(sock.LastErrorDesc));
  end
  else
    r := 0;
  Context.Data['client'] := TDataNumeric.Create(r);
end;

end.
