unit uFuncTCPRemote;

interface

uses
  uContext, uSocketRepository, uFuncTCP, BlckSock;

type
  TFuncTCPRemote = class(TFuncTCP)
  public
    constructor Create(SocketRepository: TSocketRepository<TTCPBlockSocket>);
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Returns remote IP and port from a socket.
    @type func(socket: int -> ip: string, port: int)
    @in socket int # Socket handle
    @out ip string # Remote IP
    @out port int # Remote port
    * }

implementation

uses uTypeFactory, uDataString, uDataNumeric, uOpCode;

{ TFuncTCPRemote }

constructor TFuncTCPRemote.Create(SocketRepository: TSocketRepository<TTCPBlockSocket>);
begin
  inherited Create([TypeFactory.GetInt, TypeFactory.GetString, TypeFactory.GetInt], ['socket', 'ip', 'port'], 1);
  FFuncUid := FID_TCP_REMOTE;
  FSocketRepository := SocketRepository;
end;

procedure TFuncTCPRemote.Execute(Context: TContext);
var
  sock: TTCPBlockSocket;
begin
  sock := RetrieveSocket('socket', Context);
  sock.GetSinRemote;
  Context.Data['ip'] := TDataString.Create(sock.GetRemoteSinIP);
  Context.Data['port'] := TDataNumeric.Create(sock.GetRemoteSinPort);
end;

end.
