unit uFuncTCPListen;

interface

uses
  uContext, uSocketRepository, uFuncTCP, BlckSock;

type
  TFuncTCPListen = class(TFuncTCP)
  public
    constructor Create(SocketRepository: TSocketRepository<TTCPBlockSocket>);
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Opens a listening socket on given port and interface.
    @type func(ip: string, port: int -> int)
    @in ip string # Interface to listen to
    @in port int # Listening port
    @out - int # Handle of a socket
    * }

implementation

uses SysUtils, uTypeFactory, uDataNumeric, uDataString, uExceptions, uOpCode;

{ TFuncTCPListen }

constructor TFuncTCPListen.Create(SocketRepository: TSocketRepository<TTCPBlockSocket>);
begin
  inherited Create([TypeFactory.GetString, TypeFactory.GetInt, TypeFactory.GetInt], ['ip', 'port', 'socket'], 2);
  FFuncUid := FID_TCP_LISTEN;
  FSocketRepository := SocketRepository;
end;

procedure TFuncTCPListen.Execute(Context: TContext);
var
  ip: string;
  port: Int64;
  sock: TTCPBlockSocket;
begin
  ip := RetrieveIP('ip', Context);
  port := RetrievePort('port', Context);
  sock := TTCPBlockSocket.Create;
  with sock do
  begin
    CreateSocket;
    SetLinger(true, 10000);
    Bind(ip, IntToStr(port));
    Listen;
  end;
  if sock.LastError <> 0 then
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create(sock.LastErrorDesc));

  Context.Data['socket'] := TDataNumeric.Create(FSocketRepository.Add(sock));
end;

end.
