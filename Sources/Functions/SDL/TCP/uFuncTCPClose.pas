unit uFuncTCPClose;

interface

uses
  uContext, uSocketRepository, uFuncTCP, BlckSock;

type
  TFuncTCPClose = class(TFuncTCP)
  public
    constructor Create(SocketRepository: TSocketRepository<TTCPBlockSocket>);
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Disconnects and closes a socket.
    @type func(socket: int)
    @in socket int # Socket handle
    * }

implementation

uses uTypeFactory, uDataNumeric, uOpCode;

{ TFuncTCPClose }

constructor TFuncTCPClose.Create(SocketRepository: TSocketRepository<TTCPBlockSocket>);
begin
  inherited Create([TypeFactory.GetInt], ['socket'], 1);
  FFuncUid := FID_TCP_CLOSE;
  FSocketRepository := SocketRepository;
end;

procedure TFuncTCPClose.Execute(Context: TContext);
begin
  FSocketRepository.Close(AsInt(Context.Data['socket']));
end;

end.
