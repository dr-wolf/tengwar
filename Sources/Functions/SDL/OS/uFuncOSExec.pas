unit uFuncOSExec;

interface

uses
  uDict, uStandardFunc, uContext;

type
  TFuncOSExec = class(TStandardFunc)
  public
    constructor Create(Environment: TDict<string>);
  private
    FEnvironment: TDict<string>;
    function EnvString(Separator: string): string;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Executes external application. Throws string exception if application can not be executed.
    @type func(command: string, wait: bool)
    @in command string # Path to executable binary with arguments
    @in wait bool # Waits for application to be closed if true
    * }

implementation

uses SysUtils, {$IFDEF FPC} Process, Classes {$ELSE} Windows {$ENDIF}, uTypeFactory, uExceptions, uDataBool,
  uDataString, uOpCode;

function ShellExec(const Command, Env: string; WaitFor: Boolean): string;
{$IFDEF FPC}
var
  P: TProcess;
  s: TStringList;
  i: Integer;
begin
  Result := '';
  try
    P := TProcess.Create(nil);
    s := TStringList.Create;
    s.Delimiter := ' ';
    s.DelimitedText := Command;
    P.Executable := s[0];
    for i := 1 to s.Count - 1 do
      P.Parameters.Add(s[i]);
    s.Free;
    P.Environment.Text := Env;
    if WaitFor then
      P.Options := P.Options + [poWaitOnExit];
    P.Execute;
    P.Free;
  except
    on E: Exception do
      Result := E.Message;
  end
{$ELSE}

var
  si: TStartupInfo;
  pi: TProcessInformation;
begin
  Result := '';
  try
    ZeroMemory(@si, SizeOf(si));
    si.cb := SizeOf(si);
    si.dwFlags := STARTF_USESHOWWINDOW;
    if CreateProcessW(nil, PChar(Command), nil, nil, False, CREATE_UNICODE_ENVIRONMENT, PChar(Env), nil, si, pi) then
    begin
      if WaitFor then
        WaitForSingleObject(pi.hProcess, INFINITE);
      CloseHandle(pi.hThread);
      CloseHandle(pi.hProcess);
    end
    else
      Result := SysErrorMessage(GetLastError());
  except
    on E: Exception do
      Result := E.Message;
  end;
{$ENDIF}
end;

{ TFuncOSExec }

constructor TFuncOSExec.Create(Environment: TDict<string>);
begin
  inherited Create([TypeFactory.GetString, TypeFactory.GetBool], ['command', 'wait'], 2);
  FFuncUid := FID_OS_EXEC;
  FEnvironment := Environment;
end;

function TFuncOSExec.EnvString(Separator: string): string;
begin
  Result := '';
  FEnvironment.Reset;
  while FEnvironment.Next do
  begin
    if Result <> '' then
      Result := Result + Separator;
    Result := Result + FEnvironment.Key + '=' + FEnvironment[FEnvironment.Key];
  end;
end;

procedure TFuncOSExec.Execute(Context: uContext.TContext);
var
  envstr, emsg: string;
begin
  envstr := EnvString({$IFDEF FPC} #$10 {$ELSE} #0 {$ENDIF});
  emsg := ShellExec(AsString(Context.Data['command']), envstr, AsBool(Context.Data['wait']));
  if emsg <> '' then
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create(emsg));
end;

end.
