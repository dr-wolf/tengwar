unit uFuncOSSleep;

interface

uses
  uStandardFunc, uContext;

type
  TFuncOSSleep = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Stops program execution for given timeout.
    @type func(timeout: int)
    @in timeout int # Milliseconds to waits
    * }

implementation

uses SysUtils, uTypeFactory, uDataNumeric, uOpCode;

{ TFuncOSSleep }

constructor TFuncOSSleep.Create;
begin
  inherited Create([TypeFactory.GetInt], ['timeout'], 1);
  FFuncUid := FID_OS_SLEEP;
end;

procedure TFuncOSSleep.Execute(Context: TContext);
begin
  Sleep(AsInt(Context.Data['timeout']));
end;

end.
