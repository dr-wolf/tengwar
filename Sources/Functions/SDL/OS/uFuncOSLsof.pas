unit uFuncOSLsof;

interface

uses
  uStandardFunc, uContext, uStreamRepository;

type
  TFuncOSLsof = class(TStandardFunc)
  public
    constructor Create(StreamRepository: TStreamRepository);
  private
    FStreamRepository: TStreamRepository;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Returns list of handles of files that are open in script.
    @type func(-> list(int))
    @out - list(int) # List of handles of open files
    * }

implementation

uses uArray, uTypeFactory, uData, uDataNumeric, uDataArray, uOpCode;

{ TFuncOSLsof }

constructor TFuncOSLsof.Create(StreamRepository: TStreamRepository);
begin
  inherited Create([TypeFactory.GetList(TypeFactory.GetInt)], ['files'], 0);
  FFuncUid := FID_OS_LSOF;
  FStreamRepository := StreamRepository;
end;

procedure TFuncOSLsof.Execute(Context: TContext);
var
  a: TArray<IData>;
  h: TArray<Integer>;
  I: Integer;
begin
  h := FStreamRepository.GetAllHandles;
  SetLength(a, Length(h));
  for I := 0 to High(h) do
    a[I] := TDataNumeric.Create(h[I]);
  Context.Data['files'] := TDataArray.Create(a);
end;

end.
