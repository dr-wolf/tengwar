unit uFuncOSGetEnv;

interface

uses
  uStandardFunc, uContext, uDict;

type
  TFuncOSGetEnv = class(TStandardFunc)
  public
    constructor Create(Environment: TDict<string>);
  private
    FEnvironment: TDict<string>;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Returns value of environment variable.
    @type func(name: string -> string)
    @in name string # Name of variable
    @out - string # Returned value of variable
    * }

implementation

uses SysUtils, uTypeFactory, uDataString, uOpCode;

{ TFuncOSGetEnv }

constructor TFuncOSGetEnv.Create(Environment: TDict<string>);
begin
  inherited Create([TypeFactory.GetString, TypeFactory.GetString], ['name', 'result'], 1);
  FFuncUid := FID_OS_GETENV;
  FEnvironment := Environment;
end;

procedure TFuncOSGetEnv.Execute(Context: TContext);
var
  v, s: string;
begin
  v := AsString(Context.Data['name']);
  if FEnvironment.Contains(v) then
    s := FEnvironment[v]
  else
    s := GetEnvironmentVariable(v);
  Context.Data['result'] := TDataString.Create(s);
end;

end.
