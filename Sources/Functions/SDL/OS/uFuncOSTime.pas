unit uFuncOSTime;

interface

uses
  uStandardFunc, uContext;

type
  TFuncOSTime = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

    { *
    @descr Returns current unix timestamp.
    @type func(-> int)
    @out - int # Unix timestamp
    * }

implementation

uses SysUtils, uTypeFactory, uDataNumeric, uOpCode;

{ TFuncOSTime }

constructor TFuncOSTime.Create;
begin
  inherited Create([TypeFactory.GetInt], ['time'], 0);
  FFuncUid := FID_OS_TIME;
end;

procedure TFuncOSTime.Execute(Context: TContext);
begin
  Context.Data['time'] := TDataNumeric.Create(Trunc((Now - 25569) * 86400));
end;

end.
