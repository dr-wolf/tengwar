unit uFuncOSSetEnv;

interface

uses
  uStandardFunc, uContext, uDict;

type
  TFuncOSSetEnv = class(TStandardFunc)
  public
    constructor Create(Environment: TDict<string>);
  private
    FEnvironment: TDict<string>;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Sets environment variable for current program and child processes.
    @type func(name: string, value: string)
    @in name string # Name of variable
    @in value string # New value of variable
    * }

implementation

uses uTypeFactory, uDataString, uOpCode;

{ TFuncOSSetEnv }

constructor TFuncOSSetEnv.Create(Environment: TDict<string>);
begin
  inherited Create([TypeFactory.GetString, TypeFactory.GetString], ['name', 'value'], 2);
  FFuncUid := FID_OS_SETENV;
  FEnvironment := Environment;
end;

procedure TFuncOSSetEnv.Execute(Context: TContext);
begin
  FEnvironment.Put(AsString(Context.Data['name']), AsString(Context.Data['value']));
end;

end.
