unit uFuncOSLsos;

interface

uses
  uStandardFunc, uContext, uSocketRepository, BlckSock;

type
  TFuncOSLsos = class(TStandardFunc)
  public
    constructor Create(TCPSocketRepository: TSocketRepository<TTCPBlockSocket>;
      UDPSocketRepository: TSocketRepository<TUDPBlockSocket>);
  private
    FTCPSocketRepository: TSocketRepository<TTCPBlockSocket>;
    FUDPSocketRepository: TSocketRepository<TUDPBlockSocket>;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Returns list of handles of network TCP or UDP sockets that are opened by program.
    @type func(tcp: bool -> list(int))
    @in tcp bool # if true returns TCP sockets othervise UDP sockets are returned
    @out - list(int) # List of handles of open sockets
    * }

implementation

uses uArray, uTypeFactory, uData, uDataBool, uDataNumeric, uDataArray, uOpCode;

{ TFuncOSLsos }

constructor TFuncOSLsos.Create(TCPSocketRepository: TSocketRepository<TTCPBlockSocket>;
  UDPSocketRepository: TSocketRepository<TUDPBlockSocket>);
begin
  inherited Create([TypeFactory.GetBool, TypeFactory.GetList(TypeFactory.GetInt)], ['tcp', 'sokets'], 1);
  FFuncUid := FID_OS_LSOS;
  FTCPSocketRepository := TCPSocketRepository;
  FUDPSocketRepository := UDPSocketRepository;
end;

procedure TFuncOSLsos.Execute(Context: TContext);
var
  a: TArray<IData>;
  h: TArray<Integer>;
  I: Integer;
begin
  if AsBool(Context.Data['tcp']) then
    h := FTCPSocketRepository.GetAllHandles
  else
    h := FUDPSocketRepository.GetAllHandles;
  SetLength(a, Length(h));
  for I := 0 to High(h) do
    a[I] := TDataNumeric.Create(h[I]);
  Context.Data['sokets'] := TDataArray.Create(a);
end;

end.
