unit uFuncMathSin;

interface

uses
  uStandardFunc, uContext;

type
  TFuncMathSin = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Retruns sinus of the angle.
    @type func(value: float -> float)
    @in value float # Angle given in radians
    @out - float # Sinus
    * }

implementation

uses uTypeFactory, uDataNumeric, uOpCode;

{ TFuncMathSin }

constructor TFuncMathSin.Create;
begin
  inherited Create([TypeFactory.GetFloat, TypeFactory.GetFloat], ['value', 'result'], 1);
  FFuncUid := FID_MATH_SIN;
end;

procedure TFuncMathSin.Execute(Context: TContext);
begin
  Context.Data['result'] := TDataNumeric.Create(Sin(AsFloat(Context.Data['value'])));
end;

end.
