unit uFuncMathLog;

interface

uses
  uStandardFunc, uContext;

type
  TFuncMathLog = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Returns the logarithm of a number.
    @type func(base: float, value: float -> float)
    @in value float # Logarithm base
    @in value float # Initial number
    @out - float # Result value
    * }

implementation

uses Math, uTypeFactory, uDataNumeric, uOpCode;

{ TFuncMathLog }

constructor TFuncMathLog.Create;
begin
  inherited Create([TypeFactory.GetFloat, TypeFactory.GetFloat, TypeFactory.GetFloat], ['base', 'value', 'result'], 2);
  FFuncUid := FID_MATH_LOG;
end;

procedure TFuncMathLog.Execute(Context: TContext);
begin
  Context.Data['result'] := TDataNumeric.Create(LogN(AsFloat(Context.Data['base']), AsFloat(Context.Data['value'])));
end;

end.
