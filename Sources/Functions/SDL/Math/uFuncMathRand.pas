unit uFuncMathRand;

interface

uses
  uStandardFunc, uContext;

type
  TFuncMathRand = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Return random float value greater or equal 0 and less than 1.
    @type func(-> float)
    @out - float # Random value
    * }

implementation

uses uTypeFactory, uDataNumeric, uOpCode;

{ TFuncMathRand }

constructor TFuncMathRand.Create;
begin
  inherited Create([TypeFactory.GetFloat], ['result'], 0);
  FFuncUid := FID_MATH_RAND;
end;

procedure TFuncMathRand.Execute(Context: TContext);
begin
  Context.Data['result'] := TDataNumeric.Create(Random);
end;

initialization

Randomize;

end.
