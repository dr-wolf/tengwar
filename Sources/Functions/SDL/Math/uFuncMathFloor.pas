unit uFuncMathFloor;

interface

uses
  uStandardFunc, uContext;

type
  TFuncMathFloor = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Returns the largest integer less than or equal to a given number.
    @type func(value: float -> int)
    @in value float # Initial number
    @out - int # Result
    * }

implementation

uses uTypeFactory, uDataNumeric, uOpCode;

{ TFuncMathFloor }

constructor TFuncMathFloor.Create;
begin
  inherited Create([TypeFactory.GetFloat, TypeFactory.GetInt], ['value', 'result'], 1);
  FFuncUid := FID_MATH_FLOOR;
end;

procedure TFuncMathFloor.Execute(Context: TContext);
begin
  Context.Data['result'] := TDataNumeric.Create(AsInt(Context.Data['value'], True));
end;

end.
