unit uFuncMathCos;

interface

uses
  uStandardFunc, uContext;

type
  TFuncMathCos = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Retruns cosinus of the angle.
    @type func(value: float -> float)
    @in value float # Angle given in radians
    @out - float # Cosinus
    * }

implementation

uses uTypeFactory, uDataNumeric, uOpCode;

{ TFuncMathCos }

constructor TFuncMathCos.Create;
begin
  inherited Create([TypeFactory.GetFloat, TypeFactory.GetFloat], ['value', 'result'], 1);
  FFuncUid := FID_MATH_COS;
end;

procedure TFuncMathCos.Execute(Context: TContext);
begin
  Context.Data['result'] := TDataNumeric.Create(Cos(AsFloat(Context.Data['value'])));
end;

end.
