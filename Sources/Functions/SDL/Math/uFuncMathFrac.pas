unit uFuncMathFrac;

interface

uses
  uStandardFunc, uContext;

type
  TFuncMathFrac = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Retrieves the decimal part of a float number.
    @type func(value: float -> float)
    @in value float # Initial number
    @out - float # Decimal part of a number
    * }

implementation

uses uTypeFactory, uDataNumeric, uOpCode;

{ TFuncMathFrac }

constructor TFuncMathFrac.Create;
begin
  inherited Create([TypeFactory.GetFloat, TypeFactory.GetFloat], ['value', 'result'], 1);
  FFuncUid := FID_MATH_FRAC;
end;

procedure TFuncMathFrac.Execute(Context: TContext);
begin
  Context.Data['result'] := TDataNumeric.Create(AsFloat(Context.Data['value'], True));
end;

end.
