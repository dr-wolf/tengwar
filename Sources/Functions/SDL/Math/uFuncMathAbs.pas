unit uFuncMathAbs;

interface

uses
  uStandardFunc, uContext;

type
  TFuncMathAbs = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Retruns absolute value of a number.
    @type func(value: float -> float)
    @in value float # Initial number
    @out - float # It's absolute value
    * }

implementation

uses uOperations, uTypeFactory, uDataNumeric, uOpCode;

{ TFuncMathAbs }

constructor TFuncMathAbs.Create;
begin
  inherited Create([TypeFactory.GetFloat, TypeFactory.GetFloat], ['value', 'result'], 1);
  FFuncUid := FID_MATH_ABS;
end;

procedure TFuncMathAbs.Execute(Context: TContext);
begin
  if AsFloat(Context.Data['value']) < 0 then
    Context.Data['result'] := Context.Data['value'].Apply(uoNegative)
  else
    Context.Data['result'] := Context.Data['value'];
end;

end.
