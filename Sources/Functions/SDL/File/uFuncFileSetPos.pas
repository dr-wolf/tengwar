unit uFuncFileSetPos;

interface

uses
  uFuncFile, uContext, uStreamRepository;

type
  TFuncFileSetPos = class(TFuncFile)
  public
    constructor Create(StreamRepository: TStreamRepository);
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Set position in file stream.
    @type func(handle: int, position: int)
    @in handle int # Handle of a stream
    @in postition int # Position to be set from beginning of the file
    * }

implementation

uses Classes, uTypeFactory, uDataNumeric, uOpCode;

{ TFuncFileSetPos }

constructor TFuncFileSetPos.Create(StreamRepository: TStreamRepository);
begin
  inherited Create([TypeFactory.GetInt, TypeFactory.GetInt], ['handle', 'position'], 2);
  FFuncUid := FID_FILE_SETPOS;
  FStreamRepository := StreamRepository;
end;

procedure TFuncFileSetPos.Execute(Context: TContext);
var
  s: TStream;
begin
  s := RetrieveStream('handle', Context);
  s.Position := AsInt(Context.Data['position']);
end;

end.
