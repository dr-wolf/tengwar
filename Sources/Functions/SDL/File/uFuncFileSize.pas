unit uFuncFileSize;

interface

uses
  uFuncFile, uContext, uStreamRepository;

type
  TFuncFileSize = class(TFuncFile)
  public
    constructor Create(StreamRepository: TStreamRepository);
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Returns total file stream length.
    @type func(handle: int -> int)
    @in handle int # Handle of a stream
    @out - int # Length of file
    * }

implementation

uses Classes, uTypeFactory, uDataNumeric, uOpCode;

{ TFuncFileSize }

constructor TFuncFileSize.Create(StreamRepository: TStreamRepository);
begin
  inherited Create([TypeFactory.GetInt, TypeFactory.GetInt], ['handle', 'size'], 1);
  FFuncUid := FID_FILE_SIZE;
  FStreamRepository := StreamRepository;
end;

procedure TFuncFileSize.Execute(Context: TContext);
var
  s: TStream;
begin
  s := RetrieveStream('handle', Context);
  Context.Data['size'] := TDataNumeric.Create(s.Size);
end;

end.
