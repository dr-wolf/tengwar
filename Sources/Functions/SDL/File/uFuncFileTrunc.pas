unit uFuncFileTrunc;

interface

uses
  uFuncFile, uContext, uStreamRepository;

type
  TFuncFileTrunc = class(TFuncFile)
  public
    constructor Create(StreamRepository: TStreamRepository);
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Truncates file on current position.
    @type func(handle: int)
    @in handle int # Handle of a stream
    * }

implementation

uses Classes, uTypeFactory, uOpCode;

{ TFuncFileTrunc }

constructor TFuncFileTrunc.Create(StreamRepository: TStreamRepository);
begin
  inherited Create([TypeFactory.GetInt], ['handle'], 1);
  FFuncUid := FID_FILE_TRUNC;
  FStreamRepository := StreamRepository;
end;

procedure TFuncFileTrunc.Execute(Context: TContext);
var
  s: TStream;
begin
  s := RetrieveStream('handle', Context);
  s.Size := s.Position;
end;

end.
