unit uFuncFileWriteString;

interface

uses
  uFuncFile, uContext, uStreamRepository;

type
  TFuncFileWriteString = class(TFuncFile)
  public
    constructor Create(StreamRepository: TStreamRepository);
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Writes string value to file stream.
    @type func(handle: int, value: string, encoding: int)
    @in handle int # Handle of a stream
    @in value string # Value to be written
    @in encoding int # Text codepage, see enc constant
    * }

implementation

uses SysUtils, Classes, uTypeFactory, uDataNumeric, uDataString, uOpCode;

{ TFuncFileWriteString }

constructor TFuncFileWriteString.Create(StreamRepository: TStreamRepository);
begin
  inherited Create([TypeFactory.GetInt, TypeFactory.GetString, TypeFactory.GetInt], ['handle', 'value', 'encoding'], 3);
  FFuncUid := FID_FILE_WRITESTRING;
  FStreamRepository := StreamRepository;
end;

procedure TFuncFileWriteString.Execute(Context: TContext);
var
  s: TStream;
  str: string;
  bytes: TBytes;
  cp: Int64;
begin
  s := RetrieveStream('handle', Context);
  cp := AsInt(Context.Data['encoding']);
  str := AsString(Context.Data['value']);
  bytes := TEncoding.Convert(TEncoding.UTF8, TEncoding.GetEncoding(cp), BytesOf(str));
  s.WriteBuffer(Pointer(bytes)^, Length(bytes));
end;

end.
