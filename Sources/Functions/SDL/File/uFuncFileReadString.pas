unit uFuncFileReadString;

interface

uses
  uFuncFile, uContext, uStreamRepository;

type
  TFuncFileReadString = class(TFuncFile)
  public
    constructor Create(StreamRepository: TStreamRepository);
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Reads string value from file stream.
    @type func(handle: int, size: int, encoding: int -> string)
    @in handle int # Handle of a stream
    @in size int # Maximun bytes amount to read
    @in encoding int # Text codepage, see enc constant
    @out - string # Returned value
    * }

implementation

uses SysUtils, Classes, uTypeFactory, uDataNumeric, uDataString, uOpCode;

{ TFuncFileReadString }

constructor TFuncFileReadString.Create(StreamRepository: TStreamRepository);
begin
  inherited Create([TypeFactory.GetInt, TypeFactory.GetInt, TypeFactory.GetInt, TypeFactory.GetString],
    ['handle', 'size', 'encoding', 'result'], 3);
  FFuncUid := FID_FILE_READSTRING;
  FStreamRepository := StreamRepository;
end;

procedure TFuncFileReadString.Execute(Context: TContext);
var
  bytes: TBytes;
  l, cp: Int64;
  s: TStream;
begin
  s := RetrieveStream('handle', Context);
  cp := AsInt(Context.Data['encoding']);
  l := AsInt(Context.Data['size']);
  if l > s.Size - s.Position then
    l := s.Size - s.Position;

  if l > 0 then
  begin
    SetLength(bytes, l);
    s.ReadBuffer(Pointer(bytes)^, l);
    Context.Data['result'] := TDataString.Create(StringOf(TEncoding.Convert(TEncoding.GetEncoding(cp),
      TEncoding.UTF8, bytes)));
  end
  else
    Context.Data['result'] := TDataString.Create('');
end;

end.
