unit uFuncFile;

interface

uses
  Classes, uStandardFunc, uContext, uStreamRepository;

type
  TByteSet = set of Byte;

type
  TFuncFile = class(TStandardFunc)
  protected
    FStreamRepository: TStreamRepository;
    function RetrieveStream(VarName: string; Context: TContext): TStream;
    function RetrievePackSize(VarName: string; AcceptedSizes: TByteSet; Context: TContext): Byte;
  end;

implementation

uses uExceptions, uDataNumeric, uDataString;

{ TFuncFile }

function TFuncFile.RetrievePackSize(VarName: string; AcceptedSizes: TByteSet; Context: TContext): Byte;
begin
  Result := AsInt(Context.Data[VarName]);
  if not(Result in AcceptedSizes) then
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create('Invalid packet size!'));
end;

function TFuncFile.RetrieveStream(VarName: string; Context: TContext): TStream;
begin
  Result := FStreamRepository.Stream[AsInt(Context.Data[VarName])];
  if Result = nil then
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create('Invalid file handle!'));
end;

end.
