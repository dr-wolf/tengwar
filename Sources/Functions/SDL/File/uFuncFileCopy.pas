unit uFuncFileCopy;

interface

uses
  uFuncFile, uContext, uStreamRepository;

type
  TFuncFileCopy = class(TFuncFile)
  public
    constructor Create(StreamRepository: TStreamRepository);
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Copies bytes from one file stream to another.
    @type func(source: int, destination: int, size: int -> int)
    @in source int # Handle of source stream
    @in destination int # Handle of destignation stream
    @in size int # Amount of bytes to be copied
    @out - int # How many bytes were actually copied
    * }

implementation

uses Classes, uStandardFunc, uTypeFactory, uDataNumeric, uOpCode;

{ TFuncFileCopy }

constructor TFuncFileCopy.Create(StreamRepository: TStreamRepository);
begin
  inherited Create([TypeFactory.GetInt, TypeFactory.GetInt, TypeFactory.GetInt, TypeFactory.GetInt],
    ['source', 'destionation', 'size', 'copied'], 3);
  FFuncUid := FID_FILE_COPY;
  FStreamRepository := StreamRepository;
end;

procedure TFuncFileCopy.Execute(Context: TContext);
var
  src, dst: TStream;
begin
  src := RetrieveStream('source', Context);
  dst := RetrieveStream('destination', Context);
  Context.Data['copied'] := TDataNumeric.Create(dst.CopyFrom(src, AsInt(Context.Data['size'])));
end;

end.
