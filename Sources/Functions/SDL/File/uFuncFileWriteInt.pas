unit uFuncFileWriteInt;

interface

uses
  uFuncFile, uContext, uStreamRepository;

type
  TFuncFileWriteInt = class(TFuncFile)
  public
    constructor Create(StreamRepository: TStreamRepository);
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Writes integer value to file stream.
    @type func(handle: int, value: int, size: int)
    @in handle int # Handle of a stream
    @in value int # Value to be written
    @in size int # Precision, can be 4 or 8 bytes, see sz_float and sz_double constants
    * }

implementation

uses Classes, uTypeFactory, uDataNumeric, uOpCode;

{ TFuncFileWriteInt }

constructor TFuncFileWriteInt.Create(StreamRepository: TStreamRepository);
begin
  inherited Create([TypeFactory.GetInt, TypeFactory.GetInt, TypeFactory.GetInt], ['handle', 'value', 'size'], 3);
  FFuncUid := FID_FILE_WRITEINT;
  FStreamRepository := StreamRepository;
end;

procedure TFuncFileWriteInt.Execute(Context: TContext);
var
  s: TStream;
  l: Byte;
  v: Int64;
begin
  s := RetrieveStream('handle', Context);
  l := RetrievePackSize('size', [1, 2, 4, 8], Context);
  v := AsInt(Context.Data['value']);
  s.WriteBuffer(v, l);
end;

end.
