unit uFuncFileReadFloat;

interface

uses
  uFuncFile, uContext, uStreamRepository;

type
  TFuncFileReadFloat = class(TFuncFile)
  public
    constructor Create(StreamRepository: TStreamRepository);
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Reads float value from file stream.
    @type func(handle: int, size: int -> float)
    @in handle int # Handle of a stream
    @in size int # Precision, can be 4 or 8 bytes, see sz_float and sz_double constants
    @out - float # Returned value
    * }

implementation

uses Classes, uTypeFactory, uDataNumeric, uOpCode;

{ TFuncFileReadFloat }

constructor TFuncFileReadFloat.Create(StreamRepository: TStreamRepository);
begin
  inherited Create([TypeFactory.GetInt, TypeFactory.GetInt, TypeFactory.GetFloat], ['handle', 'size', 'result'], 2);
  FFuncUid := FID_FILE_READFLOAT;
  FStreamRepository := StreamRepository;
end;

procedure TFuncFileReadFloat.Execute(Context: TContext);
var
  s: TStream;
  l: Byte;
  vs: Single;
  vd: Double;
begin
  s := RetrieveStream('handle', Context);
  l := RetrievePackSize('size', [4, 8], Context);
  if l = 4 then
  begin
    vs := 0;
    s.ReadBuffer(vs, SizeOf(vs));
    Context.Data['result'] := TDataNumeric.Create(vs);
  end else begin
    vd := 0;
    s.ReadBuffer(vd, SizeOf(vd));
    Context.Data['result'] := TDataNumeric.Create(vd);
  end;
end;

end.
