unit uFuncFileGetPos;

interface

uses
  uFuncFile, uContext, uStreamRepository;

type
  TFuncFileGetPos = class(TFuncFile)
  public
    constructor Create(StreamRepository: TStreamRepository);
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Returns current position in file stream.
    @type func(handle: int -> int)
    @in handle int # Handle of a stream
    @out - int # Current position
    * }

implementation

uses Classes, uStandardFunc, uTypeFactory, uDataNumeric, uOpCode;

{ TFuncFileGetPos }

constructor TFuncFileGetPos.Create(StreamRepository: TStreamRepository);
begin
  inherited Create([TypeFactory.GetInt, TypeFactory.GetInt], ['handle', 'position'], 1);
  FFuncUid := FID_FILE_GETPOS;
  FStreamRepository := StreamRepository;
end;

procedure TFuncFileGetPos.Execute(Context: TContext);
var
  s: TStream;
begin
  s := RetrieveStream('handle', Context);
  Context.Data['position'] := TDataNumeric.Create(s.Position);
end;

end.
