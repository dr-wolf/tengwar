unit uFuncFileReadInt;

interface

uses
  uFuncFile, uContext, uStreamRepository;

type
  TFuncFileReadInt = class(TFuncFile)
  public
    constructor Create(StreamRepository: TStreamRepository);
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Reads integer value from file stream.
    @type func(handle: int, size: int -> int)
    @in handle int # Handle of a stream
    @in size int # Size in bytes, can be 1, 2, 4 or 8, see sz_byte, sz_word, sz_int and sz_long constants
    @out - int # Returned value
    * }

implementation

uses Classes, uStandardFunc, uTypeFactory, uDataNumeric, uOpCode;

{ TFuncFileReadInt }

constructor TFuncFileReadInt.Create(StreamRepository: TStreamRepository);
begin
  inherited Create([TypeFactory.GetInt, TypeFactory.GetInt, TypeFactory.GetInt], ['handle', 'size', 'result'], 2);
  FFuncUid := FID_FILE_READINT;
  FStreamRepository := StreamRepository;
end;

procedure TFuncFileReadInt.Execute(Context: TContext);
var
  s: TStream;
  l: Byte;
  v: Int64;
begin
  s := RetrieveStream('handle', Context);
  l := RetrievePackSize('size', [1, 2, 4, 8], Context);
  v := 0;
  s.ReadBuffer(v, l);
  Context.Data['result'] := TDataNumeric.Create(v);
end;

end.
