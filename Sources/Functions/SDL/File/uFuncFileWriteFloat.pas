unit uFuncFileWriteFloat;

interface

uses
  uFuncFile, uContext, uStreamRepository;

type
  TFuncFileWriteFloat = class(TFuncFile)
  public
    constructor Create(StreamRepository: TStreamRepository);
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Writes float value to file stream.
    @type func(handle: int, value: float, size: int)
    @in handle int # Handle of a stream
    @in value float # Value to be written
    @in size int # Precision, can be 4 or 8 bytes, see sz_float and sz_double constants
    * }

implementation

uses Classes, uTypeFactory, uDataNumeric, uOpCode;

{ TFuncFileWriteFloat }

constructor TFuncFileWriteFloat.Create(StreamRepository: TStreamRepository);
begin
  inherited Create([TypeFactory.GetInt, TypeFactory.GetFloat, TypeFactory.GetInt], ['handle', 'value', 'size'], 3);
  FFuncUid := FID_FILE_WRITEFLOAT;
  FStreamRepository := StreamRepository;
end;

procedure TFuncFileWriteFloat.Execute(Context: TContext);
var
  s: TStream;
  l: Byte;
  vs: Single;
  vd: Double;
begin
  s := RetrieveStream('handle', Context);
  l := RetrievePackSize('size', [4, 8], Context);

  if l = 4 then
  begin
    vs := AsFloat(Context.Data['value']);
    s.WriteBuffer(vs, SizeOf(vs));
  end else begin
    vd := AsFloat(Context.Data['value']);
    s.WriteBuffer(vd, SizeOf(vd));
  end;
end;

end.
