# How to implemet your own SDL functions

1. Implement TFuncXY class inherited from TFunction or it's descendant class. X stands for unit, Y for func name.
2. Define unique FID_X_Y constant in Sources/Cache/uOpCode.pas
3. Assign it to FFuncUid it in TFuncXY constructor. 
4. Add a switch case to LoadFunction in Sources/Cache/uFunctionLoader.pas
5. Add value with FFuncUid instance in Source/Units/SDL/uUnitX.pas