unit uFuncBitNot;

interface

uses
  uStandardFunc, uContext;

type
  TFuncBitNot = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Inverts all bits of the value.
    @type func(value: int -> int)
    @in value int # Value to be inverted
    @out - int # Result of inversion
    * }

implementation

uses uTypeFactory, uDataNumeric, uOpCode;

{ TFuncBitNot }

constructor TFuncBitNot.Create;
begin
  inherited Create([TypeFactory.GetInt, TypeFactory.GetInt], ['value', 'result'], 1);
  FFuncUid := FID_BIT_NOT;
end;

procedure TFuncBitNot.Execute(Context: TContext);
begin
  Context.Data['result'] := TDataNumeric.Create(not AsInt(Context.Data['value']));
end;

end.
