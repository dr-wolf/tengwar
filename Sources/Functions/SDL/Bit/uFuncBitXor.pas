unit uFuncBitXor;

interface

uses
  uStandardFunc, uContext;

type
  TFuncBitXor = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Performs the logical exclusive OR operation on each pair of corresponding bits.
    @type func(a: int, b: int -> int)
    @in a int # Left value
    @in b int # Rignt value
    @out - int # Result of exclusive conjuction
    * }

implementation

uses uTypeFactory, uDataNumeric, uOpCode;

{ TFuncBitXor }

constructor TFuncBitXor.Create;
begin
  inherited Create([TypeFactory.GetInt, TypeFactory.GetInt, TypeFactory.GetInt], ['a', 'b', 'result'], 2);
  FFuncUid := FID_BIT_XOR;
end;

procedure TFuncBitXor.Execute(Context: TContext);
begin
  Context.Data['result'] := TDataNumeric.Create(AsInt(Context.Data['a']) xor AsInt(Context.Data['b']));
end;

end.
