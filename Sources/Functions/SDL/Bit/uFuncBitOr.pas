unit uFuncBitOr;

interface

uses
  uStandardFunc, uContext;

type
  TFuncBitOr = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Performs the logical inclusive OR operation on each pair of corresponding bits.
    @type func(a: int, b: int -> int)
    @in a int # Left value
    @in b int # Rignt value
    @out - int # Result of inclusive conjuction
    * }

implementation

uses uTypeFactory, uDataNumeric, uOpCode;

{ TFuncBitOr }

constructor TFuncBitOr.Create;
begin
  inherited Create([TypeFactory.GetInt, TypeFactory.GetInt, TypeFactory.GetInt], ['a', 'b', 'result'], 2);
  FFuncUid := FID_BIT_OR;
end;

procedure TFuncBitOr.Execute(Context: TContext);
begin
  Context.Data['result'] := TDataNumeric.Create(AsInt(Context.Data['a']) or AsInt(Context.Data['b']));
end;

end.
