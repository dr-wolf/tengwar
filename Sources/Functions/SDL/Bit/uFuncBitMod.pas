unit uFuncBitMod;

interface

uses
  uStandardFunc, uContext;

type
  TFuncBitMod = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Finds the remainder after division of one number by another.
    @type func(value: int, divider: int -> int)
    @in value int # The dividend
    @in divider int # Th divider
    @out - int # The remainder
    * }

implementation

uses uTypeFactory, uDataNumeric, uOpCode;

{ TFuncBitMod }

constructor TFuncBitMod.Create;
begin
  inherited Create([TypeFactory.GetInt, TypeFactory.GetInt, TypeFactory.GetInt], ['value', 'divider', 'result'], 2);
  FFuncUid := FID_BIT_MOD;
end;

procedure TFuncBitMod.Execute(Context: TContext);
begin
  Context.Data['result'] := TDataNumeric.Create(AsInt(Context.Data['value']) mod AsInt(Context.Data['divider']));
end;

end.
