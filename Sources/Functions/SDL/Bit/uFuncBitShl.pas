unit uFuncBitShl;

interface

uses
  uStandardFunc, uContext;

type
  TFuncBitShl = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Shifts each bit of its first parameter to the left. The second parameter decides the number of places the bits are shifted.
    @type func(value: int, dist: int -> int)
    @in value int # Value
    @in dist int # Number of places
    @out - int # Result value
    * }

implementation

uses uTypeFactory, uDataNumeric, uOpCode;

{ TFuncBitShl }

constructor TFuncBitShl.Create;
begin
  inherited Create([TypeFactory.GetInt, TypeFactory.GetInt, TypeFactory.GetInt], ['value', 'dist', 'result'], 2);
  FFuncUid := FID_BIT_SHL;
end;

procedure TFuncBitShl.Execute(Context: TContext);
begin
  Context.Data['result'] := TDataNumeric.Create(AsInt(Context.Data['value']) shl AsInt(Context.Data['dist']));
end;

end.
