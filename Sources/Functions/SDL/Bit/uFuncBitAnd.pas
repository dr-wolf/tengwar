unit uFuncBitAnd;

interface

uses
  uStandardFunc, uContext;

type
  TFuncBitAnd = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Performs the logical AND operation on each pair of the corresponding bits, by multiplying them.
    @type func(a: int, b: int -> int)
    @in a int # Left value
    @in b int # Rignt value
    @out - int # Result of disjunction
    * }

implementation

uses uTypeFactory, uDataNumeric, uOpCode;

{ TFuncBitAnd }

constructor TFuncBitAnd.Create;
begin
  inherited Create([TypeFactory.GetInt, TypeFactory.GetInt, TypeFactory.GetInt], ['a', 'b', 'result'], 2);
  FFuncUid := FID_BIT_AND;
end;

procedure TFuncBitAnd.Execute(Context: TContext);
begin
  Context.Data['result'] := TDataNumeric.Create(AsInt(Context.Data['a']) and AsInt(Context.Data['b']));
end;

end.
