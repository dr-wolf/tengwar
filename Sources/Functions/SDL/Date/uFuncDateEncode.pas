unit uFuncDateEncode;

interface

uses
  uStandardFunc, uContext, uType;

type
  TFuncDateEncode = class(TStandardFunc)
  public
    constructor Create(DateType: TType);
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Assembles date and time components into unix timestamp.
    @type func(date: tdate -> int)
    @in date tdate # Struct with date and time components
    @out - int # Unix timestamp
    * }

implementation

uses SysUtils, uExceptions, uTypeFactory, uDataString, uDataNumeric, uOpCode;

{ TFuncDateEncode }

constructor TFuncDateEncode.Create(DateType: TType);
begin
  inherited Create([DateType, TypeFactory.GetInt], ['date', 'unix'], 1);
  FFuncUid := FID_DATE_ENCODE;
end;

procedure TFuncDateEncode.Execute(Context: TContext);

  function EncodeDateTime(const Year, Month, Day, Hour, Minute, Second: Word; var Value: TDateTime): Boolean;
  var
    LTime: TDateTime;
  begin
    Result := TryEncodeDate(Year, Month, Day, Value);
    if Result then
    begin
      Result := TryEncodeTime(Hour, Minute, Second, 0, LTime);
      if Result then
        if Value >= 0 then
          Value := Value + LTime
        else
          Value := Value - LTime;
    end;
  end;

var
  i: Integer;
  c: array [0 .. 5] of Word;
  d: TDateTime;

begin
  for i := 0 to 5 do
    c[i] := AsInt(Context.Data['date'].Item(i));
  if EncodeDateTime(c[0], c[1], c[2], c[3], c[4], c[5], d) then
    Context.Data['unix'] := TDataNumeric.Create(Round((d - 25569) * 86400))
  else
    raise ERuntimeException.Create(E_RUNTIME_ERROR, TDataString.Create('Can not construct date!'));
end;

end.
