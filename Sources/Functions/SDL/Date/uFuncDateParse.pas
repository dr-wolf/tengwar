unit uFuncDateParse;

interface

uses
  uStandardFunc, uContext;

type
  TFuncDateParse = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Parses date and time string into unix timestamp.
    @type func(text: string -> int)
    @in text string # Date and time string in YYYY/MM/DD HH:MM:SS format
    @out - int # Unix timestamp
    * }

implementation

uses SysUtils, uTypeFactory, uDataNumeric, uDataString, uOpCode;

{ TFuncDateParse }

constructor TFuncDateParse.Create;
begin
  inherited Create([TypeFactory.GetString, TypeFactory.GetInt], ['text', 'unix'], 1);
  FFuncUid := FID_DATE_PARSE;
end;

procedure TFuncDateParse.Execute(Context: TContext);
begin
  Context.Data['unix'] := TDataNumeric.Create(Round((StrToDateTime(AsString(Context.Data['text'])) - 25569) * 86400));
end;

end.
