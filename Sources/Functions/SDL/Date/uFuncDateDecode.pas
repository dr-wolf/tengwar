unit uFuncDateDecode;

interface

uses
  uStandardFunc, uContext, uType;

type
  TFuncDateDecode = class(TStandardFunc)
  public
    constructor Create(DateType: TType);
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Decodes unix date to its components.
    @type func(unix: int -> tdate)
    @in unix int # Unix timestamp
    @out - tdate # Struct with date and time components
    * }

implementation

uses SysUtils, uTypeFactory, uDataArray, uDataNumeric, uOpCode;

{ TFuncDateDecode }

constructor TFuncDateDecode.Create(DateType: TType);
begin
  inherited Create([TypeFactory.GetInt, DateType], ['unix', 'date'], 1);
  FFuncUid := FID_DATE_DECODE;
end;

procedure TFuncDateDecode.Execute(Context: TContext);
var
  ms: Word;
  c: array [0 .. 5] of Word;
  d: TDateTime;
begin
  d := (AsInt(Context.Data['unix']) / 86400) + 25569;
  DecodeDate(d, c[0], c[1], c[2]);
  DecodeTime(d, c[3], c[4], c[5], ms);
  Context.Data['date'] := TDataArray.Create([TDataNumeric.Create(c[0]), TDataNumeric.Create(c[1]),
    TDataNumeric.Create(c[2]), TDataNumeric.Create(c[3]), TDataNumeric.Create(c[4]),
    TDataNumeric.Create(c[5])]);
end;

end.
