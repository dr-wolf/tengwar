unit uFuncRexMatch;

interface

uses
  uFuncRex, uContext;

type
  TFuncRexMatch = class(TFuncRex)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Tests a string for matching given regular expression.
    @type func(regex: string, text: string -> bool)
    @in regex string # Regular expression to test
    @in text string # Text to be matched with regular expression
    @in modifiers int # Reqular expression modifiers
    @out - bool # Match result
    * }

implementation

uses uTypeFactory, uDataBool, uDataNumeric, uDataString, uOpCode;

{ TFuncRexMatch }

constructor TFuncRexMatch.Create;
begin
  inherited Create([TypeFactory.GetString, TypeFactory.GetString, TypeFactory.GetInt, TypeFactory.GetBool],
    ['regex', 'text', 'modifiers', 'result'], 3);
  FFuncUid := FID_REX_MATCH;
end;

procedure TFuncRexMatch.Execute(Context: TContext);
begin
  with InitRex(AsInt(Context.Data['modifiers'])) do
  begin
    Expression := AsString(Context.Data['regex']);
    Context.Data['result'] := TDataBool.Create(Exec(AsString(Context.Data['text'])));
    Free;
  end;
end;

end.
