unit uFuncRex;

interface

uses
  uStandardFunc, RegExpr;

const
  REX_CASEINSENSITIVE = $01;
  REX_CYRILLIC = $02;
  REX_NEWLINE = $04;
  REX_GREED = $08;
  REX_MULTILINE = $10;
  REX_EXTENDED = $20;

type
  TFuncRex = class(TStandardFunc)
  protected
    function InitRex(Modifiers: Integer): TRegExpr;
  end;

implementation

{ TFuncRex }

function TFuncRex.InitRex(Modifiers: Integer): TRegExpr;
begin
  Result := TRegExpr.Create;
  Result.ModifierI := Modifiers and REX_CASEINSENSITIVE <> 0;
  Result.ModifierR := Modifiers and REX_CYRILLIC <> 0;
  Result.ModifierS := Modifiers and REX_NEWLINE <> 0;
  Result.ModifierG := Modifiers and REX_GREED <> 0;
  Result.ModifierM := Modifiers and REX_MULTILINE <> 0;
  Result.ModifierX := Modifiers and REX_EXTENDED <> 0;
end;

end.
