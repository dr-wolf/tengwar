unit uFuncRexFind;

interface

uses
  uType, uFuncRex, uContext;

type
  TFuncRexFind = class(TFuncRex)
  public
    constructor Create(MatchType: TType);
  private
    FMatchType: TType;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Finds all substrings matching given regular expression.
    @type func(regex: string, text: string -> list(tmatch))
    @in regex string # Regular expression to apply
    @in text string # Text to be matched with regular expression
    @in modifiers int # Reqular expression modifiers
    @out - list(tmatch) # List of matches: string, position and length
    * }

implementation

uses uArray, uTypeFactory, uData, uDataNumeric, uDataString, uDataArray, uOpCode;

{ TFuncRexFind }

constructor TFuncRexFind.Create(MatchType: TType);
begin
  inherited Create([TypeFactory.GetString, TypeFactory.GetString, TypeFactory.GetInt, TypeFactory.GetList(MatchType)],
    ['regex', 'text', 'modifiers', 'matches'], 3);
  FFuncUid := FID_REX_FIND;
  FMatchType := MatchType;
end;

procedure TFuncRexFind.Execute(Context: TContext);
var
  s: string;
  a: TArray<IData>;
begin
  SetLength(a, 0);
  s := AsString(Context.Data['text']);
  with InitRex(AsInt(Context.Data['modifiers'])) do
  begin
    Expression := AsString(Context.Data['regex']);
    if Exec(s) then
      repeat
        SetLength(a, Length(a) + 1);
        a[High(a)] := TDataArray.Create([TDataString.Create(Match[0]), TDataNumeric.Create(MatchPos[0]),
          TDataNumeric.Create(MatchLen[0])]);
      until not ExecNext;
    Free;
  end;
  Context.Data['matches'] := TDataArray.Create(a);
end;

end.
