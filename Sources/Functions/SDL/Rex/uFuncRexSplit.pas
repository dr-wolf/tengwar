unit uFuncRexSplit;

interface

uses
  uFuncRex, uContext;

type
  TFuncRexSplit = class(TFuncRex)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Splits string into list of strings by regular expression.
    @type func(regex: string, text: string -> list(string))
    @in regex string # Regular expression
    @in text string # Source text
    @in modifiers int # Reqular expression modifiers
    @out - list(string) # Splitted text
    * }

implementation

uses Classes, uArray, uTypeFactory, uData, uDataArray, uDataNumeric, uDataString, uOpCode;

{ TFuncRexSplit }

constructor TFuncRexSplit.Create;
begin
  inherited Create([TypeFactory.GetString, TypeFactory.GetString, TypeFactory.GetInt,
    TypeFactory.GetList(TypeFactory.GetString)], ['regex', 'text', 'modifiers', 'result'], 3);
  FFuncUid := FID_REX_SPLIT;
end;

procedure TFuncRexSplit.Execute(Context: TContext);
var
  r: TArray<IData>;
  s: TStrings;
  i: Integer;
begin
  s := TStringList.Create;
  with InitRex(AsInt(Context.Data['modifiers'])) do
  begin
    Expression := AsString(Context.Data['regex']);
    Split(AsString(Context.Data['text']), s);
    SetLength(r, s.Count);
    for i := 0 to s.Count - 1 do
      r[i] := TDataString.Create(s[i]);
    Context.Data['result'] := TDataArray.Create(r);
    Free;
  end;
  s.Free;
end;

end.
