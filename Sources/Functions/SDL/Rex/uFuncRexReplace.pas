unit uFuncRexReplace;

interface

uses
  uFuncRex, uContext;

type
  TFuncRexReplace = class(TFuncRex)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Replaces all matched substring with given one.
    @type func(regex: string, text: string, replacement: string -> string)
    @in regex string # Regular expression to test
    @in text string # Source text
    @in replacement string # Replacement
    @in modifiers int # Reqular expression modifiers
    @out - string # Text after replace
    * }

implementation

uses uTypeFactory, uDataString, uDataNumeric, uOpCode;

{ TFuncRexReplace }

constructor TFuncRexReplace.Create;
begin
  inherited Create([TypeFactory.GetString, TypeFactory.GetString, TypeFactory.GetString, TypeFactory.GetInt,
    TypeFactory.GetString], ['regex', 'text', 'replacement', 'modifiers', 'result'], 4);
  FFuncUid := FID_REX_REPLACE;
end;

procedure TFuncRexReplace.Execute(Context: TContext);
begin
  with InitRex(AsInt(Context.Data['modifiers'])) do
  begin
    Expression := AsString(Context.Data['regex']);
    Context.Data['result'] := TDataString.Create(Replace(AsString(Context.Data['text']),
      AsString(Context.Data['replacement']), False));
    Free;
  end;
end;

end.
