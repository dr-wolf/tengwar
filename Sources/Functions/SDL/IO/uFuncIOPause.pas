unit uFuncIOPause;

interface

uses
  uStandardFunc, uContext;

type
  TFuncIOPause = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Waits till user strikes Enter key.
    @type func()
    * }

implementation

uses uOpCode;

{ TFuncIOPause }

constructor TFuncIOPause.Create;
begin
  inherited Create([], [], 0);
  FFuncUid := FID_IO_PAUSE;
end;

procedure TFuncIOPause.Execute(Context: TContext);
begin
  Readln;
end;

end.
