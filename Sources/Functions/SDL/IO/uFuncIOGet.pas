unit uFuncIOGet;

interface

uses
  uStandardFunc, uContext;

type
  TFuncIOGet = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Reads text from console.
    @type func(-> string)
    @out - string # Text read from console
    * }

implementation

uses uTypeFactory, uDataString, uOpCode;

{ TFuncIOGet }

constructor TFuncIOGet.Create;
begin
  inherited Create([TypeFactory.GetString], ['text'], 0);
  FFuncUid := FID_IO_GET;
end;

procedure TFuncIOGet.Execute(Context: TContext);
var
  s: string;
begin
  Readln(s);
  Context.Data['text'] := TDataString.Create(s);
end;

end.
