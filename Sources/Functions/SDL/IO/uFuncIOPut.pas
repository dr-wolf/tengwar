unit uFuncIOPut;

interface

uses
  uStandardFunc, uContext;

type
  TFuncIOPut = class(TStandardFunc)
  public
    constructor Create;
  protected
    procedure Execute(Context: TContext); override;
  end;

  { *
    @descr Prints a text in console.
    @type func(value: string)
    @in value string # Text to be printed
    * }

implementation

uses uTypeFactory, uDataString, uOpCode {$IFDEF WINDOWS}, Windows{$ENDIF};

{ TFuncPut }

constructor TFuncIOPut.Create;
begin
  inherited Create([TypeFactory.GetString], ['text'], 1);
  FFuncUid := FID_IO_PUT;
end;

procedure TFuncIOPut.Execute(Context: uContext.TContext);
begin
{$IFDEF WINDOWS}
  SetConsoleOutputCP(CP_UTF8);
{$ENDIF}
  Write(AsString(Context.Data['text']));
end;

end.
