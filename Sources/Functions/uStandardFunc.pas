unit uStandardFunc;

interface

uses
  uArray, uFunction, uType, uValue, uData, uContext, uState;

type
  TStandardFunc = class(TFunction)
  public
    constructor Create(Types: array of TType; Names: array of string; ParamCount: Integer); overload;
    destructor Destroy; override;
  private
    FType: TType;
  protected
    procedure Execute(Context: TContext); virtual; abstract;
  public
    function Wrap: IValue;
    function Log: string; override;
    procedure Call(State: TState; ParamMap: TArray<Integer>; Closures: TArray<IData>); override;
    procedure RefreshSelf(Data: IData); override;
  end;

implementation

uses SysUtils, uTypeFactory, uDataFunc;

{ TStandardFunc }

constructor TStandardFunc.Create(Types: array of TType; Names: array of string; ParamCount: Integer);

  function CP(a: array of TType; Pos: Integer; Count: Integer = -1): TArray<TType>; overload;
  var
    i: Integer;
  begin
    if Count < 0 then
      Count := Length(a) - Pos;
    SetLength(Result, Count);
    for i := 0 to High(Result) do
      Result[i] := a[Pos + i];
  end;

  function CP(a: array of string; Pos: Integer; Count: Integer = -1): TArray<string>; overload;
  var
    i: Integer;
  begin
    if Count < 0 then
      Count := Length(a) - Pos;
    SetLength(Result, Count);
    for i := 0 to High(Result) do
      Result[i] := a[Pos + i];
  end;

var
  i, n: Integer;
  p, r: TType;
begin
  FContext := TContext.Create;
  SetLength(FClosures, 0);
  SetLength(FParameters, ParamCount);
  for i := 0 to High(FParameters) do
    FParameters[i] := FContext.Keep(Types[i].Default, Names[i], False);
  SetLength(FResults, Length(Types) - ParamCount);
  for i := 0 to High(FResults) do
    FResults[i] := FContext.Keep(Types[i + ParamCount].Default, Names[i + ParamCount], False);

  p := TypeFactory.GetStruct(CP(Types, 0, ParamCount), CP(Names, 0, ParamCount));
  n := Length(Types) - ParamCount;
  case n of
    0:
      r := TypeFactory.GetVoid;
    1:
      r := Types[High(Types)]
  else
    r := TypeFactory.GetStruct(CP(Types, ParamCount), CP(Names, ParamCount));
  end;
  FType := TypeFactory.GetFunc(p, r, n = 1);
end;

destructor TStandardFunc.Destroy;
begin
  inherited;
end;

procedure TStandardFunc.Call(State: TState; ParamMap: TArray<Integer>; Closures: TArray<IData>);
var
  c: TContext;
begin
  c := RecreateContext(State, ParamMap, Closures);
  Execute(c);
  ResolveResults(c, State);
  c.Free;
end;

function TStandardFunc.Log: string;
begin
  Result := 'std ' + IntToHex(FFuncUid, 2);
end;

procedure TStandardFunc.RefreshSelf(Data: IData);
begin

end;

function TStandardFunc.Wrap: IValue;
begin
  Result := TValue.Create(FType, TDataFunc.Create(Self));
end;

end.
