unit uCustomFunc;

interface

uses
  uArray, uFunction, uContext, uOpCache, uState, uData;

type
  TCustomFunc = class(TFunction)
  public
    constructor Create(Context: TContext; Closures, Params, Results: array of Integer);
    destructor Destroy; override;
  private
    FOpAddress: Integer;
  public
    property Context: TContext read FContext;
    function Log: string; override;
    function Serialize(const Cache: TOpCache): TClassStream; override;
    procedure Call(State: TState; ParamMap: TArray<Integer>; Closures: TArray<IData>); override;
    procedure RefreshSelf(Data: IData); override;
    procedure SetAddress(OpAddress: Integer);
  end;

implementation

uses SysUtils, uOpCode, uIdentifierRegistry;

{ TCustomFunc }

constructor TCustomFunc.Create(Context: TContext; Closures, Params, Results: array of Integer);
begin
  inherited Create(Context, Closures, Params, Results);
  FOpAddress := -1;
end;

destructor TCustomFunc.Destroy;
begin
  inherited;
end;

function TCustomFunc.Log: string;
begin
  Result := 'at <0x' + IntToHex(FOpAddress, 8) + '>';
end;

procedure TCustomFunc.RefreshSelf(Data: IData);
var
  Id: Integer;
begin
  Id := FContext.Resolve(RI_SELF);
  if Id >= 0 then
    FContext.Replace(Id, Data);
end;

procedure TCustomFunc.Call(State: TState; ParamMap: TArray<Integer>; Closures: TArray<IData>);
var
  c: TContext;
begin
  c := RecreateContext(State, ParamMap, Closures);
  if FOpAddress >= 0 then
    State.Call(FOpAddress, c)
  else
  begin
    ResolveResults(c, State);
    c.Free;
  end;
end;

function TCustomFunc.Serialize(const Cache: TOpCache): TClassStream;

  procedure WriteArray(Stream: TClassStream; a: TArray<Integer>);
  var
    i: Integer;
  begin
    Stream.WriteInteger(Length(a));
    for i := 0 to High(a) do
      Stream.WriteInteger(a[i]);
  end;

begin
  Result := TClassStream.Create;
  with Result do
  begin
    WriteByte(UID_FUNCTION);
    WriteByte(FID_CUSTOM);
    WriteInteger(FOpAddress);
    WriteArray(Result, FParameters);
    WriteArray(Result, FResults);
    WriteArray(Result, FClosures);
    FContext.Save(Result, Cache);
  end;
end;

procedure TCustomFunc.SetAddress(OpAddress: Integer);
begin
  FOpAddress := OpAddress;
end;

end.
