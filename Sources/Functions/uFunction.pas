unit uFunction;

interface

uses
  uArray, uData, uContext, uOpCache, uOpCode, uState;

type
  IFunction = interface(IInterface)
    function Log: string;
    function Serialize(const Cache: TOpCache): TClassStream;
    procedure Call(State: TState; ParamMap: TArray<Integer>; Closures: TArray<IData>);
    procedure RefreshSelf(Data: IData);
  end;

type

  { TFunction }

  TFunction = class(TInterfacedObject, IFunction)
  public
    constructor Create(Context: TContext; Closures, Params, Results: array of Integer);
    destructor Destroy; override;
  protected
    FFuncUid: TFID;
    FContext: TContext;
    FClosures: TArray<Integer>;
    FParameters: TArray<Integer>;
    FResults: TArray<Integer>;
    function RecreateContext(State: TState; ParamMap: TArray<Integer>; Closures: TArray<IData>): TContext;
    procedure ResolveResults(Context: TContext; State: TState);
  public
    function Log: string; virtual; abstract;
    function Serialize(const Cache: TOpCache): TClassStream; virtual;
    procedure Call(State: TState; ParamMap: TArray<Integer>; Closures: TArray<IData>); virtual; abstract;
    procedure RefreshSelf(Data: IData); virtual; abstract;
  end;

implementation

uses uExceptions, uDataArray;

{ TFunction }

constructor TFunction.Create(Context: TContext; Closures, Params, Results: array of Integer);
var
  i: Integer;
begin
  FFuncUid := FID_CUSTOM;
  FContext := Context;
  SetLength(FClosures, Length(Closures));
  for i := 0 to High(FClosures) do
    FClosures[i] := Closures[i];
  SetLength(FParameters, Length(Params));
  for i := 0 to High(FParameters) do
    FParameters[i] := Params[i];
  SetLength(FResults, Length(Results));
  for i := 0 to High(FResults) do
    FResults[i] := Results[i];
end;

destructor TFunction.Destroy;
begin
  FClosures := nil;
  FParameters := nil;
  FResults := nil;
  FContext.Free;
  inherited;
end;

function TFunction.RecreateContext(State: TState; ParamMap: TArray<Integer>; Closures: TArray<IData>): TContext;
var
  d: IData;
  i: Integer;
begin
  Result := FContext.Recreate;
  d := State.Stack.Pop;
  if Length(ParamMap) > 0 then
  begin
    for i := 0 to High(ParamMap) do
      Result.Resolve(FParameters[ParamMap[i]]).Assign(d.Item(i));
  end else begin
    for i := 0 to d.Count - 1 do
      Result.Resolve(FParameters[i]).Assign(d.Item(i));
  end;
  if Length(Closures) <> Length(FClosures) then
    raise ERuntimeException.Create(E_UNEXPECTED, nil);
  for i := 0 to High(Closures) do
    Result.Replace(FClosures[i], Closures[i]);
end;

procedure TFunction.ResolveResults(Context: TContext; State: TState);
var
  a: TArray<IData>;
  i: Integer;
begin
  if Length(FResults) = 1 then
    State.Stack.Push(Context.Resolve(FResults[0]).Clone)
  else if Length(FResults) > 0 then
  begin
    SetLength(a, Length(FResults));
    for i := 0 to High(FResults) do
      a[i] := Context.Resolve(FResults[i]).Clone;
    State.Stack.Push(TDataArray.Create(a));
  end;
end;

function TFunction.Serialize(const Cache: TOpCache): TClassStream;
begin
  Result := TClassStream.Create;
  with Result do
  begin
    WriteByte(UID_FUNCTION);
    WriteByte(FFuncUid);
  end;
end;

end.
