unit uContext;

interface

uses
  uArray, uData, uOpCache;

type
  TContext = class(TObject)
  public
    constructor Create;
    destructor Destroy; override;
  private type
    TDataItem = record
      Persistent: Boolean;
      Name: string;
      Data: IData;
    end;
  private
    FContextValues: TArray<TDataItem>;
    function GetData(Name: string): IData;
    procedure SetData(Name: string; Data: IData);
  public
    property Data[Name: string]: IData read GetData write SetData;
    function Keep(Data: IData; Name: string; Persistent: Boolean): Integer;
    function Resolve(Name: string): Integer; overload;
    function Resolve(Id: Integer): IData; overload;
    function Recreate: TContext;
    procedure Replace(Id: Integer; Data: IData);
    procedure UnregisterName(Name: string);
    procedure Save(Stream: TClassStream; Cache: TOpCache);
    procedure Load(Stream: TClassStream; Cache: TOpCache);
  end;

implementation

uses SysUtils;

{ TContext }

constructor TContext.Create;
begin
  SetLength(FContextValues, 0);
end;

destructor TContext.Destroy;
begin
  FContextValues := nil;
  inherited;
end;

function TContext.GetData(Name: string): IData;
var
  Id: Integer;
begin
  Id := Resolve(Name);
  if Id < 0 then
    raise Exception.Create('Variable "' + Name + '" is not registered in context');
  Result := FContextValues[Id].Data;
end;

function TContext.Resolve(Id: Integer): IData;
begin
  Result := FContextValues[Id].Data;
end;

function TContext.Resolve(Name: string): Integer;
begin
  Result := High(FContextValues);
  while (Result >= 0) and (FContextValues[Result].Name <> Name) do
    Dec(Result);
end;

function TContext.Recreate: TContext;
var
  i: Integer;
begin
  Result := TContext.Create;
  SetLength(Result.FContextValues, Length(FContextValues));
  for i := 0 to High(FContextValues) do
    if FContextValues[i].Persistent then
      Result.FContextValues[i] := FContextValues[i]
    else
    begin
      Result.FContextValues[i].Name := FContextValues[i].Name;
      Result.FContextValues[i].Data := FContextValues[i].Data.Clone;
      Result.FContextValues[i].Persistent := False;
    end;
end;

function TContext.Keep(Data: IData; Name: string; Persistent: Boolean): Integer;
begin
  Result := 0;
  while (Result < Length(FContextValues)) and (FContextValues[Result].Data <> Data) do
    Inc(Result);
  if Result = Length(FContextValues) then
  begin
    if Resolve(Name) >= 0 then
      raise Exception.Create('Variable "' + Name + '" is already registered in context');
    SetLength(FContextValues, Result + 1);
    FContextValues[Result].Data := Data;
    FContextValues[Result].Name := Name;
    FContextValues[Result].Persistent := Persistent;
  end;
end;

procedure TContext.Load(Stream: TClassStream; Cache: TOpCache);
var
  i: Integer;
begin
  with Stream do
  begin
    SetLength(FContextValues, ReadInteger);
    for i := 0 to High(FContextValues) do
    begin
      FContextValues[i].Persistent := Boolean(ReadByte);
      FContextValues[i].Name := ReadString;
      FContextValues[i].Data := Cache.Load(ReadInteger) as TData;
    end;
  end;
end;

procedure TContext.Save(Stream: TClassStream; Cache: TOpCache);
var
  i: Integer;
begin
  with Stream do
  begin
    WriteInteger(Length(FContextValues));
    for i := 0 to High(FContextValues) do
    begin
      WriteByte(Byte(FContextValues[i].Persistent));
      WriteString(FContextValues[i].Name);
      WriteInteger(Cache.Save(FContextValues[i].Data as TData, (FContextValues[i].Data as TData).Serialize));
    end;
  end;
end;

procedure TContext.SetData(Name: string; Data: IData);
var
  Id: Integer;
begin
  Id := Resolve(Name);
  if Id < 0 then
    raise Exception.Create('Variable "' + Name + '" is not registered in context');
  FContextValues[Id].Data.Assign(Data);
end;

procedure TContext.UnregisterName(Name: string);
var
  Id: Integer;
begin
  Id := Resolve(Name);
  if Id >= 0 then
    FContextValues[Id].Name := '';
end;

procedure TContext.Replace(Id: Integer; Data: IData);
begin
  FContextValues[Id].Data := Data;
end;

end.
