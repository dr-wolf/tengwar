unit uStreamRepository;

interface

uses
  Classes, uArray;

type
  TStreamRepository = class(TObject)
  public
    constructor Create;
    destructor Destroy; override;
  private type
    TFile = record
      Filename: string;
      Handle: Integer;
      Stream: TStream;
    end;
  private
    FFiles: TArray<TFile>;
    function GetStream(Handle: Integer): TStream;
    function FindFile(Handle: Integer): Integer;
    procedure CloseFile(I: Integer);
  public
    property Stream[Handle: Integer]: TStream read GetStream;
    function Add(Stream: TStream; Filename: string): Integer;
    function StreamFilename(Handle: Integer): string;
    function GetAllHandles: TArray<Integer>;
    procedure Close(Handle: Integer);
  end;

implementation

{ TStreamRepository }

constructor TStreamRepository.Create;
begin
  SetLength(FFiles, 0);
end;

destructor TStreamRepository.Destroy;
var
  I: Integer;
begin
  for I := 0 to High(FFiles) do
    CloseFile(I);
  inherited;
end;

function TStreamRepository.FindFile(Handle: Integer): Integer;
begin
  Result := High(FFiles);
  while (Result >= 0) and (FFiles[Result].Handle <> Handle) do
    Dec(Result);
end;

function TStreamRepository.GetAllHandles: TArray<Integer>;
var
  I: Integer;
begin
  SetLength(Result, Length(FFiles));
  for I := 0 to High(FFiles) do
    Result[I] := FFiles[I].Handle;
end;

function TStreamRepository.GetStream(Handle: Integer): TStream;
var
  I: Integer;
begin
  I := FindFile(Handle);
  if I >= 0 then
    Result := FFiles[I].Stream
  else
    Result := nil;
end;

function TStreamRepository.StreamFilename(Handle: Integer): string;
var
  I: Integer;
begin
  I := FindFile(Handle);
  if I >= 0 then
    Result := FFiles[I].Filename
  else
    Result := '';
end;

procedure TStreamRepository.CloseFile(I: Integer);
begin
  FFiles[I].Stream.Free;
  if I < High(FFiles) then
    FFiles[I] := FFiles[High(FFiles)];
  SetLength(FFiles, Length(FFiles) - 1);
end;

function TStreamRepository.Add(Stream: TStream; Filename: string): Integer;
var
  f: TFile;
begin
  f.Handle := Random(MaxInt);
  f.Stream := Stream;
  f.Filename := Filename;
  SetLength(FFiles, Length(FFiles) + 1);
  FFiles[High(FFiles)] := f;
  Result := f.Handle;
end;

procedure TStreamRepository.Close(Handle: Integer);
var
  I: Integer;
begin
  I := FindFile(Handle);
  if I >= 0 then
    CloseFile(I);
end;

end.
