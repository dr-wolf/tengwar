unit uRepositoryFactory;

interface

uses
  uSocketRepository, uStreamRepository, BlckSock;

type
  TRepositoryFactory = class(TObject)
  public
    constructor Create;
    destructor Destroy; override;
  private
    FTCPSocketRepository: TSocketRepository<TTCPBlockSocket>;
    FUDPSocketRepository: TSocketRepository<TUDPBlockSocket>;
    FStreamRepository: TStreamRepository;
    function GetTCPSocketRepository: TSocketRepository<TTCPBlockSocket>;
    function GetUDPSocketRepository: TSocketRepository<TUDPBlockSocket>;
    function GetStreamRepository: TStreamRepository;
  public
    property TCPSocketRepository: TSocketRepository<TTCPBlockSocket> read GetTCPSocketRepository;
    property UDPSocketRepository: TSocketRepository<TUDPBlockSocket> read GetUDPSocketRepository;
    property StreamRepository: TStreamRepository read GetStreamRepository;
  end;

implementation

{ TRepositoryFactory }

constructor TRepositoryFactory.Create;
begin
  FTCPSocketRepository := nil;
  FUDPSocketRepository := nil;
  FStreamRepository := nil;
end;

destructor TRepositoryFactory.Destroy;
begin
  if FTCPSocketRepository <> nil then
    FTCPSocketRepository.Free;
  if FUDPSocketRepository <> nil then
    FUDPSocketRepository.Free;
  if FStreamRepository <> nil then
    FStreamRepository.Free;
  inherited;
end;

function TRepositoryFactory.GetTCPSocketRepository: TSocketRepository<TTCPBlockSocket>;
begin
  if FTCPSocketRepository = nil then
    FTCPSocketRepository := TSocketRepository<TTCPBlockSocket>.Create;
  Result := FTCPSocketRepository;
end;

function TRepositoryFactory.GetUDPSocketRepository: TSocketRepository<TUDPBlockSocket>;
begin
  if FUDPSocketRepository = nil then
    FUDPSocketRepository := TSocketRepository<TUDPBlockSocket>.Create;
  Result := FUDPSocketRepository;
end;

function TRepositoryFactory.GetStreamRepository: TStreamRepository;
begin
  if FStreamRepository = nil then
    FStreamRepository := TStreamRepository.Create;
  Result := FStreamRepository;
end;

end.
