unit uSocketRepository;

interface

uses
  BlckSock, uArray;

type
  TSocketRepository<T: TSocksBlockSocket> = class(TObject)
  public
    constructor Create;
    destructor Destroy; override;
  private type
    TSock = record
      Handle: Integer;
      Sock: T;
    end;
  private
    FSocks: TArray<TSock>;
    function GetSock(Handle: Integer): T;
    function FindSock(Handle: Integer): Integer;
    procedure CloseSock(I: Integer);
  public
    property Sock[Handle: Integer]: T read GetSock;
    function Add(Socket: T): Integer;
    function GetAllHandles: TArray<Integer>;
    procedure Close(Handle: Integer);
  end;

implementation

{ TSocketRepository }

constructor TSocketRepository<T>.Create;
begin
  SetLength(FSocks, 0);
end;

destructor TSocketRepository<T>.Destroy;
var
  I: Integer;
begin
  for I := 0 to High(FSocks) do
    CloseSock(I);
  inherited;
end;

function TSocketRepository<T>.FindSock(Handle: Integer): Integer;
begin
  Result := High(FSocks);
  while (Result >= 0) and (FSocks[Result].Handle <> Handle) do
    Dec(Result);
end;

function TSocketRepository<T>.GetAllHandles: TArray<Integer>;
var
  I: Integer;
begin
  SetLength(Result, Length(FSocks));
  for I := 0 to High(FSocks) do
    Result[I] := FSocks[I].Handle;
end;

function TSocketRepository<T>.GetSock(Handle: Integer): T;
var
  I: Integer;
begin
  I := FindSock(Handle);
  if I >= 0 then
    Result := FSocks[I].Sock
  else
    Result := default (T);
end;

procedure TSocketRepository<T>.CloseSock(I: Integer);
begin
  with FSocks[I] do
  begin
    Sock.CloseSocket;
    Sock.Free;
  end;
  if I < High(FSocks) then
    FSocks[I] := FSocks[High(FSocks)];
  SetLength(FSocks, Length(FSocks) - 1);
end;

function TSocketRepository<T>.Add(Socket: T): Integer;
var
  s: TSock;
begin
  s.Sock := Socket;
  s.Handle := Random(MaxInt);
  SetLength(FSocks, Length(FSocks) + 1);
  FSocks[High(FSocks)] := s;
  Result := s.Handle;
end;

procedure TSocketRepository<T>.Close(Handle: Integer);
var
  I: Integer;
begin
  I := FindSock(Handle);
  if I >= 0 then
    CloseSock(I);
end;

end.
