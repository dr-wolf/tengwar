unit uUnit;

interface

uses
  uDict, uType, uValue, uMachine;

type
  TUnit = class(TObject)
  public
    constructor Create(Filename: string); overload;
    destructor Destroy; override;
  protected
    FFilename: string;
    FUnits: TDict<TUnit>;
    FTypes: TDict<TType>;
    FValues: TDict<IValue>;
  public
    property Filename: string read FFilename write FFilename;
    function GetType(Name, UnitName: string): TType;
    function GetValue(Name, UnitName: string): IValue;
    procedure AddUnit(Name: string; U: TUnit);
    procedure AddType(Name: string; T: TType);
    procedure AddValue(Name: string; V: IValue);
    procedure Initialize(Machine: TMachine);
  end;

implementation

uses uExceptions, uTypeFactory, uOpVal;

{ TUnit }

constructor TUnit.Create(Filename: string);
begin
  FFilename := Filename;
  FUnits := TDict<TUnit>.Create;
  FTypes := TDict<TType>.Create;
  FValues := TDict<IValue>.Create;
end;

destructor TUnit.Destroy;
begin
  FUnits.Free;
  FTypes.Free;
  FValues.Free;
  inherited;
end;

procedure TUnit.AddType(Name: string; T: TType);
begin
  FTypes.Put(Name, T);
end;

procedure TUnit.AddUnit(Name: string; U: TUnit);
begin
  FUnits.Put(Name, U);
end;

procedure TUnit.AddValue(Name: string; V: IValue);
begin
  FValues.Put(Name, V);
end;

function TUnit.GetType(Name, UnitName: string): TType;
begin
  Result := nil;
  if UnitName <> '' then
  begin
    if FUnits.Contains(UnitName) then
      Result := FUnits[UnitName].GetType(Name, '')
    else
      raise ECodeException.Create(E_UNKNOWN_UNIT, UnitName);
  end else if FTypes.Contains(Name) then
    Result := FTypes[Name];
end;

function TUnit.GetValue(Name, UnitName: string): IValue;
begin
  Result := nil;
  if UnitName <> '' then
  begin
    if FUnits.Contains(UnitName) then
      Result := FUnits[UnitName].GetValue(Name, '')
    else
      raise ECodeException.Create(E_UNKNOWN_UNIT, UnitName);
  end else if FValues.Contains(Name) then
    Result := FValues[Name];
end;

procedure TUnit.Initialize(Machine: TMachine);
var
  T: TType;
begin
  if FValues.Contains('main') then
  begin
    T := TypeFactory.GetFunc(TypeFactory.GetStruct([TypeFactory.GetList(TypeFactory.GetString)], ['args']),
      TypeFactory.GetVoid, False);
    if not T.Compatible(FValues['main'].TypeInfo, True) then
      raise ECodeException.Create(E_TYPE_UNCOMPATIBLE, T.Hash);
    Machine.SetStartOp(TOpVal.Create(FValues['main'].Data));
  end
  else
    raise ECodeException.Create(E_IDENTIFIER_UNDECLARED, 'main');
end;

end.
