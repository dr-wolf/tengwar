unit uUnitLibrary;

interface

uses
  uDict, uConfig, uUnit, uRepositoryFactory, uMachine;

type
  TUnitLibrary = class(TObject)
  public
    constructor Create(Repositories: TRepositoryFactory; Config: TConfig);
    destructor Destroy; override;
  private
    FRepositories: TRepositoryFactory;
    FUnits: TDict<TUnit>;
    FConfig: TConfig;
    FLinesTotal: Integer;
    function LocateFile(const BasePath, Path: string): string;
  public
    property LinesTotal: Integer read FLinesTotal;
    function Load(Path: string; Machine: TMachine; const BasePath: string = ''): TUnit;
  end;

implementation

uses SysUtils, uExceptions, uUnitBuilder, uUnitBit, uUnitDate, uUnitFile, uUnitFS, uUnitIO, uUnitOS, uUnitRex, uUnitStr,
  uUnitMath, uUnitTCP, uUnitUDP;

function IsRelativePath(const Path: string): Boolean;
var
  L: Integer;
begin
  L := Length(Path);
  Result := (L > 0) and (Path[1] <> PathDelim)
{$IFDEF MSWINDOWS} and (L > 1) and (Path[2] <> ':'){$ENDIF MSWINDOWS};
end;

{ TUnitLibrary }

constructor TUnitLibrary.Create(Repositories: TRepositoryFactory; Config: TConfig);
begin
  FRepositories := Repositories;
  FUnits := TDict<TUnit>.Create;
  FConfig := Config;
  FLinesTotal := 0;
end;

destructor TUnitLibrary.Destroy;
begin
  FUnits.Reset;
  while FUnits.Next do
    FUnits[FUnits.Key].Free;
  FUnits.Free;
  inherited;
end;

function TUnitLibrary.Load(Path: string; Machine: TMachine; const BasePath: string = ''): TUnit;

type
  TUID = (uiBit, uiDate, uiFile, uiFS, uiIO, uiMath, uiOS, uiRex, uiStr, uiTCP, uiUDP, uiErr);

  function UnitID(Script: string): TUID;
  const
    UNITS: array [TUID] of string = ('<bit>.tw', '<date>.tw', '<file>.tw', '<fs>.tw', '<io>.tw', '<math>.tw', '<os>.tw',
      '<rex>.tw', '<str>.tw', '<tcp>.tw', '<udp>.tw', '');
  begin
    Result := Low(TUID);
    while (Result < High(TUID)) and (UNITS[Result] <> Script) do
      Result := Succ(Result);
  end;

  function CreateBuiltIn(U: TUID): TUnit;
  begin
    case U of
      uiBit:
        Result := TUnitBit.Create;
      uiDate:
        Result := TUnitDate.Create;
      uiFile:
        Result := TUnitFile.Create(FRepositories);
      uiFS:
        Result := TUnitFS.Create(FRepositories);
      uiIO:
        Result := TUnitIO.Create;
      uiMath:
        Result := TUnitMath.Create;
      uiOS:
        Result := TUnitOS.Create(FRepositories);
      uiRex:
        Result := TUnitRex.Create;
      uiStr:
        Result := TUnitStr.Create;
      uiTCP:
        Result := TUnitTCP.Create(FRepositories);
      uiUDP:
        Result := TUnitUDP.Create(FRepositories);
    else
      Result := nil;
    end;
  end;

var
  uid: TUID;

begin
  uid := UnitID(Path);
  if uid = uiErr then
    Path := LocateFile(BasePath, Path);
  if FUnits.Contains(Path) then
  begin
    Result := FUnits[Path];
    if Result = nil then
      raise ECodeException.Create(E_CIRCULAR_IMPORT);
  end else begin
    if uid <> uiErr then
      Result := CreateBuiltIn(uid)
    else
    begin
      FUnits.Put(Path, nil);
      with TUnitBuilder.Create(Path, Self, Machine) do
      begin
        FUnits.Put(Path, Application);
        Result := Application;
        Inc(FLinesTotal, LinesCount);
        Free;
      end;
      Writeln('Loaded ' + Path);
    end;
  end;
end;

function TUnitLibrary.LocateFile(const BasePath, Path: string): string;
var
  i: Integer;
begin
  if not IsRelativePath(Path) then
    Result := Path
  else if FileExists(BasePath + Path) then
    Result := ExpandFileName(BasePath + Path)
  else
  begin
    for i := 0 to FConfig.LibPathCount - 1 do
      if FileExists(FConfig.LibPath[i] + Path) then
      begin
        Result := ExpandFileName(FConfig.LibPath[i] + Path);
        Exit;
      end;
    raise ECodeException.Create(E_MISSING_UNIT, Path);
  end;
end;

end.
