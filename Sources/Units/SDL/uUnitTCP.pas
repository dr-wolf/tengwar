unit uUnitTCP;

interface

uses
  uUnit, uRepositoryFactory;

type
  TUnitTCP = class(TUnit)
  public
    constructor Create(Repositories: TRepositoryFactory);
  end;

  { *
    @descr Network TCP blocking sockets
    * }

implementation

uses uFuncTCPConnect, uFuncTCPClose, uFuncTCPSend, uFuncTCPReceive, uFuncTCPListen, uFuncTCPAccept, uFuncTCPRemote;

{ TUnitTCP }

constructor TUnitTCP.Create(Repositories: TRepositoryFactory);
begin
  inherited Create('<tcp>');

  AddValue('connect', TFuncTCPConnect.Create(Repositories.TCPSocketRepository).Wrap);
  AddValue('close', TFuncTCPClose.Create(Repositories.TCPSocketRepository).Wrap);
  AddValue('receive', TFuncTCPReceive.Create(Repositories.TCPSocketRepository, Repositories.StreamRepository).Wrap);
  AddValue('send', TFuncTCPSend.Create(Repositories.TCPSocketRepository, Repositories.StreamRepository).Wrap);
  AddValue('listen', TFuncTCPListen.Create(Repositories.TCPSocketRepository).Wrap);
  AddValue('accept', TFuncTCPAccept.Create(Repositories.TCPSocketRepository).Wrap);
  AddValue('remote', TFuncTCPRemote.Create(Repositories.TCPSocketRepository).Wrap);
end;

end.
