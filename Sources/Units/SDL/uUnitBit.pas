unit uUnitBit;

interface

uses
  uUnit;

type
  TUnitBit = class(TUnit)
  public
    constructor Create;
  end;

  { *
    @descr Contains functions for bitwise operations on integer values.
    * }

implementation

uses uFuncBitAnd, uFuncBitMod, uFuncBitNot, uFuncBitOr, uFuncBitShl, uFuncBitShr, uFuncBitXor;

{ TUnitInt }

constructor TUnitBit.Create;
begin
  inherited Create('<bit>');

  AddValue('and', TFuncBitAnd.Create.Wrap);
  AddValue('mod', TFuncBitMod.Create.Wrap);
  AddValue('not', TFuncBitNot.Create.Wrap);
  AddValue('or', TFuncBitOr.Create.Wrap);
  AddValue('shl', TFuncBitShl.Create.Wrap);
  AddValue('shr', TFuncBitShr.Create.Wrap);
  AddValue('xor', TFuncBitXor.Create.Wrap);
end;

end.
