unit uUnitMath;

interface

uses
  uUnit;

type
  TUnitMath = class(TUnit)
  public
    constructor Create;
  end;

  { *
    @descr Math fuctions and constants
    @const pi float # Pi value
    @const e float # Exponent
    * }

implementation

uses uValue, uTypeFactory, uDataNumeric, uFuncMathAbs, uFuncMathCos, uFuncMathFrac, uFuncMathFloor, uFuncMathLog,
  uFuncMathRand, uFuncMathSin;

{ TUnitMath }

constructor TUnitMath.Create;
begin
  inherited Create('<math>');

  AddValue('pi', TValue.Create(TypeFactory.GetFloat, TDataNumeric.Create(Pi)));
  AddValue('e', TValue.Create(TypeFactory.GetFloat, TDataNumeric.Create(Exp(1))));

  AddValue('abs', TFuncMathAbs.Create.Wrap);
  AddValue('cos', TFuncMathCos.Create.Wrap);
  AddValue('frac', TFuncMathFrac.Create.Wrap);
  AddValue('floor', TFuncMathFloor.Create.Wrap);
  AddValue('log', TFuncMathLog.Create.Wrap);
  AddValue('rand', TFuncMathRand.Create.Wrap);
  AddValue('sin', TFuncMathSin.Create.Wrap);
end;

end.
