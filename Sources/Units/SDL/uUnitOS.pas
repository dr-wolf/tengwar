unit uUnitOS;

interface

uses
  uUnit, uRepositoryFactory;

type
  TUnitOS = class(TUnit)
  public
    constructor Create(Repositories: TRepositoryFactory);
  end;

  { *
    @descr Interaction with some OS features
    @const envvars list(string) # Contains list of environment variable names
    * }

implementation

uses uArray, uDict, uEnvironment, uTypeFactory, uValue, uData, uDataString, uDataArray, uFuncOSSleep,
  uFuncOSExec, uFuncOSLsof, uFuncOSLsos, uFuncOSGetEnv, uFuncOSSetEnv, uFuncOSTime;

function MakeEnvVars(Variables: TDict<string>): TDataArray;
var
  d: TArray<IData>;
  i: Integer;
begin
  SetLength(d, Variables.Count);
  Variables.Reset;
  i := 0;
  while Variables.Next do
  begin
    d[i] := TDataString.Create(Variables.Key);
    Inc(i);
  end;
  Result := TDataArray.Create(d);
end;

{ TUnitOS }

constructor TUnitOS.Create(Repositories: TRepositoryFactory);
begin
  inherited Create('<os>');

  AddValue('envvars', TValue.Create(TypeFactory.GetList(TypeFactory.GetString), MakeEnvVars(Environment.Variables)));

  AddValue('exec', TFuncOSExec.Create(Environment.Variables).Wrap);
  AddValue('lsof', TFuncOSLsof.Create(Repositories.StreamRepository).Wrap);
  AddValue('lsos', TFuncOSLsos.Create(Repositories.TCPSocketRepository, Repositories.UDPSocketRepository).Wrap);
  AddValue('getenv', TFuncOSGetEnv.Create(Environment.Variables).Wrap);
  AddValue('setenv', TFuncOSSetEnv.Create(Environment.Variables).Wrap);
  AddValue('sleep', TFuncOSSleep.Create.Wrap);
  AddValue('time', TFuncOSTime.Create.Wrap);
end;

end.
