unit uUnitFS;

interface

uses
  uUnit, uRepositoryFactory;

type
  TUnitFS = class(TUnit)
  public
    constructor Create(Repositories: TRepositoryFactory);
  end;

  { *
    @descr Works with filesytem.
    @types tfilestat: (size: int, attr: int)
    @const fa_invalid # -1
    @const fa_readonly # 1
    @const fa_hidden # 2
    @const fa_sysfile # 4
    @const fa_directory # 16
    @const fa_archive # 32
    * }

implementation

uses uTypeFactory, uType, uValue, uDataNumeric, uFuncFSPwd, uFuncFSDelete, uFuncFSChDir, uFuncFSMkDir,
  uFuncFSRmDir, uFuncFSStat, uFuncFSAbsPath, uFuncFSScan, uFuncFSOpen, uFuncFSClose;

{ TUnitFS }

constructor TUnitFS.Create(Repositories: TRepositoryFactory);
var
  t: TType;
begin
  inherited Create('<fs>');

  t := TypeFactory.GetPredefined(pdtFileAttr);

  AddType('tfilestat', t);

  AddValue('fa_invalid', TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(-1)));
  AddValue('fa_readonly', TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(1)));
  AddValue('fa_hidden', TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(2)));
  AddValue('fa_sysfile', TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(4)));
  AddValue('fa_directory', TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(16)));
  AddValue('fa_archive', TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(32)));

  AddValue('abspath', TFuncFSAbsPath.Create.Wrap);
  AddValue('chdir', TFuncFSChDir.Create.Wrap);
  AddValue('close', TFuncFSClose.Create(Repositories.StreamRepository).Wrap);
  AddValue('delete', TFuncFSDelete.Create.Wrap);

  AddValue('mkdir', TFuncFSMkDir.Create.Wrap);
  AddValue('open', TFuncFSOpen.Create(Repositories.StreamRepository).Wrap);
  AddValue('pwd', TFuncFSPwd.Create.Wrap);
  AddValue('rmdir', TFuncFSRmDir.Create.Wrap);
  AddValue('scan', TFuncFSScan.Create.Wrap);
  AddValue('stat', TFuncFSStat.Create(t).Wrap);
end;

end.
