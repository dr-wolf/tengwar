unit uUnitUDP;

interface

uses
  uUnit, uRepositoryFactory;

type
  TUnitUDP = class(TUnit)
  public
    constructor Create(Repositories: TRepositoryFactory);
  end;

  { *
    @descr Network UDP blocking sockets
    * }

implementation

uses uFuncUDPBind, uFuncUDPClose, uFuncUDPReceive, uFuncUDPSend, uFuncUDPBroadcast;

{ TUnitUDP }

constructor TUnitUDP.Create(Repositories: TRepositoryFactory);
begin
  inherited Create('<udp>');

  AddValue('bind', TFuncUDPBind.Create(Repositories.UDPSocketRepository).Wrap);
  AddValue('broadcast', TFuncUDPBroadcast.Create(Repositories.StreamRepository).Wrap);
  AddValue('close', TFuncUDPClose.Create(Repositories.UDPSocketRepository).Wrap);
  AddValue('send', TFuncUDPSend.Create(Repositories.StreamRepository).Wrap);
  AddValue('receive', TFuncUDPReceive.Create(Repositories.UDPSocketRepository, Repositories.StreamRepository).Wrap);
end;

end.
