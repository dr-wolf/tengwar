unit uUnitRex;

interface

uses
  uUnit;

type
  TUnitRex = class(TUnit)
  public
    constructor Create;
  end;

  { *
    @descr Regular expressions.
    @types tmatch: (match: string, pos: int, length: int)
    @const case_insensitive # 1
    @const cyrillic # 2
    @const newline # 4
    @const greed # 8
    @const multiline # 16
    @const extended # 32
    * }

implementation

uses uType, uTypeFactory, uValue, uDataNumeric, uFuncRex, uFuncRexMatch, uFuncRexFind, uFuncRexReplace,
  uFuncRexSplit;

{ TUnitRex }

constructor TUnitRex.Create;
var
  t: TType;
begin
  inherited Create('<rex>');

  t := TypeFactory.GetPredefined(pdtRex);

  AddType('tmatch', t);

  AddValue('case_insensitive', TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(REX_CASEINSENSITIVE)));
  AddValue('cyrillic', TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(REX_CYRILLIC)));
  AddValue('newline', TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(REX_NEWLINE)));
  AddValue('greed', TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(REX_GREED)));
  AddValue('multiline', TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(REX_MULTILINE)));
  AddValue('extended', TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(REX_EXTENDED)));

  AddValue('match', TFuncRexMatch.Create.Wrap);
  AddValue('find', TFuncRexFind.Create(t).Wrap);
  AddValue('replace', TFuncRexReplace.Create.Wrap);
  AddValue('split', TFuncRexSplit.Create.Wrap);
end;

end.
