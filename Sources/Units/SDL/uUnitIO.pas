unit uUnitIO;

interface

uses
  uUnit;

type
  TUnitIO = class(TUnit)
  public
    constructor Create;
  end;

  { *
    @descr This unit designed for console input and output.
    @const eol string # Contains \r\n string
    * }

implementation

uses uValue, uTypeFactory, uDataString, uFuncIOPut, uFuncIOGet, uFuncIOPause;

{ TUnitIO }

constructor TUnitIO.Create;
begin
  inherited Create('<io>');

  AddValue('eol', TValue.Create(TypeFactory.GetString, TDataString.Create(#$D#$A)));

  AddValue('get', TFuncIOGet.Create.Wrap);
  AddValue('put', TFuncIOPut.Create.Wrap);
  AddValue('pause', TFuncIOPause.Create.Wrap);
end;

end.
