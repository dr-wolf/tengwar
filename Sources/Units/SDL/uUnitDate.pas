unit uUnitDate;

interface

uses
  uUnit;

type
  TUnitDate = class(TUnit)
  public
    constructor Create;
  end;

  { *
    @descr Contains functions for decoding and encoding date and time.
    @types tdate: (year: int, month: int, day: int, hour: int, minute: int, second: int)
    * }

implementation

uses uTypeFactory, uType, uFuncDateDecode, uFuncDateEncode, uFuncDateFmt, uFuncDateParse;

{ TUnitDate }

constructor TUnitDate.Create;
var
  t: TType;
begin
  inherited Create('<date>');

  t := TypeFactory.GetPredefined(pdtDate);

  AddType('tdate', t);

  AddValue('decode', TFuncDateDecode.Create(t).Wrap);
  AddValue('encode', TFuncDateEncode.Create(t).Wrap);
  AddValue('fmt', TFuncDateFmt.Create.Wrap);
  AddValue('parse', TFuncDateParse.Create.Wrap);
end;

end.
