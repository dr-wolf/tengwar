unit uUnitFile;

interface

uses
  uUnit, uRepositoryFactory;

type
  TUnitFile = class(TUnit)
  public
    constructor Create(Repositories: TRepositoryFactory);
  end;

  { *
    @descr Contains routines for working with file stream.
    @const sz_byte int # 1
    @const sz_word int # 2
    @const sz_int int # 4
    @const sz_long int # 8
    @const sz_single int # 4
    @const sz_double int # 8
    @const enc.cp866 int # 866
    @const enc.koi8r int # 878
    @const enc.koi8u int # 1168
    @const enc.unicode int # 1200
    @const enc.unicode_bigendian int # 1201
    @const enc.win1250 int # 1250
    @const enc.win1251 int # 1251
    @const enc.win1252 int # 1252
    @const enc.utf7 int # 65000
    @const enc.utf8 int # 65001
    * }

implementation

uses uTypeFactory, uValue, uDataNumeric, uDataArray, uFuncFileCopy, uFuncFileGetPos, uFuncFileSetPos,
  uFuncFileSize, uFuncFileTrunc, uFuncFileReadInt, uFuncFileReadFloat, uFuncFileReadString, uFuncFileWriteInt,
  uFuncFileWriteFloat, uFuncFileWriteString;

{ TUnitFile }

constructor TUnitFile.Create(Repositories: TRepositoryFactory);
  function GenerateEncodingStruct: TValue;
  var
    c: TDataArray;
  begin
    c := TDataArray.Create([TDataNumeric.Create(866), TDataNumeric.Create(878), TDataNumeric.Create(1168),
      TDataNumeric.Create(1200), TDataNumeric.Create(1201), TDataNumeric.Create(1250), TDataNumeric.Create(1251),
      TDataNumeric.Create(1252), TDataNumeric.Create(65000), TDataNumeric.Create(65001)]);
    Result := TValue.Create(TypeFactory.GetStruct([TypeFactory.GetInt, TypeFactory.GetInt, TypeFactory.GetInt,
      TypeFactory.GetInt, TypeFactory.GetInt, TypeFactory.GetInt, TypeFactory.GetInt, TypeFactory.GetInt,
      TypeFactory.GetInt, TypeFactory.GetInt], ['cp866', 'koi8r', 'koi8u', 'unicode', 'unicode_bigendian', 'win1250',
      'win1251', 'win1252', 'utf7', 'utf8']), c);
  end;

begin
  inherited Create('<file>');

  AddValue('sz_byte', TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(1)));
  AddValue('sz_word', TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(2)));
  AddValue('sz_int', TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(4)));
  AddValue('sz_long', TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(8)));

  AddValue('sz_single', TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(4)));
  AddValue('sz_double', TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(8)));

  AddValue('enc', GenerateEncodingStruct);

  AddValue('copy', TFuncFileCopy.Create(Repositories.StreamRepository).Wrap);
  AddValue('getpos', TFuncFileGetPos.Create(Repositories.StreamRepository).Wrap);
  AddValue('setpos', TFuncFileSetPos.Create(Repositories.StreamRepository).Wrap);
  AddValue('size', TFuncFileSize.Create(Repositories.StreamRepository).Wrap);
  AddValue('trunc', TFuncFileTrunc.Create(Repositories.StreamRepository).Wrap);

  AddValue('readint', TFuncFileReadInt.Create(Repositories.StreamRepository).Wrap);
  AddValue('readfloat', TFuncFileReadFloat.Create(Repositories.StreamRepository).Wrap);
  AddValue('readstring', TFuncFileReadString.Create(Repositories.StreamRepository).Wrap);

  AddValue('writeint', TFuncFileWriteInt.Create(Repositories.StreamRepository).Wrap);
  AddValue('writefloat', TFuncFileWriteFloat.Create(Repositories.StreamRepository).Wrap);
  AddValue('writestring', TFuncFileWriteString.Create(Repositories.StreamRepository).Wrap);
end;

end.
