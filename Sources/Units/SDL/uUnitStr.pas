unit uUnitStr;

interface

uses
  uUnit;

type
  TUnitStr = class(TUnit)
  public
    constructor Create;
  end;

  { *
    @descr String utilities.
    * }

implementation

uses uFuncStrFmt, uFuncStrHex, uFuncStrLen, uFuncStrSub, uFuncStrPos, uFuncStrParse, uFuncStrAscii, uFuncStrText,
  uFuncStrLower, uFuncStrUpper;

{ TUnitStr }

constructor TUnitStr.Create;
begin
  inherited Create('<str>');

  AddValue('fmt', TFuncStrFmt.Create.Wrap);
  AddValue('hex', TFuncStrHex.Create.Wrap);
  AddValue('parse', TFuncStrParse.Create.Wrap);
  AddValue('len', TFuncStrLen.Create.Wrap);
  AddValue('sub', TFuncStrSub.Create.Wrap);
  AddValue('pos', TFuncStrPos.Create.Wrap);
  AddValue('ascii', TFuncStrAscii.Create.Wrap);
  AddValue('text', TFuncStrText.Create.Wrap);
  AddValue('lower', TFuncStrLower.Create.Wrap);
  AddValue('upper', TFuncStrUpper.Create.Wrap);
end;

end.
