unit uIdentifierRegistry;

interface

uses
  uArray;

const
  RI_SELF = 'self';
  RI_TRUE = 'true';
  RI_FALSE = 'false';
  RI_ANY = 'any';
  RI_BOOL = 'bool';
  RI_INT = 'int';
  RI_FLOAT = 'float';
  RI_STRING = 'string';
  RI_MAIN = 'main';

type
  TIdentifierRegistry = class(TObject)
  public
    constructor Create;
    destructor Destroy; override;
  private
    FItems: TArray<string>;
  public
    function Add(Identifier: string): Boolean;
    procedure Unregister(Identifier: string);
    procedure Clear;
    class function RandomIdentifier(Alpha: string; Len: Integer): string;
  end;

implementation

{ TRegistry }

constructor TIdentifierRegistry.Create;
const
  RESERVED: array [0 .. 5] of string = (RI_BOOL, RI_INT, RI_FLOAT, RI_STRING, RI_TRUE, RI_FALSE);
var
  i: Integer;
begin
  SetLength(FItems, Length(RESERVED));
  for i := 0 to High(FItems) do
    FItems[i] := RESERVED[i];
end;

destructor TIdentifierRegistry.Destroy;
begin
  FItems := nil;
  inherited;
end;

function TIdentifierRegistry.Add(Identifier: string): Boolean;
var
  i: Integer;
begin
  Result := True;
  for i := 0 to High(FItems) do
  begin
    Result := Result and (FItems[i] <> Identifier);
    if not Result then
      Exit;
  end;
  SetLength(FItems, Length(FItems) + 1);
  FItems[High(FItems)] := Identifier;
end;

procedure TIdentifierRegistry.Clear;
begin
  SetLength(FItems, 0);
end;

procedure TIdentifierRegistry.Unregister(Identifier: string);
var
  i: Integer;
  f: Boolean;
begin
  f := False;
  for i := 0 to High(FItems) - 1 do
  begin
    f := f or (FItems[i] = Identifier);
    if f then
      FItems[i] := FItems[i + 1];
  end;
  if f or (FItems[High(FItems)] = Identifier) then
    SetLength(FItems, Length(FItems) - 1);
end;

class function TIdentifierRegistry.RandomIdentifier(Alpha: string; Len: Integer): string;
var
  i: Integer;
begin
  Result := '';
  for i := 1 to Len do
    Result := Result + Alpha[Random(Length(Alpha)) + 1];
end;

end.
