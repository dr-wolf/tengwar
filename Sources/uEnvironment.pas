unit uEnvironment;

interface

uses
  uDict;

type
  TEnvironment = class(TObject)
  public
    constructor Create;
    destructor Destroy; override;
  private
    FVariables: TDict<string>;
  public
    property Variables: TDict<string> read FVariables;
    function GetValue(Name: string): string;
    procedure SetValue(Name, Value: string);
  end;

var
  Environment: TEnvironment;

implementation

uses {$IFDEF FPC} SysUtils {$ELSE} Windows {$ENDIF};

{ TEnvironment }

constructor TEnvironment.Create;

  procedure ProcessVariable(V: string);
  var
    n: string;
  begin
    n := Copy(V, 1, Pos('=', V) - 1);
    V := Copy(V, Pos('=', V) + 1);
    if n <> '' then
      SetValue(n, V);
  end;

var
{$IFDEF FPC}
  i: Integer;
{$ELSE}
  V, VI: PChar;
{$ENDIF}
begin
  FVariables := TDict<string>.Create;

{$IFDEF FPC}
  for i := 1 to GetEnvironmentVariableCount do
    ProcessVariable(GetEnvironmentString(i));
{$ELSE}
  V := GetEnvironmentStrings;
  if V <> nil then
    try
      VI := V;
      while VI^ <> #0 do
      begin
        ProcessVariable(VI);
        Inc(VI, Length(VI) + 1);
      end;
    finally
      FreeEnvironmentStrings(V);
    end;
{$ENDIF}
end;

destructor TEnvironment.Destroy;
begin
  FVariables.Free;
  inherited;
end;

function TEnvironment.GetValue(Name: string): string;
begin
  if FVariables.Contains(Name) then
    Result := FVariables[Name]
  else
    Result := '';
end;

procedure TEnvironment.SetValue(Name, Value: string);
begin
  if Value <> '' then
    FVariables.Put(Name, Value)
  else
    FVariables.Delete(Name);
end;

initialization

Environment := TEnvironment.Create;

finalization

Environment.Free;

end.
