unit uTypeFactory;

interface

uses
  uArray, uType;

type
  TPredefinedTypeId = (pdtDate, pdtFileAttr, pdtRex);

type
  TTypeFactory = class(TObject)
  public
    constructor Create;
    destructor Destroy; override;
  private
    FTypes: TArray<TType>;
    function Find(Hash: string): Integer;
    function Get(Hash: string): TType;
    procedure Add(Hash: string; TypeInfo: TType);
  public
    function GetAny: TType;
    function GetBool: TType;
    function GetFloat: TType;
    function GetFunc(ParamTypes, ResultTypes: TType; MonoResult: Boolean): TType;
    function GetInt: TType;
    function GetList(ItemType: TType = nil): TType;
    function GetSet(ItemType: TType = nil): TType;
    function GetPipe(ItemType: TType): TType;
    function GetString: TType;
    function GetStruct(Fields: array of TType; Names: array of string): TType;
    function GetVoid: TType;
    function GetPredefined(TypeId: TPredefinedTypeId): TType;
  end;

var
  TypeFactory: TTypeFactory;

implementation

uses uIdentifierRegistry, uTypeAny, uTypeBool, uTypeFloat, uTypeFunc, uTypeInt, uTypeList, uTypeSet, uTypePipe,
  uTypeString, uTypeStruct, uTypeVoid;

{ TTypeFactory }

constructor TTypeFactory.Create;
begin
  SetLength(FTypes, 0);
end;

destructor TTypeFactory.Destroy;
var
  i: Integer;
begin
  for i := 0 to High(FTypes) do
    FTypes[i].Free;
  inherited;
end;

function TTypeFactory.Find(Hash: string): Integer;
var
  S, f, m: Integer;
begin
  S := 0;
  f := High(FTypes);
  while S <= f do
  begin
    m := S + (f - S) div 2;
    if Hash = FTypes[m].Hash then
    begin
      Result := m;
      Exit;
    end;
    if Hash < FTypes[m].Hash then
      f := m - 1
    else
      S := m + 1;
  end;
  Result := S;
end;

function TTypeFactory.Get(Hash: string): TType;
var
  i: Integer;
begin
  i := Find(Hash);
  if (i <= High(FTypes)) and (FTypes[i].Hash = Hash) then
    Result := FTypes[i]
  else
    Result := nil;
end;

function TTypeFactory.GetAny: TType;
begin
  Result := Get(RI_ANY);
  if Result = nil then
  begin
    Result := TTypeAny.Create(RI_ANY);
    Add(RI_ANY, Result);
  end;
end;

procedure TTypeFactory.Add(Hash: string; TypeInfo: TType);
var
  n, i: Integer;
begin
  n := Find(Hash);
  if (n < Length(FTypes)) and (FTypes[n].Hash = Hash) then
    Exit;
  SetLength(FTypes, Length(FTypes) + 1);
  for i := High(FTypes) downto n + 1 do
    FTypes[i] := FTypes[i - 1];
  FTypes[n] := TypeInfo;
end;

function TTypeFactory.GetBool: TType;
begin
  Result := Get(RI_BOOL);
  if Result = nil then
  begin
    Result := TTypeBool.Create(RI_BOOL);
    Add(RI_BOOL, Result);
  end;
end;

function TTypeFactory.GetFloat: TType;
begin
  Result := Get(RI_FLOAT);
  if Result = nil then
  begin
    Result := TTypeFloat.Create(RI_FLOAT);
    Add(RI_FLOAT, Result);
  end;
end;

function TTypeFactory.GetFunc(ParamTypes, ResultTypes: TType; MonoResult: Boolean): TType;
var
  h, r: string;
begin
  h := 'func(' + Copy(ParamTypes.Hash, 8, Length(ParamTypes.Hash) - 8);
  if MonoResult then
    r := ResultTypes.Hash
  else
    r := Copy(ResultTypes.Hash, 8, Length(ResultTypes.Hash) - 8);
  if r <> '' then
    h := h + ' -> ' + r;
  h := h + ')';
  Result := Get(h);
  if Result = nil then
  begin
    Result := TTypeFunc.Create(h, ParamTypes, ResultTypes, MonoResult);
    Add(h, Result);
  end;
end;

function TTypeFactory.GetInt: TType;
begin
  Result := Get(RI_INT);
  if Result = nil then
  begin
    Result := TTypeInt.Create(RI_INT);
    Add(RI_INT, Result);
  end;
end;

function TTypeFactory.GetList(ItemType: TType = nil): TType;
var
  h: string;
begin
  if ItemType <> nil then
    h := 'list(' + ItemType.Hash + ')'
  else
    h := 'list()';
  Result := Get(h);
  if Result = nil then
  begin
    Result := TTypeList.Create(h, ItemType);
    Add(h, Result);
  end;
end;

function TTypeFactory.GetPipe(ItemType: TType): TType;
var
  h: string;
begin
  h := 'pipe(' + ItemType.Hash + ')';
  Result := Get(h);
  if Result = nil then
  begin
    Result := TTypePipe.Create(h, ItemType);
    Add(h, Result);
  end;
end;

function TTypeFactory.GetPredefined(TypeId: TPredefinedTypeId): TType;
begin
  case TypeId of
    pdtDate:
      Result := TypeFactory.GetStruct([TypeFactory.GetInt, TypeFactory.GetInt, TypeFactory.GetInt, TypeFactory.GetInt,
        TypeFactory.GetInt, TypeFactory.GetInt], ['year', 'month', 'day', 'hour', 'minute', 'second']);
    pdtFileAttr:
      Result := TypeFactory.GetStruct([TypeFactory.GetInt, TypeFactory.GetInt], ['size', 'attr']);
    pdtRex:
      Result := TypeFactory.GetStruct([TypeFactory.GetString, TypeFactory.GetInt, TypeFactory.GetInt],
        ['match', 'pos', 'length']);
  else
    Result := nil;
  end;
end;

function TTypeFactory.GetSet(ItemType: TType = nil): TType;
var
  h: string;
begin
  if ItemType <> nil then
    h := 'set(' + ItemType.Hash + ')'
  else
    h := 'set()';
  Result := Get(h);
  if Result = nil then
  begin
    Result := TTypeSet.Create(h, ItemType);
    Add(h, Result);
  end;
end;

function TTypeFactory.GetString: TType;
begin
  Result := Get(RI_STRING);
  if Result = nil then
  begin
    Result := TTypeString.Create(RI_STRING);
    Add(RI_STRING, Result);
  end;
end;

function TTypeFactory.GetVoid: TType;
begin
  Result := Get('void');
  if Result = nil then
  begin
    Result := TTypeVoid.Create('void');
    Add('void', Result);
  end;
end;

function TTypeFactory.GetStruct(Fields: array of TType; Names: array of string): TType;
var
  h: string;
  i: Integer;
begin
  h := 'struct(';
  for i := 0 to High(Fields) do
  begin
    if i < Length(Names) then
      h := h + Names[i] + ': ';
    h := h + Fields[i].Hash;
    if i < High(Fields) then
      h := h + ', ';
  end;
  h := h + ')';
  Result := Get(h);
  if Result = nil then
  begin
    Result := TTypeStruct.Create(h, Fields, Names);
    Add(h, Result);
  end;
end;

initialization

TypeFactory := TTypeFactory.Create;

finalization

TypeFactory.Free;

end.
