unit uTypeVoid;

interface

uses
  uType, uData;

type
  TTypeVoid = class(TType)
  public
    function Compatible(T: TType; StrictCompat: Boolean = False): Boolean; override;
    function Default: TData; override;
  end;

implementation

{ TTypeVoid }

function TTypeVoid.Compatible(T: TType; StrictCompat: Boolean = False): Boolean;
begin
  Result := T is TTypeVoid;
end;

function TTypeVoid.Default: TData;
begin
  Result := nil;
end;

end.
