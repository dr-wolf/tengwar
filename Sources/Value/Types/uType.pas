unit uType;

interface

uses
  uOperations, uData;

type
  TType = class(TObject)
  public
    constructor Create(Hash: string);
  protected
    FHash: string;
  public
    property Hash: string read FHash;
    function Apply(Op: TBinaryOp; T: TType): TType; overload; virtual;
    function Apply(Op: TUnaryOp): TType; overload; virtual;
    function Call(T: TType): TType; virtual;
    function Compatible(T: TType; StrictCompat: Boolean = False): Boolean; virtual; abstract;
    function Default: TData; virtual; abstract;
    function NestedType(Field: string = ''): TType; virtual;
  end;

implementation

uses uExceptions;

{ TType }

constructor TType.Create(Hash: string);
begin
  FHash := Hash;
end;

function TType.Apply(Op: TBinaryOp; T: TType): TType;
begin
  if not Compatible(T) then
    raise ECodeException.Create(E_TYPE_UNCOMPATIBLE, FHash);
end;

function TType.Apply(Op: TUnaryOp): TType;
begin
  raise ECodeException.Create(E_UNSUPPORTED_OPERATION, FHash);
end;

function TType.Call(T: TType): TType;
begin
  raise ECodeException.Create(E_NOT_CALLABLE);
end;

function TType.NestedType(Field: string): TType;
begin
  raise ECodeException.Create(E_TYPE_ATOMIC);
end;

end.
