unit uTypeStruct;

interface

uses
  uArray, uDict, uOperations, uType, uData;

type
  TTypeStruct = class(TType)
  public
    constructor Create(Hash: string; Fields: array of TType; Names: array of string);
    destructor Destroy; override;
  private
    FFieldTypes: TDict<TType>;
    FNameless: Boolean;
  public
    property Nameless: Boolean read FNameless;
    function Apply(Op: TBinaryOp; T: TType): TType; override;
    function Compatible(T: TType; StrictCompat: Boolean = False): Boolean; override;
    function Default: TData; override;
    function Fields: TArray<string>;
    function IndexOf(Field: string): Integer;
    function NestedType(Field: string = ''): TType; override;
    function FieldIndex(Field: string): Integer;
  end;

implementation

uses SysUtils, uExceptions, uTypeFactory, uDataArray;

{ TTypeStruct }

constructor TTypeStruct.Create(Hash: string; Fields: array of TType; Names: array of string);
var
  i: Integer;
begin
  inherited Create(Hash);
  if (Length(Fields) <> Length(Names)) and (Length(Names) > 0) then
    raise ECodeException.Create(E_STRUCT_MIXED);
  FNameless := (Length(Names) = 0);
  FFieldTypes := TDict<TType>.Create;
  for i := 0 to High(Fields) do
    if Length(Names) > 0 then
      FFieldTypes.Put(Names[i], Fields[i])
    else
      FFieldTypes.Put('_' + IntToStr(i), Fields[i]);
end;

destructor TTypeStruct.Destroy;
begin
  FFieldTypes.Free;
  inherited;
end;

function TTypeStruct.Apply(Op: TBinaryOp; T: TType): TType;
begin
  inherited;
  if not(Op in [boEqual, boNotEqual]) then
    raise ECodeException.Create(E_UNSUPPORTED_OPERATION, FHash);
  Result := TypeFactory.GetBool;
end;

function TTypeStruct.Default: TData;
var
  A: TArray<IData>;
  i: Integer;
begin
  SetLength(A, FFieldTypes.Count);
  FFieldTypes.Reset;
  i := 0;
  while FFieldTypes.Next do
  begin
    A[i] := FFieldTypes[FFieldTypes.Key].Default;
    Inc(i);
  end;
  Result := TDataArray.Create(A);
end;

function TTypeStruct.Compatible(T: TType; StrictCompat: Boolean = False): Boolean;
begin
  Result := (T is TTypeStruct);
  if Result and (FFieldTypes.Count = TTypeStruct(T).FFieldTypes.Count) then
  begin
    FFieldTypes.Reset;
    TTypeStruct(T).FFieldTypes.Reset;
    while FFieldTypes.Next and TTypeStruct(T).FFieldTypes.Next do
      Result := Result and FFieldTypes[FFieldTypes.Key].Compatible
        (TTypeStruct(T).FFieldTypes[TTypeStruct(T).FFieldTypes.Key], StrictCompat);
  end
  else
    Result := False;
end;

function TTypeStruct.FieldIndex(Field: string): Integer;
begin
  if not FFieldTypes.Contains(Field) then
    raise ECodeException.Create(E_FIELD_UNDECLARED, Field);
  Result := FFieldTypes.IndexOf(Field);
end;

function TTypeStruct.Fields: TArray<string>;
var
  i: Integer;
begin
  SetLength(Result, FFieldTypes.Count);
  FFieldTypes.Reset;
  i := 0;
  while FFieldTypes.Next do
  begin
    Result[i] := FFieldTypes.Key;
    Inc(i);
  end;
end;

function TTypeStruct.IndexOf(Field: string): Integer;
begin
  Result := FFieldTypes.IndexOf(Field);
end;

function TTypeStruct.NestedType(Field: string): TType;
begin
  if Field = '' then
    raise ECodeException.Create(E_SYNTAX_ERROR, '');
  if not FFieldTypes.Contains(Field) then
    raise ECodeException.Create(E_FIELD_UNDECLARED, Field);
  Result := FFieldTypes[Field];
end;

end.
