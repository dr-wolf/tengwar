unit uTypeList;

interface

uses
  uOperations, uType, uData;

type
  TTypeList = class(TType)
  public
    constructor Create(Hash: string; ItemType: TType);
  protected
    FItemType: TType;
  public
    function Apply(Op: TBinaryOp; T: TType): TType; override;
    function Apply(Op: TUnaryOp): TType; override;
    function Compatible(T: TType; StrictCompat: Boolean = False): Boolean; override;
    function Default: TData; override;
    function NestedType(Field: string = ''): TType; override;
  end;

implementation

uses uExceptions, uTypeFactory, uTypeInt, uDataArray;

{ TTypeList }

constructor TTypeList.Create(Hash: string; ItemType: TType);
begin
  inherited Create(Hash);
  FItemType := ItemType;
end;

function TTypeList.Apply(Op: TUnaryOp): TType;
begin
  if Op = uoLength then
    Result := TypeFactory.GetInt
  else
    raise ECodeException.Create(E_UNSUPPORTED_OPERATION, 'list');
end;

function TTypeList.Compatible(T: TType; StrictCompat: Boolean = False): Boolean;
var
  it: TType;
begin
  if StrictCompat then
    Result := T.ClassType = TTypeList
  else
    Result := T is TTypeList;
  if Result then
  begin
    it := (T as TTypeList).FItemType;
    Result := Result and ((FItemType = nil) or (it = nil) or FItemType.Compatible(it, StrictCompat));
  end;
end;

function TTypeList.Apply(Op: TBinaryOp; T: TType): TType;
var
  it: TType;
begin
  inherited;
  case Op of
    boAddition:
      begin
        it := (T as TTypeList).FItemType;
        if it = nil then
          Result := Self
        else if FItemType is TTypeInt then
          Result := TypeFactory.GetList(FItemType.Apply(Op, it))
        else
          Result := TypeFactory.GetList(it);
      end;
    boEqual, boNotEqual:
      Result := TypeFactory.GetBool;
  else
    raise ECodeException.Create(E_UNSUPPORTED_OPERATION, FHash);
  end;
end;

function TTypeList.Default: TData;
begin
  Result := TDataArray.Create(FItemType.Default);
end;

function TTypeList.NestedType(Field: string): TType;
begin
  if Field <> '' then
    raise ECodeException.Create(E_SYNTAX_ERROR, '');
  if FItemType = nil then
    raise ECodeException.Create(E_LIST_BOUNDS);
  Result := FItemType;
end;

end.
