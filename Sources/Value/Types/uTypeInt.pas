unit uTypeInt;

interface

uses
  uOperations, uType, uTypeFloat, uData;

type
  TTypeInt = class(TTypeFloat)
  public
    function Apply(Op: TBinaryOp; T: TType): TType; override;
    function Compatible(T: TType; StrictCompat: Boolean = False): Boolean; override;
    function Default: TData; override;
  end;

implementation

uses uExceptions, uTypeFactory, uDataNumeric;

{ TTypeInt }

function TTypeInt.Apply(Op: TBinaryOp; T: TType): TType;
begin
  if not((T is TTypeInt) or (T is TTypeFloat)) then
    raise ECodeException.Create(E_TYPE_UNCOMPATIBLE, FHash);
  case Op of
    boAddition .. boMultiplication, boModulo, boPower:
      if T is TTypeInt then
        Result := TypeFactory.GetInt
      else
        Result := TypeFactory.GetFloat;
    boDivision:
      Result := TypeFactory.GetFloat;
    boRange:
      if T is TTypeInt then
        Result := TypeFactory.GetSet(TypeFactory.GetInt)
      else
        raise ECodeException.Create(E_UNSUPPORTED_OPERATION, FHash);
    boEqual .. boGreaterEqual:
      Result := TypeFactory.GetBool;
  else
    raise ECodeException.Create(E_UNSUPPORTED_OPERATION, FHash);
  end;
end;

function TTypeInt.Compatible(T: TType; StrictCompat: Boolean = False): Boolean;
begin
  if StrictCompat then
    Result := T.ClassType = TTypeInt
  else
    Result := T is TTypeFloat;
end;

function TTypeInt.Default: TData;
begin
  Result := TDataNumeric.Create(0, 0, True);
end;

end.
