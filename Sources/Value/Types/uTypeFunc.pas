unit uTypeFunc;

interface

uses
  uOperations, uType, uData;

type
  TTypeFunc = class(TType)
  public
    constructor Create(Hash: string; ParamTypes, ResultTypes: TType; SingleResult: Boolean);
  private
    FSingleResult: Boolean;
    FParamTypes: TType;
    FResultTypes: TType;
  public
    property Params: TType read FParamTypes;
    function Apply(Op: TBinaryOp; T: TType): TType; override;
    function Call(T: TType): TType; override;
    function Compatible(T: TType; StrictCompat: Boolean = False): Boolean; override;
    function Default: TData; override;
  end;

implementation

uses uExceptions, uArray, uStringSet, uContext, uTypeFactory, uTypeStruct, uDataFunc, uCustomFunc;

{ TTypeFunc }

constructor TTypeFunc.Create(Hash: string; ParamTypes, ResultTypes: TType; SingleResult: Boolean);
var
  i: Integer;
  p: TStringSet;
begin
  inherited Create(Hash);
  if not SingleResult and (ResultTypes is TTypeStruct) then
  begin
    p := TStringSet.Create((ParamTypes as TTypeStruct).Fields);
    for i := 0 to High((ResultTypes as TTypeStruct).Fields) do
      if p.IndexOf((ResultTypes as TTypeStruct).Fields[i]) >= 0 then
        raise ECodeException.Create(E_FIELD_REDECLARED, (ResultTypes as TTypeStruct).Fields[i]);
  end;
  FSingleResult := SingleResult;
  FParamTypes := ParamTypes;
  FResultTypes := ResultTypes;
end;

function TTypeFunc.Apply(Op: TBinaryOp; T: TType): TType;
begin
  inherited;
  if not(Op in [boEqual, boNotEqual]) then
    raise ECodeException.Create(E_UNSUPPORTED_OPERATION);
  Result := TypeFactory.GetBool;
end;

function TTypeFunc.Call(T: TType): TType;
var
  f: TArray<string>;
  i: Integer;
begin
  if (T is TTypeStruct) and not TTypeStruct(T).Nameless then
  begin
    f := TTypeStruct(T).Fields;
    for i := 0 to High(f) do
      if not FParamTypes.NestedType(f[i]).Compatible(T.NestedType(f[i])) then
        raise ECodeException.Create(E_TYPE_UNCOMPATIBLE, FParamTypes.NestedType(f[i]).Hash)
  end else if not FParamTypes.Compatible(T) then
    raise ECodeException.Create(E_ARG_MISMATCH);
  Result := FResultTypes;
end;

function TTypeFunc.Compatible(T: TType; StrictCompat: Boolean = False): Boolean;
begin
  Result := (T is TTypeFunc) and FParamTypes.Compatible(TTypeFunc(T).FParamTypes, True) and
    FResultTypes.Compatible(TTypeFunc(T).FResultTypes, True);
end;

function TTypeFunc.Default: TData;
var
  i: Integer;
  c: TContext;
  p, r: TArray<Integer>;
begin
  c := TContext.Create;
  SetLength(p, Length(TTypeStruct(FParamTypes).Fields));
  for i := 0 to High(p) do
    p[i] := c.Keep(FParamTypes.NestedType(TTypeStruct(FParamTypes).Fields[i]).Default,
      TTypeStruct(FParamTypes).Fields[i], False);
  if FSingleResult then
  begin
    SetLength(r, 1);
    r[0] := c.Keep(FResultTypes.Default, '$', False);
  end else begin
    if FResultTypes is TTypeStruct then
      SetLength(r, Length((FResultTypes as TTypeStruct).Fields))
    else
      SetLength(r, 0);
    for i := 0 to High(r) do
      r[i] := c.Keep(FResultTypes.NestedType(TTypeStruct(FResultTypes).Fields[i]).Default,
        TTypeStruct(FResultTypes).Fields[i], False);
  end;
  Result := TDataFunc.Create(TCustomFunc.Create(c, [], p, r));
end;

end.
