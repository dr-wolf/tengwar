unit uTypeBool;

interface

uses
  uOperations, uType, uData;

type
  TTypeBool = class(TType)
  public
    function Apply(Op: TBinaryOp; T: TType): TType; override;
    function Apply(Op: TUnaryOp): TType; override;
    function Compatible(T: TType; StrictCompat: Boolean = False): Boolean; override;
    function Default: TData; override;
  end;

implementation

uses uExceptions, uTypeFactory, uDataBool;

{ TTypeBool }

function TTypeBool.Apply(Op: TBinaryOp; T: TType): TType;
begin
  inherited;
  if not(Op in [boAnd, boOr, boEqual, boNotEqual]) then
    raise ECodeException.Create(E_UNSUPPORTED_OPERATION, FHash);
  Result := TypeFactory.GetBool;
end;

function TTypeBool.Apply(Op: TUnaryOp): TType;
begin
  if Op <> uoNot then
    raise ECodeException.Create(E_UNSUPPORTED_OPERATION, 'bool');
  Result := Self;
end;

function TTypeBool.Compatible(T: TType; StrictCompat: Boolean = False): Boolean;
begin
  Result := T is TTypeBool;
end;

function TTypeBool.Default: TData;
begin
  Result := TDataBool.Create;
end;

end.
