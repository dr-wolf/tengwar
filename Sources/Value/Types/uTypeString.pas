unit uTypeString;

interface

uses
  uOperations, uType, uData;

type
  TTypeString = class(TType)
  public
    function Apply(Op: TBinaryOp; T: TType): TType; override;
    function Compatible(T: TType; StrictCompat: Boolean = False): Boolean; override;
    function Default: TData; override;
  end;

implementation

uses uExceptions, uTypeFactory, uDataString;

{ TTypeString }

function TTypeString.Apply(Op: TBinaryOp; T: TType): TType;
begin
  inherited;
  case Op of
    boAddition:
      Result := TypeFactory.GetString;
    boEqual .. boGreaterEqual:
      Result := TypeFactory.GetBool;
  else
    raise ECodeException.Create(E_UNSUPPORTED_OPERATION, FHash);
  end;
end;

function TTypeString.Compatible(T: TType; StrictCompat: Boolean = False): Boolean;
begin
  Result := T is TTypeString;
end;

function TTypeString.Default: TData;
begin
  Result := TDataString.Create;
end;

end.
