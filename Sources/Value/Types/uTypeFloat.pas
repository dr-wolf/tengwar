unit uTypeFloat;

interface

uses
  uOperations, uType, uData;

type
  TTypeFloat = class(TType)
  public
    function Apply(Op: TBinaryOp; T: TType): TType; override;
    function Apply(Op: TUnaryOp): TType; override;
    function Compatible(T: TType; StrictCompat: Boolean = False): Boolean; override;
    function Default: TData; override;
  end;

implementation

uses uExceptions, uTypeFactory, uDataNumeric;

{ TTypeFloat }

function TTypeFloat.Apply(Op: TBinaryOp; T: TType): TType;
begin
  inherited;
  case Op of
    boAddition .. boPower:
      Result := TypeFactory.GetFloat;
    boEqual .. boGreaterEqual:
      Result := TypeFactory.GetBool;
  else
    raise ECodeException.Create(E_UNSUPPORTED_OPERATION, FHash);
  end;
end;

function TTypeFloat.Apply(Op: TUnaryOp): TType;
begin
  if Op in [uoPositive, uoNegative] then
    Result := Self
  else
    raise ECodeException.Create(E_UNSUPPORTED_OPERATION, 'float');
end;

function TTypeFloat.Compatible(T: TType; StrictCompat: Boolean = False): Boolean;
begin
  if StrictCompat then
    Result := T.ClassType = TTypeFloat
  else
    Result := T is TTypeFloat;
end;

function TTypeFloat.Default: TData;
begin
  Result := TDataNumeric.Create(0, 0, False);
end;

end.
