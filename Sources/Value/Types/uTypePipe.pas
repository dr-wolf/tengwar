unit uTypePipe;

interface

uses
  uOperations, uType, uData;

type
  TTypePipe = class(TType)
  public
    constructor Create(Hash: string; ItemType: TType);
  protected
    FItemType: TType;
  public
    function Apply(Op: TBinaryOp; T: TType): TType; override;
    function Apply(Op: TUnaryOp): TType; override;
    function Compatible(T: TType; StrictCompat: Boolean = False): Boolean; override;
    function Default: TData; override;
    function NestedType(Field: string = ''): TType; override;
  end;

implementation

uses uExceptions, uTypeFactory, uDataPipe;

{ TTypePipe }

constructor TTypePipe.Create(Hash: string; ItemType: TType);
begin
  inherited Create(Hash);
  FItemType := ItemType;
end;

function TTypePipe.Apply(Op: TUnaryOp): TType;
begin
  case Op of
    uoPop:
      Result := FItemType;
    uoLength:
      Result := TypeFactory.GetBool
  else
    raise ECodeException.Create(E_UNSUPPORTED_OPERATION, 'pipe');
  end;
end;

function TTypePipe.Compatible(T: TType; StrictCompat: Boolean = False): Boolean;
begin
  Result := (T is TTypePipe) and FItemType.Compatible((T as TTypePipe).FItemType, True);
end;

function TTypePipe.Apply(Op: TBinaryOp; T: TType): TType;
begin
  inherited;
  if Op in [boEqual, boNotEqual] then
    Result := TypeFactory.GetBool
  else
    raise ECodeException.Create(E_UNSUPPORTED_OPERATION, FHash);
end;

function TTypePipe.Default: TData;
begin
  Result := TDataPipe.Create(FItemType.Default);
end;

function TTypePipe.NestedType(Field: string): TType;
begin
  if Field <> '' then
    raise ECodeException.Create(E_SYNTAX_ERROR, '');
  Result := FItemType;
end;

end.
