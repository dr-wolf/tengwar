unit uDataBool;

interface

uses
  uOperations, uData, uOpCache;

type
  TDataBool = class(TData)
  public
    constructor Create; overload;
    constructor Create(const Value: Boolean); overload;
    constructor Create(const Value: string); overload;
  private
    FValue: Boolean;
  public
    function Apply(Op: TBinaryOp; const Data: IData): IData; override;
    function Apply(Op: TUnaryOp): IData; override;
    function Clone: IData; override;
    function IsEqual(const Data: IData): Boolean; override;
    function Log: string; override;
    function Serialize(const Cache: TOpCache): TClassStream; override;
    procedure Assign(const Data: IData); override;
  end;

function AsBool(const Data: IData): Boolean;

implementation

uses uExceptions, uIdentifierRegistry, uOpCode;

function AsBool(const Data: IData): Boolean;
begin
  if Data.Unwrap is TDataBool then
    Result := (Data.Unwrap as TDataBool).FValue
  else
    raise ERuntimeException.Create(E_TYPE_UNCOMPATIBLE, nil);
end;

{ TDataBool }

constructor TDataBool.Create;
begin
  inherited Create;
  FValue := False;
end;

constructor TDataBool.Create(const Value: Boolean);
begin
  inherited Create;
  FValue := Value;
end;

constructor TDataBool.Create(const Value: string);
begin
  inherited Create;
  FValue := Value = RI_TRUE;
end;

procedure TDataBool.Assign(const Data: IData);
begin
  FValue := AsBool(Data);
end;

function TDataBool.Apply(Op: TBinaryOp; const Data: IData): IData;
begin
  case Op of
    boAnd:
      Result := TDataBool.Create(FValue and AsBool(Data));
    boOr:
      Result := TDataBool.Create(FValue or AsBool(Data));
    boEqual:
      Result := TDataBool.Create(IsEqual(Data));
    boNotEqual:
      Result := TDataBool.Create(not IsEqual(Data));
  else
    raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
  end
end;

function TDataBool.Apply(Op: TUnaryOp): IData;
begin
  if Op = uoNot then
    Result := TDataBool.Create(not FValue)
  else
    Result := Self;
end;

function TDataBool.Clone: IData;
begin
  Result := TDataBool.Create(FValue);
end;

function TDataBool.IsEqual(const Data: IData): Boolean;
begin
  Result := FValue = AsBool(Data);
end;

function TDataBool.Log: string;
begin
  if FValue then
    Result := RI_TRUE
  else
    Result := RI_FALSE;
end;

function TDataBool.Serialize(const Cache: TOpCache): TClassStream;
begin
  Result := TClassStream.Create;
  with Result do
  begin
    WriteByte(UID_DATA_BOOL);
    WriteByte(Byte(FValue));
  end;
end;

end.
