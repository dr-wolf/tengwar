unit uDataSet;

interface

uses
  uArray, uOperations, uData, uOpCache;

type
  TDataSet = class(TData)
  public
    constructor Create; overload;
    constructor Create(const Items: array of IData); overload;
    destructor Destroy; override;
  private
    FItems: TArray<IData>;
    function Find(Data: IData): Integer;
  public
    function Apply(Op: TBinaryOp; const Data: IData): IData; override;
    function Apply(Op: TUnaryOp): IData; override;
    function IsEqual(const Data: IData): Boolean; override;
    function Clone: IData; override;
    function Count: Integer; override;
    function Item(I: Integer): IData; override;
    function Log: string; override;
    function Serialize(const Cache: TOpCache): TClassStream; override;
    procedure AddItem(const Item: IData);
    procedure Assign(const Data: IData); override;
  end;

implementation

uses uExceptions, uDataNumeric, uDataBool, uOpCode;

type
  TAssoc = (aLeft, aMiddle, aRight);
  TAssocSet = set of TAssoc;

function Merge(Left, Right: TArray<IData>; Assoc: TAssocSet): TArray<IData>;

  function Pink(var N: Integer): Integer;
  begin
    Result := N;
    Inc(N);
  end;

var
  L, R, N, I: Integer;
begin
  L := 0;
  R := 0;
  N := 0;
  SetLength(Result, Length(Left) + Length(Right));
  while (L <= High(Left)) and (R <= High(Right)) do
  begin
    if Left[L].Log = Right[R].Log then
    begin
      if aMiddle in Assoc then
        Result[Pink(N)] := Left[L];
      Inc(L);
      Inc(R);
    end else if Left[L].Log < Right[R].Log then
    begin
      if aLeft in Assoc then
        Result[Pink(N)] := Left[L];
      Inc(L);
    end else begin
      if aRight in Assoc then
        Result[Pink(N)] := Right[R];
      Inc(R);
    end;
  end;
  if aLeft in Assoc then
    for I := L to High(Left) do
      Result[Pink(N)] := Left[I];
  if aRight in Assoc then
    for I := R to High(Right) do
      Result[Pink(N)] := Right[I];
  SetLength(Result, N);
end;

{ TDataSet }

constructor TDataSet.Create;
begin
  inherited Create;
  SetLength(FItems, 0);
end;

constructor TDataSet.Create(const Items: array of IData);
var
  I: Integer;
begin
  inherited Create;
  SetLength(FItems, 0);
  for I := 0 to High(Items) do
    AddItem(Items[I]);
end;

destructor TDataSet.Destroy;
begin
  FItems := nil;
  inherited;
end;

procedure TDataSet.AddItem(const Item: IData);
var
  I, N: Integer;
begin
  N := Find(Item);
  if (N < Length(FItems)) and FItems[N].IsEqual(Item) then
    Exit;
  SetLength(FItems, Length(FItems) + 1);
  for I := High(FItems) downto N + 1 do
    FItems[I] := FItems[I - 1];
  FItems[N] := Item;
end;

function TDataSet.Apply(Op: TBinaryOp; const Data: IData): IData;
begin
  LockAll(Data);
  case Op of
    boAddition:
      Result := TDataSet.Create(Merge(FItems, (Data as TDataSet).FItems, [aLeft, aMiddle, aRight]));
    boMultiplication:
      Result := TDataSet.Create(Merge(FItems, (Data as TDataSet).FItems, [aMiddle]));
    boDivision:
      Result := TDataSet.Create(Merge(FItems, (Data as TDataSet).FItems, [aLeft]));
    boLess:
      Result := TDataBool.Create(Length(Merge(FItems, (Data as TDataSet).FItems, [aLeft])) = 0);
    boGreater:
      Result := TDataBool.Create(Length(Merge(FItems, (Data as TDataSet).FItems, [aRight])) = 0);
    boEqual:
      Result := TDataBool.Create(IsEqual(Data));
    boNotEqual:
      Result := TDataBool.Create(Length(Merge(FItems, (Data as TDataSet)
      .FItems, [aMiddle])) = 0);
  else
    raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
  end;
  UnlockAll(Data);
end;

function TDataSet.Apply(Op: TUnaryOp): IData;
begin
  if Op = uoLength then
    Result := TDataNumeric.Create(Length(FItems))
  else
    raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
end;

procedure TDataSet.Assign(const Data: IData);
var
  I: Integer;
begin
  LockAll(Data);
  SetLength(FItems, Length((Data as TDataSet).FItems));
  for I := 0 to High(FItems) do
    FItems[I] := (Data as TDataSet).FItems[I].Clone;
  UnlockAll(Data);
end;

function TDataSet.Clone: IData;
var
  I: Integer;
  R: TDataSet;
begin
  R := TDataSet.Create();
  Lock;
  SetLength(R.FItems, Length(FItems));
  for I := 0 to High(R.FItems) do
    R.FItems[I] := FItems[I].Clone;
  Unlock;
  Result := R;
end;

function TDataSet.Count: Integer;
begin
  Result := Length(FItems);
end;

function TDataSet.Find(Data: IData): Integer;
var
  s, f, m: Integer;
begin
  s := 0;
  f := High(FItems);
  while s <= f do
  begin
    m := s + (f - s) div 2;
    if Data.IsEqual(FItems[m]) then
    begin
      Result := m;
      Exit;
    end else if AsBool(Data.Apply(boLess, FItems[m])) then
      f := m - 1
    else
      s := m + 1;
  end;
  Result := s;
end;

function TDataSet.IsEqual(const Data: IData): Boolean;
var
  I: Integer;
begin
  LockAll(Data);
  Result := Count = Data.Count;
  if Result then
    for I := 0 to High(FItems) do
    begin
      Result := Result and (FItems[I].IsEqual(Data.Item(I)));
      if not Result then
        Exit;
    end;
  UnlockAll(Data);
end;

function TDataSet.Item(I: Integer): IData;
begin
  if (I >= 0) and (I < Length(FItems)) then
    Result := FItems[I]
  else
    raise ERuntimeException.Create(E_LIST_BOUNDS, nil);
end;

function TDataSet.Log: string;
var
  I: Integer;
begin
  Lock;
  if Length(FItems) = 0 then
    Result := '[::]'
  else
  begin
    Result := '[:' + FItems[0].Log;
    for I := 1 to High(FItems) do
      Result := Result + ', ' + FItems[I].Log;
    Result := Result + ':]';
  end;
  Unlock;
end;

function TDataSet.Serialize(const Cache: TOpCache): TClassStream;
var
  I: Integer;
begin
  Result := TClassStream.Create;
  with Result do
  begin
    WriteByte(UID_DATA_SET);
    WriteInteger(Length(FItems));
    for I := 0 to High(FItems) do
      WriteInteger(Cache.Save(FItems[I] as TObject, (FItems[I] as TData).Serialize));
  end;
end;

end.
