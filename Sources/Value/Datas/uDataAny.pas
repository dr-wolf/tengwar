unit uDataAny;

interface

uses
  uOperations, uData, uOpCache;

type
  TDataAny = class(TData)
  public
    constructor Create(const Data: IData);
  private
    FData: IData;
  public
    function Apply(Op: TBinaryOp; const Data: IData): IData; overload; override;
    function Apply(Op: TUnaryOp): IData; overload; override;
    function Clone: IData; override;
    function Count: Integer; override;
    function Unwrap: IData; override;
    function IsEqual(const Data: IData): Boolean; override;
    function Item(I: Integer): IData; override;
    function Log: string; override;
    function Serialize(const Cache: TOpCache): TClassStream; override;
    procedure Assign(const Data: IData); override;
  end;

implementation

uses uOpCode;

{ TDataAny }

constructor TDataAny.Create(const Data: IData);
begin
  inherited Create;
  FData := Data;
end;

function TDataAny.Unwrap: IData;
begin
  Result := FData;
end;

function TDataAny.Apply(Op: TUnaryOp): IData;
begin
  Result := FData.Apply(Op);
end;

function TDataAny.Apply(Op: TBinaryOp; const Data: IData): IData;
begin
  Result := FData.Apply(Op, Data);
end;

function TDataAny.Clone: IData;
begin
  Result := TDataAny.Create(FData);
end;

function TDataAny.Count: Integer;
begin
  Result := FData.Count;
end;

function TDataAny.IsEqual(const Data: IData): Boolean;
begin
  Result := FData.IsEqual(Data);
end;

function TDataAny.Item(I: Integer): IData;
begin
  Result := FData.Item(I);
end;

function TDataAny.Log: string;
begin
  Result := FData.Log;
end;

function TDataAny.Serialize(const Cache: TOpCache): TClassStream;
begin
  Result := TClassStream.Create;
  with Result do
  begin
    WriteByte(UID_DATA_ANY);
    WriteInteger(Cache.Save(FData as TObject, (FData as TData).Serialize));
  end;
end;

procedure TDataAny.Assign(const Data: IData);
begin
  FData := Data.Clone;
end;

end.
