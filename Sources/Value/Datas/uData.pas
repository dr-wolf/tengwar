unit uData;

interface

uses
  uOperations, uOpCache, SyncObjs;

type
  IData = interface(IInterface)
    function Apply(Op: TBinaryOp; const Data: IData): IData; overload;
    function Apply(Op: TUnaryOp): IData; overload;
    function Clone: IData;
    function Count: Integer;
    function Unwrap: IData;
    function IsEqual(const Data: IData): Boolean;
    function Item(I: Integer): IData;
    function Log: string;
    function Serialize(const Cache: TOpCache): TClassStream;
    procedure Assign(const Data: IData);
    procedure Lock;
    procedure Unlock;
  end;

type
  TData = class(TInterfacedObject, IData)
  public
    constructor Create;
  private
    FLock: TCriticalSection;
  protected
    procedure LockAll(Data: IData);
    procedure UnlockAll(Data: IData);
  public
    function Apply(Op: TBinaryOp; const Data: IData): IData; overload; virtual; abstract;
    function Apply(Op: TUnaryOp): IData; overload; virtual;
    function Clone: IData; virtual; abstract;
    function Count: Integer; virtual;
    function Unwrap: IData; virtual;
    function IsEqual(const Data: IData): Boolean; virtual; abstract;
    function Item(I: Integer): IData; virtual;
    function Log: string; virtual; abstract;
    function Serialize(const Cache: TOpCache): TClassStream; virtual; abstract;
    procedure Assign(const Data: IData); virtual; abstract;
    procedure Lock;
    procedure Unlock;
  end;

implementation

uses uArray, uExceptions;

function AsIntegerArray(Items: array of Integer): TArray<Integer>;
var
  I: Integer;
begin
  if Length(Items) > 0 then
  begin
    SetLength(Result, Length(Items));
    for I := 0 to High(Items) do
      Result[I] := Items[I];
  end
  else
    Result := nil;
end;

{ TData }

function TData.Count: Integer;
begin
  raise ERuntimeException.Create(E_TYPE_ATOMIC, nil);
end;

constructor TData.Create;
begin
  FLock := TCriticalSection.Create;
end;

function TData.Unwrap: IData;
begin
  Result := Self;
end;

function TData.Item(I: Integer): IData;
begin
  raise ERuntimeException.Create(E_TYPE_ATOMIC, nil);
end;

function TData.Apply(Op: TUnaryOp): IData;
begin
  raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
end;

procedure TData.Lock;
begin
  FLock.Enter;
end;

procedure TData.LockAll(Data: IData);
begin
  Self.Lock;
  Data.Lock;
end;

procedure TData.Unlock;
begin
  FLock.Leave;
end;

procedure TData.UnlockAll(Data: IData);
begin
  Data.Unlock;
  Self.Unlock;
end;

end.
