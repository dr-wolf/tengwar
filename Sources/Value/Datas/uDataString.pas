unit uDataString;

interface

uses
  uOperations, uData, uOpCache;

type
  TDataString = class(TData)
  public
    constructor Create; overload;
    constructor Create(const Value: string); overload;
  private
    FValue: string;
  public
    function Apply(Op: TBinaryOp; const Data: IData): IData; override;
    function Clone: IData; override;
    function IsEqual(const Data: IData): Boolean; override;
    function Log: string; override;
    function Serialize(const Cache: TOpCache): TClassStream; override;
    procedure Assign(const Data: IData); override;
  end;

function AsString(const Data: IData): string;

implementation

uses SysUtils, uExceptions, uDataBool, uOpCode;

function AsString(const Data: IData): string;
begin
  if Data.Unwrap is TDataString then
    Result := (Data.Unwrap as TDataString).FValue
  else
    raise ERuntimeException.Create(E_TYPE_UNCOMPATIBLE, nil);
end;

{ TDataString }

constructor TDataString.Create;
begin
  inherited Create;
  FValue := '';
end;

constructor TDataString.Create(const Value: string);
begin
  inherited Create;
  FValue := Value;
end;

procedure TDataString.Assign(const Data: IData);
begin
  FValue := AsString(Data);
end;

function TDataString.Apply(Op: TBinaryOp; const Data: IData): IData;
begin
  case Op of
    boAddition:
      Result := TDataString.Create(FValue + AsString(Data));
    boEqual:
      Result := TDataBool.Create(IsEqual(Data));
    boNotEqual:
      Result := TDataBool.Create(not IsEqual(Data));
    boLess:
      Result := TDataBool.Create(FValue < AsString(Data));
    boGreater:
      Result := TDataBool.Create(FValue > AsString(Data));
    boLessEqual:
      Result := TDataBool.Create(FValue <= AsString(Data));
    boGreaterEqual:
      Result := TDataBool.Create(FValue >= AsString(Data));
  else
    raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
  end;
end;

function TDataString.Clone: IData;
begin
  Result := TDataString.Create(FValue);
end;

function TDataString.IsEqual(const Data: IData): Boolean;
begin
  Result := FValue = AsString(Data);
end;

function TDataString.Log: string;
begin
  // TODO: escape string
  Result := '"' + StringReplace(StringReplace(FValue, #10, '\n', [rfReplaceAll]), #13, '\r', [rfReplaceAll]) + '"';
end;

function TDataString.Serialize(const Cache: TOpCache): TClassStream;
begin
  Result := TClassStream.Create;
  with Result do
  begin
    WriteByte(UID_DATA_STRING);
    WriteString(FValue);
  end;
end;

end.
