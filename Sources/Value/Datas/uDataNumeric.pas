unit uDataNumeric;

interface

uses
  uOperations, uData, uOpCache;

type
  TDataNumeric = class(TData)
  public
    constructor Create(I: Int64; F: Double; S: Boolean); overload;
    constructor Create(Value: Double); overload;
    constructor Create(Value: Int64); overload;
    constructor Create(Value: string); overload;
  private
    FStrictInt: Boolean;
    FInt: Int64;
    FFrac: Double;
  public
    function Apply(Op: TBinaryOp; const Data: IData): IData; override;
    function Apply(Op: TUnaryOp): IData; override;
    function Clone: IData; override;
    function IsEqual(const Data: IData): Boolean; override;
    function Log: string; override;
    function Serialize(const Cache: TOpCache): TClassStream; override;
    procedure Assign(const Data: IData); override;
  end;

function AsInt(const Data: IData; IgnoreStrict: Boolean = False): Int64;
function AsFloat(const Data: IData; FracOnly: Boolean = False): Double;

implementation

uses SysUtils, Math, uArray, uExceptions, uDataBool, uDataSet, uOpCode;

function AsInt(const Data: IData; IgnoreStrict: Boolean = False): Int64;
begin
  if Data.Unwrap is TDataNumeric then
  begin
    if not((Data.Unwrap as TDataNumeric).FStrictInt or IgnoreStrict) then
      raise ERuntimeException.Create(E_TYPE_UNCOMPATIBLE, nil);
    Result := (Data.Unwrap as TDataNumeric).FInt;
  end
  else
    raise ERuntimeException.Create(E_TYPE_UNCOMPATIBLE, nil);
end;

function AsFloat(const Data: IData; FracOnly: Boolean = False): Double;
begin
  Data.Lock;
  if Data.Unwrap is TDataNumeric then
  begin
    Result := (Data.Unwrap as TDataNumeric).FFrac;
    if not FracOnly then
      Result := (Data.Unwrap as TDataNumeric).FInt + Result;
  end
  else
    raise ERuntimeException.Create(E_TYPE_UNCOMPATIBLE, nil);
  Data.Unlock;
end;

function Norm(var Value: Double): Int64;
begin
  Result := Trunc(Value);
  Value := Frac(Value);
end;

{ TDataNumeric }

constructor TDataNumeric.Create(I: Int64; F: Double; S: Boolean);
begin
  inherited Create;
  FStrictInt := S;
  FInt := I + Norm(F);
  FFrac := F;
end;

constructor TDataNumeric.Create(Value: Double);
begin
  inherited Create;
  FStrictInt := False;
  FInt := Norm(Value);
  FFrac := Value;
end;

constructor TDataNumeric.Create(Value: Int64);
begin
  inherited Create;
  FStrictInt := True;
  FInt := Value;
  FFrac := 0;
end;

constructor TDataNumeric.Create(Value: string);
var
  c: Integer;
  si: string;
begin
  inherited Create;
  if Copy(Value, 1, 2) = '0x' then
  begin
    Val('$' + Copy(Value, 3), FInt, c);
    if c > 0 then
      raise ECodeException.Create(E_PARSE_ERROR, Value);
    FFrac := 0;
    FStrictInt := True;
  end else begin
    si := Copy(Value, 1, Pos('.', Value + '.') - 1);
    Delete(Value, 1, Length(si) + 1);
    Val(si, FInt, c);
    if c > 0 then
      raise ECodeException.Create(E_PARSE_ERROR, Value);
    if Value <> '' then
    begin
      Val('0.' + Value, FFrac, c);
      if c > 0 then
        raise ECodeException.Create(E_PARSE_ERROR, Value);
      FStrictInt := False;
    end else begin
      FFrac := 0;
      FStrictInt := True;
    end;
  end;
end;

function TDataNumeric.Apply(Op: TBinaryOp; const Data: IData): IData;

  function Range(RangeFrom, RangeTo: Int64): TArray<IData>;
  var
    I: Int64;
  begin
    SetLength(Result, Abs(RangeFrom - RangeTo) + 1);
    if RangeFrom <= RangeTo then
    begin
      I := RangeFrom;
      while I <= RangeTo do
      begin
        Result[I - RangeFrom] := TDataNumeric.Create(I);
        Inc(I);
      end;
    end else begin
      I := RangeFrom;
      while I >= RangeTo do
      begin
        Result[-I + RangeFrom] := TDataNumeric.Create(I);
        Dec(I);
      end;
    end;
  end;

var
  D: TDataNumeric;
begin
  LockAll(Data);
  if not(Data is TDataNumeric) then
    raise ECodeException.Create(E_TYPE_UNCOMPATIBLE, 'int, float');
  D := Data as TDataNumeric;
  case Op of
    boAddition:
      Result := TDataNumeric.Create(FInt + D.FInt, FFrac + D.FFrac, FStrictInt and D.FStrictInt);
    boSubtraction:
      Result := TDataNumeric.Create(FInt - D.FInt, FFrac - D.FFrac, FStrictInt and D.FStrictInt);
    boMultiplication:
      Result := TDataNumeric.Create(FInt * D.FInt, FFrac * D.FInt + AsFloat(Self) * D.FFrac,
        FStrictInt and D.FStrictInt);
    boDivision:
      if AsFloat(D) <> 0 then
        Result := TDataNumeric.Create(AsFloat(Self) / AsFloat(D))
      else
        raise ERuntimeException.Create(E_DIVIZION_ZERO, nil);
    boModulo:
      if AsFloat(D) <> 0 then
        Result := TDataNumeric.Create(AsFloat(Self) - Trunc(AsFloat(Self) / AsFloat(D)) * AsFloat(D))
      else
        raise ERuntimeException.Create(E_DIVIZION_ZERO, nil);
    boRange:
      Result := TDataSet.Create(Range(FInt, D.FInt));
    boPower:
      Result := TDataNumeric.Create(Power(AsFloat(Self), AsFloat(D)));
    boEqual:
      Result := TDataBool.Create(IsEqual(D));
    boNotEqual:
      Result := TDataBool.Create(not IsEqual(D));
    boLess:
      Result := TDataBool.Create((FInt < D.FInt) or ((FInt = D.FInt) and (FFrac < D.FFrac)));
    boGreater:
      Result := TDataBool.Create((FInt > D.FInt) or ((FInt = D.FInt) and (FFrac > D.FFrac)));
    boLessEqual:
      Result := TDataBool.Create(not((FInt > D.FInt) or ((FInt = D.FInt) and (FFrac > D.FFrac))));
    boGreaterEqual:
      Result := TDataBool.Create(not((FInt < D.FInt) or ((FInt = D.FInt) and (FFrac < D.FFrac))));
  else
    raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
  end;
  UnlockAll(Data);
end;

function TDataNumeric.Apply(Op: TUnaryOp): IData;
begin
  case Op of
    uoPositive:
      Result := Self;
    uoNegative:
      Result := TDataNumeric.Create(-FInt, -FFrac, FStrictInt);
  else
    raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
  end;
end;

procedure TDataNumeric.Assign(const Data: IData);
begin
  if FStrictInt and not(Data as TDataNumeric).FStrictInt then
    raise ERuntimeException.Create(E_TYPE_UNCOMPATIBLE, nil);
  FInt := (Data as TDataNumeric).FInt;
  FFrac := (Data as TDataNumeric).FFrac;
end;

function TDataNumeric.Clone: IData;
begin
  Result := TDataNumeric.Create(FInt, FFrac, FStrictInt);
end;

function TDataNumeric.IsEqual(const Data: IData): Boolean;
begin
  Result := (FInt = (Data as TDataNumeric).FInt) and (FFrac = (Data as TDataNumeric).FFrac);
end;

function TDataNumeric.Log: string;
begin
  Result := FloatToStr(FInt + FFrac);
end;

function TDataNumeric.Serialize(const Cache: TOpCache): TClassStream;
begin
  Result := TClassStream.Create;
  with Result do
  begin
    WriteByte(UID_DATA_NUMERIC);
    WriteByte(Byte(FStrictInt));
    WriteInteger(FInt);
    WriteDouble(FFrac);
  end;
end;

end.
