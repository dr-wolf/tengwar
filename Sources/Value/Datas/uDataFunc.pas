unit uDataFunc;

interface

uses
  uArray, uOperations, uData, uFunction, uState, uOpCache;

type
  TDataFunc = class(TData)
  public
    constructor Create; overload;
    constructor Create(Func: IFunction); overload;
    destructor Destroy; override;
  private
    FFunc: IFunction;
    FClosures: TArray<IData>;
  public
    function Apply(Op: TBinaryOp; const Data: IData): IData; override;
    function Clone: IData; override;
    function IsEqual(const Data: IData): Boolean; override;
    function Log: string; override;
    function Serialize(const Cache: TOpCache): TClassStream; override;
    procedure Assign(const Data: IData); override;
    procedure AddClosures(const Closures: TArray<IData>);
    procedure Call(const State: TState; ParamMap: TArray<Integer>);
    procedure RefreshSelf;
    procedure SetFunc(Func: IFunction);
  end;

implementation

uses uExceptions, uDataBool, uOpCode;

{ TDataFunc }

constructor TDataFunc.Create;
begin
  inherited Create;
  FFunc := nil;
  SetLength(FClosures, 0);
end;

constructor TDataFunc.Create(Func: IFunction);
begin
  inherited Create;
  FFunc := Func;
  SetLength(FClosures, 0);
end;

destructor TDataFunc.Destroy;
begin
  FFunc := nil;
  FClosures := nil;
  inherited;
end;

procedure TDataFunc.AddClosures(const Closures: TArray<IData>);
var
  i, p: Integer;
begin
  p := High(FClosures) + 1;
  SetLength(FClosures, Length(FClosures) + Length(Closures));
  for i := 0 to High(Closures) do
    FClosures[p + i] := Closures[i];
end;

function TDataFunc.Apply(Op: TBinaryOp; const Data: IData): IData;
begin
  case Op of
    boEqual:
      Result := TDataBool.Create(IsEqual(Data));
    boNotEqual:
      Result := TDataBool.Create(not IsEqual(Data));
  else
    raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
  end
end;

procedure TDataFunc.Assign(const Data: IData);
var
  i: Integer;
begin
  LockAll(Data);
  FFunc := (Data as TDataFunc).FFunc;
  SetLength(FClosures, Length((Data as TDataFunc).FClosures));
  for i := 0 to High(FClosures) do
    FClosures[i] := (Data as TDataFunc).FClosures[i];
  UnlockAll(Data);
end;

procedure TDataFunc.Call(const State: TState; ParamMap: TArray<Integer>);
begin
  //Lock;
  FFunc.Call(State, ParamMap, FClosures);
  //Unlock;
end;

function TDataFunc.Clone: IData;
var
  R: TDataFunc;
  i: Integer;
begin
  Lock;
  R := TDataFunc.Create(FFunc);
  SetLength(R.FClosures, Length(FClosures));
  for i := 0 to High(FClosures) do
    R.FClosures[i] := FClosures[i];
  Unlock;
  Result := R;
end;

function TDataFunc.IsEqual(const Data: IData): Boolean;
var
  i: Integer;
begin
  LockAll(Data);
  Result := FFunc = (Data as TDataFunc).FFunc;
  for i := 0 to High(FClosures) do
  begin
    Result := Result and FClosures[i].IsEqual((Data as TDataFunc).FClosures[i]);
    if not Result then
      Break;
  end;
  UnlockAll(Data);
end;

function TDataFunc.Log: string;
begin
  Lock;
  Result := 'func ' + FFunc.Log;
  Unlock;
end;

procedure TDataFunc.RefreshSelf;
begin
  Lock;
  FFunc.RefreshSelf(Self);
  Unlock;
end;

function TDataFunc.Serialize(const Cache: TOpCache): TClassStream;
var
  i: Integer;
begin
  Result := TClassStream.Create;
  with Result do
  begin
    WriteByte(UID_DATA_FUNC);
    WriteInteger(Cache.Save(FFunc as TFunction, (FFunc as TFunction).Serialize));
    WriteInteger(Length(FClosures));
    for i := 0 to High(FClosures) do
      WriteInteger(Cache.Save((FClosures[i] as TData), (FClosures[i] as TData).Serialize));
  end;
end;

procedure TDataFunc.SetFunc(Func: IFunction);
begin
  FFunc := Func;
end;

end.
