unit uDataPipe;

interface

uses
  uQueue, uOperations, uData, uOpCache;

type
  TDataPipe = class(TData)
  public
    constructor Create(PlaceHolder: IData);
    destructor Destroy; override;
  private
    FPlaceHolder: IData;
    FPipe: IQueue<IData>;
  public
    function Apply(Op: TBinaryOp; const Data: IData): IData; override;
    function Apply(Op: TUnaryOp): IData; override;
    function Clone: IData; override;
    function IsEqual(const Data: IData): Boolean; override;
    function Log: string; override;
    function Serialize(const Cache: TOpCache): TClassStream; override;
    function Tail: IData;
    procedure Assign(const Data: IData); override;
  end;

implementation

uses uExceptions, uDataBool, uOpCode;

{ TDataPipe }

constructor TDataPipe.Create(PlaceHolder: IData);
begin
  inherited Create;
  FPlaceHolder := PlaceHolder;
  FPipe := TQueue<IData>.Create;
end;

destructor TDataPipe.Destroy;
begin
  inherited;
end;

function TDataPipe.Apply(Op: TBinaryOp; const Data: IData): IData;
begin
  case Op of
    boEqual:
      Result := TDataBool.Create(IsEqual(Data));
    boNotEqual:
      Result := TDataBool.Create(not IsEqual(Data));
  else
    raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
  end
end;

function TDataPipe.Apply(Op: TUnaryOp): IData;
begin
  Lock;
  case Op of
    uoPop:
      Result := FPipe.Pop;
    uoLength:
      Result := TDataBool.Create(FPipe.Count > 0);
  else
    raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
  end;
  Unlock;
end;

procedure TDataPipe.Assign(const Data: IData);
begin
  LockAll(Data);
  FPipe := (Data as TDataPipe).FPipe;
  UnlockAll(Data);
end;

function TDataPipe.Clone: IData;
begin
  Lock;
  Result := TDataPipe.Create(FPlaceHolder.Clone);
  (Result as TDataPipe).FPipe := FPipe;
  Unlock;
end;

function TDataPipe.IsEqual(const Data: IData): Boolean;
begin
  LockAll(Data);
  Result := FPipe = (Data as TDataPipe).FPipe;
  UnlockAll(Data);
end;

function TDataPipe.Log: string;
begin
  Result := '<pipe>';
end;

function TDataPipe.Serialize(const Cache: TOpCache): TClassStream;
begin
  Result := TClassStream.Create;
  with Result do
  begin
    WriteByte(UID_DATA_PIPE);
    WriteInteger(Cache.Save(FPlaceHolder as TData, (FPlaceHolder as TData).Serialize));
  end;
end;

function TDataPipe.Tail: IData;
begin
  Result := FPlaceHolder.Clone;
  FPipe.Push(Result);
end;

end.
