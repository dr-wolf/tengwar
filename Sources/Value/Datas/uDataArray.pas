unit uDataArray;

interface

uses
  uArray, uOperations, uData, uOpCache;

type
  TDataArray = class(TData)
  public
    constructor Create(PlaceHolder: IData); overload;
    constructor Create(Items: array of IData; PlaceHolder: IData = nil); overload;
    destructor Destroy; override;
  private
    FPlaceHolder: IData;
    FItems: TArray<IData>;
  public
    function Apply(Op: TBinaryOp; const Data: IData): IData; override;
    function Apply(Op: TUnaryOp): IData; override;
    function IsEqual(const Data: IData): Boolean; override;
    function Clone: IData; override;
    function Count: Integer; override;
    function Item(I: Integer): IData; override;
    function Log: string; override;
    function Serialize(const Cache: TOpCache): TClassStream; override;
    procedure Assign(const Data: IData); override;
  end;

function TranslateIndex(Index, ListSize: Integer): Integer;

implementation

uses uExceptions, uDataBool, uDataNumeric, uOpCode;

function TranslateIndex(Index, ListSize: Integer): Integer;
begin
  Result := Index;
  if Result < 0 then
    Result := Result + ListSize
  else
    Result := Result - 1;
end;

{ TDataArray }

constructor TDataArray.Create(PlaceHolder: IData);
begin
  inherited Create;
  FPlaceHolder := PlaceHolder;
  SetLength(FItems, 0);
end;

constructor TDataArray.Create(Items: array of IData; PlaceHolder: IData = nil);
var
  I: Integer;
begin
  inherited Create;
  FPlaceHolder := PlaceHolder;
  SetLength(FItems, Length(Items));
  for I := 0 to High(Items) do
    FItems[I] := Items[I];
end;

destructor TDataArray.Destroy;
begin
  FItems := nil;
  inherited;
end;

function TDataArray.Apply(Op: TBinaryOp; const Data: IData): IData;
var
  I: Integer;
  R: TArray<IData>;
begin   
  LockAll(Data);
  case Op of
    boAddition:
      begin
        SetLength(R, Length(FItems) + Data.Count);
        for I := 0 to High(FItems) do
          R[I] := FItems[I].Clone;
        for I := 0 to Data.Count - 1 do
          R[Length(FItems) + I] := Data.Item(I).Clone;
        Result := TDataArray.Create(R);
      end;
    boEqual:
      Result := TDataBool.Create(IsEqual(Data));
    boNotEqual:
      Result := TDataBool.Create(not IsEqual(Data));
  else
    raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
  end;
  UnlockAll(Data);
end;

function TDataArray.Apply(Op: TUnaryOp): IData;
begin
  Lock;
  if Op = uoLength then
    Result := TDataNumeric.Create(Length(FItems))
  else
    raise ERuntimeException.Create(E_UNSUPPORTED_OPERATION, nil);
  Unlock;
end;

procedure TDataArray.Assign(const Data: IData);
var
  I: Integer;
begin
  LockAll(Data);
  SetLength(FItems, Data.Count);
  for I := 0 to Data.Count - 1 do
  begin
    if FItems[I] = nil then
    begin
      if FPlaceHolder = nil then
        raise ERuntimeException.Create(E_TYPE_UNCOMPATIBLE, nil);
      FItems[I] := FPlaceHolder.Clone;
    end;
    FItems[I].Assign(Data.Item(I).Unwrap)
  end;
  UnlockAll(Data);
end;

function TDataArray.IsEqual(const Data: IData): Boolean;
var
  I: Integer;
begin
  LockAll(Data);
  Result := Length(FItems) = Data.Count;
  if Result then
    for I := 0 to High(FItems) do
    begin
      Result := Result and FItems[I].IsEqual(Data.Item(I));
      if not Result then
        Break;
    end;
  UnlockAll(Data);
end;

function TDataArray.Item(I: Integer): IData;
begin
  if (I >= 0) and (I < Length(FItems)) then
    Result := FItems[I]
  else
    raise ERuntimeException.Create(E_LIST_BOUNDS, nil);
end;

function TDataArray.Log: string;
var
  I: Integer;
begin
  Lock;
  if Length(FItems) = 0 then
    Result := '[]'
  else
  begin
    Result := '[' + FItems[0].Log;
    for I := 1 to High(FItems) do
      Result := Result + ', ' + FItems[I].Log;
    Result := Result + ']';
  end;
  Unlock;
end;

function TDataArray.Serialize(const Cache: TOpCache): TClassStream;
var
  I: Integer;
begin
  Result := TClassStream.Create;
  with Result do
  begin
    WriteByte(UID_DATA_ARRAY);
    if FPlaceHolder <> nil then
      WriteInteger(Cache.Save(FPlaceHolder as TData, (FPlaceHolder as TData).Serialize))
    else
      WriteInteger(-1);
    WriteInteger(Length(FItems));
    for I := 0 to High(FItems) do
      WriteInteger(Cache.Save(FItems[I] as TData, (FItems[I] as TData).Serialize));
  end;
end;

function TDataArray.Count: Integer;
begin
  Result := Length(FItems);
end;

function TDataArray.Clone: IData;
var
  I: Integer;
begin
  Lock;
  Result := TDataArray.Create(FPlaceHolder);
  SetLength((Result as TDataArray).FItems, Length(FItems));
  for I := 0 to High(FItems) do
    (Result as TDataArray).FItems[I] := FItems[I].Clone;
  Unlock;
end;

end.
