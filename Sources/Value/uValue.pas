unit uValue;

interface

uses
  uOperations, uType, uData;

type
  IValue = interface(IInterface)
    function Data: IData;
    function TypeInfo: TType;
    function Immutable: Boolean;
    function Apply(Op: TBinaryOp; Value: IValue): IValue; overload;
    function Apply(Op: TUnaryOp): IValue; overload;
    function Sub(Index: Integer): IValue; overload;
    function Sub(Field: string): IValue; overload;
    procedure Assign(Data: IData);
  end;

type
  TValue = class(TInterfacedObject, IValue)
  public
    constructor Create(TypeInfo: TType; Data: IData; Immutable: Boolean = True); overload;
    constructor Create(TypeInfo: TType); overload;
    destructor Destroy; override;
  private
    FData: IData;
    FType: TType;
    FImmutable: Boolean;
  public
    function Data: IData;
    function TypeInfo: TType;
    function Immutable: Boolean;
    function Apply(Op: TBinaryOp; Value: IValue): IValue; overload;
    function Apply(Op: TUnaryOp): IValue; overload;
    function Sub(Index: Integer): IValue; overload;
    function Sub(Field: string): IValue; overload;
    procedure Assign(Data: IData);
  end;

implementation

uses uTypeStruct;

{ TValue }

constructor TValue.Create(TypeInfo: TType; Data: IData; Immutable: Boolean = True);
begin
  FType := TypeInfo;
  FData := Data;
  FImmutable := Immutable;
end;

constructor TValue.Create(TypeInfo: TType);
begin
  FType := TypeInfo;
  FData := FType.Default;
  FImmutable := False;
end;

destructor TValue.Destroy;
begin
  FData := nil;
  inherited;
end;

function TValue.Data: IData;
begin
  Result := FData;
end;

function TValue.Immutable: Boolean;
begin
  Result := FImmutable;
end;

function TValue.Sub(Index: Integer): IValue;
begin
  Result := TValue.Create(FType.NestedType(), FData.Item(Index));
end;

function TValue.Sub(Field: string): IValue;
begin
  Result := TValue.Create(FType.NestedType(Field), FData.Item((FType as TTypeStruct).FieldIndex(Field)));
end;

function TValue.TypeInfo: TType;
begin
  Result := FType;
end;

function TValue.Apply(Op: TBinaryOp; Value: IValue): IValue;
begin
  Result := TValue.Create(FType.Apply(Op, Value.TypeInfo), FData.Apply(Op, Value.Data));
end;

function TValue.Apply(Op: TUnaryOp): IValue;
begin
  Result := TValue.Create(FType.Apply(Op), FData.Apply(Op));
end;

procedure TValue.Assign(Data: IData);
begin
  FData := Data;
end;

end.
