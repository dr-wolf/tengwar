unit uOperations;

interface

uses
  uSourceParser;

type
  TBinaryOp = (boAddition, boSubtraction, boMultiplication, boDivision, boModulo, boRange, boPower, boAnd, boOr,
    boEqual, boNotEqual, boLess, boGreater, boLessEqual, boGreaterEqual);

  TUnaryOp = (uoPositive, uoNegative, uoNot, uoLength, uoPop);

const
  OPERATIONS: set of TSymbol = [sPlus, sMinus, sStar, sSlash, sPerc, sColon, sPow, sAmp, sPipe, sEq, sLt, sGt, sNotEq,
    sLtEq, sGtEq];

function BinaryOp(Symbol: TSymbol): TBinaryOp;
function UnaryOp(Symbol: TSymbol): TUnaryOp;
function OperationPriority(Op: TBinaryOp): Byte;

implementation

uses uExceptions;

function BinaryOp(Symbol: TSymbol): TBinaryOp;
begin
  case Symbol of
    sPlus:
      Result := boAddition;
    sMinus:
      Result := boSubtraction;
    sStar:
      Result := boMultiplication;
    sSlash:
      Result := boDivision;
    sPerc:
      Result := boModulo;
    sColon:
      Result := boRange;
    sPow:
      Result := boPower;
    sAmp:
      Result := boAnd;
    sPipe:
      Result := boOr;
    sEq:
      Result := boEqual;
    sLt:
      Result := boLess;
    sGt:
      Result := boGreater;
    sNotEq:
      Result := boNotEqual;
    sLtEq:
      Result := boLessEqual;
    sGtEq:
      Result := boGreaterEqual;
  else
    raise ECodeException.Create(E_SYNTAX_ERROR);
  end;
end;

function UnaryOp(Symbol: TSymbol): TUnaryOp;
begin
  case Symbol of
    sPlus:
      Result := uoPositive;
    sMinus:
      Result := uoNegative;
    sExcl:
      Result := uoNot;
    sAt:
      Result := uoLength;
    sTilde:
      Result := uoPop;
  else
    raise ECodeException.Create(E_SYNTAX_ERROR);
  end;
end;

function OperationPriority(Op: TBinaryOp): Byte;
begin
  case Op of
    boPower:
      Result := 5;
    boMultiplication, boDivision, boModulo:
      Result := 4;
    boAddition, boSubtraction:
      Result := 3;
    boRange:
      Result := 2;
    boEqual, boNotEqual, boLess, boGreater, boLessEqual, boGreaterEqual:
      Result := 1;
    boAnd, boOr:
      Result := 0;
  else
    raise ECodeException.Create(E_SYNTAX_ERROR);
  end;
end;

end.
