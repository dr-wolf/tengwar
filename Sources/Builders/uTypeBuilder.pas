unit uTypeBuilder;

interface

uses
  uResolver, uSourceParser, uType;

function BuildType(Tokens: TTokenStream; Resolver: IResolver): TType;

implementation

uses uArray, uStringSet, uExceptions, uIdentifierRegistry, uTypeFactory, uTypeStruct;

function BuildType(Tokens: TTokenStream; Resolver: IResolver): TType;

  function BuildSingle(Token: TToken): TType;
  begin
    if Token.Value = RI_ANY then
      Result := TypeFactory.GetAny
    else if Token.Value = RI_BOOL then
      Result := TypeFactory.GetBool
    else if Token.Value = RI_INT then
      Result := TypeFactory.GetInt
    else if Token.Value = RI_FLOAT then
      Result := TypeFactory.GetFloat
    else if Token.Value = RI_STRING then
      Result := TypeFactory.GetString
    else
      Result := Resolver.ResolveType(Token.Value);
  end;

  function BuildStruct(Tokens: TTokenStream): TType;
  var
    types: TArray<TType>;
    s: TStringSet;
    t: TToken;
  begin
    SetLength(types, 0);
    s := TStringSet.Create;
    if Tokens.Token.Kind = tkIdentifier then
      repeat
        t := Tokens.Fetch(tkIdentifier);
        if s.IndexOf(t.Value) >= 0 then
          raise ECodeException.Create(E_FIELD_REDECLARED, t.Value, t.Index);
        s.Add(t.Value);
        Tokens.Test(sColon, E_SYNTAX_ERROR);
        SetLength(types, Length(types) + 1);
        types[High(types)] := BuildType(Tokens, Resolver);
        if ToSmbl(Tokens.Token.Value) <> sComma then
          Break;
        Tokens.Skip;
      until False;
    Result := TypeFactory.GetStruct(types, s.AsArray);
    s.Free;
  end;

  function BuildFunc(Tokens: TTokenStream): TType;
  var
    multi: Boolean;
    params, res: TType;
    i: Integer;
  begin
    params := BuildStruct(Tokens);

    if Eq(Tokens.Token, sArrow) then
    begin
      Tokens.Skip;
      i := Tokens.Token.Index;
      multi := Eq(Tokens.Token(1), sColon);
      if multi then
      begin
        res := BuildStruct(Tokens);
        if Length((res as TTypeStruct).Fields) = 1 then
          raise ECodeException.Create(E_SYNTAX_ERROR, '', i);
      end
      else
        res := BuildType(Tokens, Resolver);
    end else begin
      res := TypeFactory.GetVoid;
      multi := True;
    end;
    Result := TypeFactory.GetFunc(params, res, not multi);
  end;

var
  t: TToken;

begin
  Result := nil;
  try
    t := Tokens.Token;
    Tokens.Skip;
    case t.Kind of
      tkIdentifier:
        begin
          if Eq(Tokens.Token, sNameSp) then
          begin
            Tokens.Skip;
            Result := Resolver.ResolveType(Tokens.Fetch(tkIdentifier).Value, t.Value);
          end
          else
            Result := BuildSingle(t);
        end;
      tkKeyword:
        begin
          Tokens.Test(sBrOp, E_SYNTAX_ERROR);
          case ToKword(t.Value) of
            kwStruct:
              Result := BuildStruct(Tokens);
            kwSet:
              Result := TypeFactory.GetSet(BuildType(Tokens, Resolver));
            kwList:
              Result := TypeFactory.GetList(BuildType(Tokens, Resolver));
            kwPipe:
              Result := TypeFactory.GetPipe(BuildType(Tokens, Resolver));
            kwFunc:
              Result := BuildFunc(Tokens)
          else
            raise ECodeException.Create(E_SYNTAX_ERROR);
          end;
          Tokens.Test(sBrCl, E_SYNTAX_ERROR);
        end;
    else
      raise ECodeException.Create(E_SYNTAX_ERROR);
    end;
  except
    on e: ECodeException do
    begin
      e.SetPos(Tokens.Token.Index);
      raise;
    end;
  end;
end;

end.
