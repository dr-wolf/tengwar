unit uTreeAssembler;

interface

uses
  uExpression, uOperations;

type
  ITreeAssembler = interface(IInterface)
    function Attach(Expression: TExpression; Operation: TBinaryOp): ITreeAssembler;
    function Assemble: TExpression;
  end;

function MakeAssembler(Expression: TExpression): ITreeAssembler;

implementation

uses uExpressionBinary;

type
  TTreeNode = class(TInterfacedObject, ITreeAssembler)
  public
    constructor Create(Left, Right: ITreeAssembler; Operation: TBinaryOp);
  private
    FLeft: ITreeAssembler;
    FRight: ITreeAssembler;
    FOperation: TBinaryOp;
  public
    function Attach(Expression: TExpression; Operation: TBinaryOp): ITreeAssembler;
    function Assemble: TExpression;
  end;

  TTreeLeaf = class(TInterfacedObject, ITreeAssembler)
  public
    constructor Create(Expression: TExpression);
  private
    FLeaf: TExpression;
  public
    function Attach(Expression: TExpression; Operation: TBinaryOp): ITreeAssembler;
    function Assemble: TExpression;
  end;

function MakeAssembler(Expression: TExpression): ITreeAssembler;
begin
  Result := TTreeLeaf.Create(Expression);
end;

{ TTreeNode }

constructor TTreeNode.Create(Left, Right: ITreeAssembler; Operation: TBinaryOp);
begin
  FLeft := Left;
  FRight := Right;
  FOperation := Operation;
end;

function TTreeNode.Attach(Expression: TExpression; Operation: TBinaryOp): ITreeAssembler;
begin
  if OperationPriority(FOperation) >= OperationPriority(Operation) then
    Result := TTreeNode.Create(Self, TTreeLeaf.Create(Expression), Operation)
  else
  begin
    FRight := FRight.Attach(Expression, Operation);
    Result := Self;
  end;
end;

function TTreeNode.Assemble: TExpression;
begin
  Result := TExpressionBinary.Create(FLeft.Assemble, FRight.Assemble, FOperation);
end;

{ TTreeLeaf }

constructor TTreeLeaf.Create(Expression: TExpression);
begin
  FLeaf := Expression;
end;

function TTreeLeaf.Assemble: TExpression;
begin
  Result := FLeaf;
end;

function TTreeLeaf.Attach(Expression: TExpression; Operation: TBinaryOp): ITreeAssembler;
begin
  Result := TTreeNode.Create(Self, TTreeLeaf.Create(Expression), Operation);
end;

end.
