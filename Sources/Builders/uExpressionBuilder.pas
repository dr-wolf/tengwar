unit uExpressionBuilder;

interface

uses
  uResolver, uContext, uExpression, uSourceParser, uMachine;

function BuildExpression(Tokens: TTokenStream; Resolver: IResolver; Machine: TMachine; Context: TContext): TExpression;

implementation

uses uArray, uStringSet, uIdentifierRegistry, uTypeFactory, uTypeBuilder, uType, uTypeInt, uOperations, uExceptions,
  uFuncBuilder, uExpressionFunc, uExpressionList, uExpressionListItem, uExpressionListRange, uExpressionUnary,
  uExpressionCall, uExpressionCast, uExpressionSet, uExpressionStruct, uExpressionStructField, uExpressionTest,
  uExpressionPipeTail, uExpressionValue, uExpressionVariable, uDataBool, uDataNumeric, uDataSet, uDataString,
  uDataArray, uValue, uTreeAssembler;

function BuildExpression(Tokens: TTokenStream; Resolver: IResolver; Machine: TMachine; Context: TContext): TExpression;

function BuildTree(Tokens: TTokenStream): TExpression; forward;

  function BuildValue(Tokens: TTokenStream): TExpression;
  var
    t: TToken;
    v: IValue;
    f: Boolean;
  begin
    t := Tokens.Pop;
    case t.Kind of
      tkIdentifier:
        if Eq(Tokens.Token, sNameSp) then
        begin
          Tokens.Skip;
          if Tokens.Token.Kind <> tkIdentifier then
            raise ECodeException.Create(E_EXPECTED_IDENTIFIER, Tokens.Token.Value, Tokens.Token.Index);
          v := Resolver.ResolveValue(f, Tokens.Pop.Value, t.Value);
        end else if (t.Value = RI_TRUE) or (t.Value = RI_FALSE) then
          v := TValue.Create(TypeFactory.GetBool, TDataBool.Create(t.Value))
        else
        begin
          v := Resolver.ResolveValue(f, t.Value);
          if (Context <> nil) and not v.Immutable then
          begin
            Result := TExpressionVariable.Create(Context.Keep(v.Data, t.Value, f), v.TypeInfo, v.Immutable);
            Exit;
          end;
        end;
      tkInteger:
        v := TValue.Create(TypeFactory.GetInt, TDataNumeric.Create(t.Value));
      tkFloat:
        v := TValue.Create(TypeFactory.GetFloat, TDataNumeric.Create(t.Value));
      tkString:
        v := TValue.Create(TypeFactory.GetString, TDataString.Create(t.Value));
    else
      raise ECodeException.Create(E_SYNTAX_ERROR, '', t.Index);
    end;
    Result := TExpressionValue.Create(v);
  end;

  function BuildItems(Tokens: TTokenStream; var t: TType): TArray<TExpression>;
  var
    e: TExpression;
  begin
    SetLength(Result, 0);
    t := nil;
    repeat
      e := BuildTree(Tokens);
      if t = nil then
        t := e.ResultType
      else if not t.Compatible(e.ResultType) then
        try
          t := t.Apply(boAddition, e.ResultType)
        except
          t := TypeFactory.GetAny;
        end;
      SetLength(Result, Length(Result) + 1);
      Result[High(Result)] := e;
      if not Eq(Tokens.Token, sComma) then
        Break
      else
        Tokens.Skip;
    until False;
  end;

  function BuildList(Tokens: TTokenStream): TExpression;
  var
    e: TArray<TExpression>;
    t: TType;
  begin
    Tokens.Test(sListBgn, E_SYNTAX_ERROR);
    if Eq(Tokens.Token, sListEnd) then
      Result := TExpressionValue.Create(TValue.Create(TypeFactory.GetList, TDataArray.Create(nil)))
    else
    begin
      e := BuildItems(Tokens, t);
      Result := TExpressionList.Create(e, t);
    end;
    Tokens.Test(sListEnd, E_SYNTAX_ERROR);
  end;

  function BuildSet(Tokens: TTokenStream): TExpression;
  var
    e: TArray<TExpression>;
    t: TType;
  begin
    Tokens.Test(sSetBgn, E_SYNTAX_ERROR);
    if Eq(Tokens.Token, sSetEnd) then
      Result := TExpressionValue.Create(TValue.Create(TypeFactory.GetSet, TDataSet.Create))
    else
    begin
      e := BuildItems(Tokens, t);
      Result := TExpressionSet.Create(e, t);
    end;
    Tokens.Test(sSetEnd, E_SYNTAX_ERROR);
  end;

  function BuildFunc(Tokens: TTokenStream): TExpressionFunc;
  var
    c: TArray<Integer>;
    i, p: Integer;
  begin
    p := Tokens.Token.Index;
    with TFuncBuilder.Create(Resolver, Machine, Tokens) do
    begin
      if (ClosureNames.Count > 0) and (Context = nil) then
        raise ECodeException.Create(E_EXPECTED_STATIC, '', p);
      SetLength(c, ClosureNames.Count);
      for i := 0 to High(c) do
        c[i] := Context.Resolve(ClosureNames[i]);
      Result := TExpressionFunc.Create(Func, FuncType, c);
      Free;
    end;
  end;

  function BuildStruct(Tokens: TTokenStream; Brackets: Boolean = True): TExpressionStruct;
  var
    e: TArray<TExpression>;
    s: TStringSet;
    t: TToken;
  begin
    s := TStringSet.Create;
    SetLength(e, 0);
    if Brackets then
      Tokens.Test(sStructBgn, E_SYNTAX_ERROR);
    repeat
      if (Tokens.Token.Kind = tkIdentifier) and Eq(Tokens.Token(1), sColon) then
      begin
        t := Tokens.Fetch(tkIdentifier);
        if s.IndexOf(t.Value) >= 0 then
          raise ECodeException.Create(E_UNEXPECTED_KEYWORD, t.Value, t.Index);
        s.Add(t.Value);
        Tokens.Test(sColon, E_SYNTAX_ERROR);
      end;
      SetLength(e, Length(e) + 1);
      e[High(e)] := BuildTree(Tokens);
      if not Eq(Tokens.Token, sComma) then
        Break;
      Tokens.Skip;
    until False;
    Result := TExpressionStruct.Create(e, s.AsArray);
    s.Free;
    if Brackets then
      Tokens.Test(sStructEnd, E_SYNTAX_ERROR);
  end;

  function BuildNode(Tokens: TTokenStream): TExpression;

    function BuildListIndex(Expression: TExpression; Tokens: TTokenStream): TExpression;
    var
      f: TExpression;
    begin
      Tokens.Test(sListBgn, E_SYNTAX_ERROR);
      f := BuildExpression(Tokens, Resolver, Machine, Context);
      if f.ResultType is TTypeInt then
        Result := TExpressionListItem.Create(Expression, f)
      else
        Result := TExpressionListRange.Create(Expression, f);
      Tokens.Test(sListEnd, E_SYNTAX_ERROR);
    end;

    function BuildFuncCall(Expression: TExpression; Tokens: TTokenStream): TExpression;
    var
      p: TExpression;
    begin
      Tokens.Test(sBrOp, E_SYNTAX_ERROR);
      if not Eq(Tokens.Token, sBrCl) then
      begin
        p := BuildStruct(Tokens, False);
        if p.Immutable and not(p is TExpressionValue) then
        begin
          Result := TExpressionCall.Create(Expression, TExpressionValue.Create(p.Eval));
          p.Free;
        end
        else
          Result := TExpressionCall.Create(Expression, p);
      end
      else
        Result := TExpressionCall.Create(Expression, TExpressionStruct.Create([], []));
      Tokens.Test(sBrCl, E_SYNTAX_ERROR);
    end;

  var
    o: TUnaryOp;
    t: TToken;
  begin
    if Tokens.Token.Kind = tkKeyword then
      raise ECodeException.Create(E_UNEXPECTED_KEYWORD, Tokens.Token.Value, Tokens.Token.Index);

    if Tokens.Token.Kind = tkSymbol then
      case ToSmbl(Tokens.Token.Value) of
        sMinus, sPlus, sExcl, sAt, sTilde:
          begin
            o := UnaryOp(ToSmbl(Tokens.Pop.Value));
            Result := TExpressionUnary.Create(BuildNode(Tokens), o);
          end;
        sListBgn:
          Result := BuildList(Tokens);
        sFunc:
          Result := BuildFunc(Tokens);
        sSetBgn:
          Result := BuildSet(Tokens);
        sStructBgn:
          Result := BuildStruct(Tokens);
        sBrOp:
          begin
            Tokens.Test(sBrOp, E_SYNTAX_ERROR);
            Result := BuildExpression(Tokens, Resolver, Machine, Context);
            Tokens.Test(sBrCl, E_SYNTAX_ERROR);
          end;
      else
        raise ECodeException.Create(E_SYNTAX_ERROR, '', Tokens.Token.Index);
      end
    else
      Result := BuildValue(Tokens);

    while Tokens.Token.Kind = tkSymbol do
      case ToSmbl(Tokens.Token.Value) of
        sDot:
          begin
            Tokens.Test(sDot, E_SYNTAX_ERROR);
            Result := TExpressionStructField.Create(Result, Tokens.Fetch(tkIdentifier).Value);
          end;
        sListBgn:
          Result := BuildListIndex(Result, Tokens);
        sBrOp:
          Result := BuildFuncCall(Result, Tokens);
        sExcl:
          begin
            Tokens.Test(sExcl, E_SYNTAX_ERROR);
            Result := TExpressionCast.Create(Result, BuildType(Tokens, Resolver));
          end;
        sQuest:
          begin
            Tokens.Test(sQuest, E_SYNTAX_ERROR);
            Result := TExpressionTest.Create(Result, BuildType(Tokens, Resolver));
          end;
        sTilde:
          begin
            Tokens.Test(sTilde, E_SYNTAX_ERROR);
            Result := TExpressionPipeTail.Create(Result);
          end;
      else
        Break;
      end;
  end;

  function BuildTree(Tokens: TTokenStream): TExpression;
  var
    t: TToken;
    ta: ITreeAssembler;
  begin
    ta := MakeAssembler(BuildNode(Tokens));
    repeat
      t := Tokens.Token;
      if (t.Kind <> tkSymbol) or not(ToSmbl(t.Value) in OPERATIONS) then
        Break;
      ta := ta.Attach(BuildNode(Tokens.Skip), BinaryOp(ToSmbl(t.Value)));
    until False;
    Result := ta.Assemble;
  end;

var
  ind: Integer;

begin
  ind := Tokens.Token.Index;
  try
    Result := BuildTree(Tokens);
  except
    on e: ECodeException do
    begin
      e.SetPos(ind);
      raise;
    end;
  end;

  if Result.Immutable and not(Result is TExpressionValue) then
    Result := TExpressionValue.Create(Result.Eval);
end;

end.
