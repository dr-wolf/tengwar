unit uFuncBuilder;

interface

uses
  uArray, uDict, uStringSet, uSourceParser, uCustomFunc, uContext, uMachine, uBuilder, uResolver, uValue, uData, uType,
  uInstruction, uInstructionBlock, uInstructionEach, uInstructionIf, uInstructionWhile, uInstructionWith,
  uInstructionControl, uInstructionSync, uInstructionTry;

type
  TFuncBuilder = class(TBuilder)
  public
    constructor Create(Resolver: IResolver; Machine: TMachine; Tokens: TTokenStream);
    destructor Destroy; override;
  private
    FValues: TDict<IValue>;
    FParamNames: TStringSet;
    FResultNames: TStringSet;
    FResolver: IResolver;
    FContext: TContext;
    FFunc: TCustomFunc;
    FClosureNames: TStringSet;
    FClosures: TArray<Integer>;
    function BuildInstruction(Tokens: TTokenStream; AllowedCode: Byte): TInstruction;
    function BuildBlock(Tokens: TTokenStream; AllowedCode: Byte): TInstructionBlock;
    function BuildControl(Token: TToken; AllowedCode: Byte): TInstructionControl;
    function BuildEach(Tokens: TTokenStream; AllowedCode: Byte): TInstructionEach;
    function BuildIf(Tokens: TTokenStream; AllowedCode: Byte): TInstructionIf;
    function BuildSync(Tokens: TTokenStream; AllowedCode: Byte): TInstructionSync;
    function BuildTry(Tokens: TTokenStream; AllowedCode: Byte): TInstructionTry;
    function BuildWhile(Tokens: TTokenStream; AllowedCode: Byte): TInstructionWhile;
    function BuildWith(Tokens: TTokenStream; AllowedCode: Byte): TInstructionWith;
    function GetValue(Name: string; UnitName: string): IValue;
    function MakeInterface: TType;
    function Wrap: IData;
    procedure ProcessClosures(Tokens: TTokenStream);
    procedure BuildHeader(Tokens: TTokenStream);
    procedure UnregisterValue(Name: string);
  protected
    function MakeResolver: IResolver; override;
    procedure SaveValue(Name: string; Value: IValue; Section: TValueSection); override;
  public
    property Func: IData read Wrap;
    property FuncType: TType read MakeInterface;
    property ClosureNames: TStringSet read FClosureNames;
  end;

implementation

uses uExceptions, uDataFunc, uDataString, uExpressionBuilder, uExpression, uExpressionVariable, uTypeBuilder,
  uTypeFactory, uTypeString, uOp, uOpJmp, uOpReturn, uIdentifierRegistry, uInstructionAssign, uInstructionBind,
  uInstructionExecute, uInstructionThrow, uInstructionLog;

{ TFuncBuilder }

constructor TFuncBuilder.Create(Resolver: IResolver; Machine: TMachine; Tokens: TTokenStream);
var
  p, r: TArray<Integer>;
  i: Integer;
  a: TArray<TOp>;
begin
  inherited Create(Machine);
  FResolver := Resolver;

  FRegistry.Add(RI_SELF);

  FValues := TDict<IValue>.Create;
  FParamNames := TStringSet.Create;
  FResultNames := TStringSet.Create;
  FClosureNames := TStringSet.Create;
  SetLength(FClosures, 0);

  FContext := TContext.Create;

  BuildHeader(Tokens);

  SetLength(p, FParamNames.Count);
  for i := 0 to High(p) do
    p[i] := FContext.Keep(FValues[FParamNames[i]].Data, FParamNames[i], False);
  SetLength(r, FResultNames.Count);
  for i := 0 to High(r) do
    r[i] := FContext.Keep(FValues[FResultNames[i]].Data, FResultNames[i], False);

  FFunc := TCustomFunc.Create(FContext, FClosures, p, r);

  SetLength(a, 0);
  AppendOp(a, BuildInstruction(Tokens, CC_EXIT).Compile);
  AppendOp(a, [TOpReturn.Create(r)]);
  for i := 0 to High(a) - 1 do
    if (a[i] is TOpJmp) and ((a[i] as TOpJmp).Delta = 0) then
      (a[i] as TOpJmp).Delta := High(a) - i;

  FFunc.SetAddress(FMachine.Size);
  FMachine.AddAll(a);
end;

destructor TFuncBuilder.Destroy;
begin
  FClosureNames.Free;
  FValues.Free;
  FParamNames.Free;
  FResultNames.Free;
  inherited;
end;

function TFuncBuilder.GetValue(Name, UnitName: string): IValue;
var
  V: IValue;
begin
  if FValues.Contains(Name) then
    Result := FValues[Name]
  else
  begin
    if Name = RI_SELF then
    begin
      V := TValue.Create(MakeInterface, TDataFunc.Create(FFunc), False);
      FValues.Put(RI_SELF, V);
      Result := V;
    end
    else
      Result := nil;
  end;
end;

function TFuncBuilder.BuildInstruction(Tokens: TTokenStream; AllowedCode: Byte): TInstruction;
var
  T: TToken;
  E: TExpression;
begin
  if Tokens.Token.Kind = tkKeyword then
  begin
    T := Tokens.Pop;
    case ToKword(T.Value) of
      kwDo:
        Result := BuildBlock(Tokens, AllowedCode);
      kwEach:
        Result := BuildEach(Tokens, AllowedCode or CC_BREAK or CC_CONTINUE);
      kwIf:
        Result := BuildIf(Tokens, AllowedCode);
      kwWhile:
        Result := BuildWhile(Tokens, AllowedCode or CC_BREAK or CC_CONTINUE);
      kwWith:
        Result := BuildWith(Tokens, AllowedCode);
      kwSync:
        Result := BuildSync(Tokens, AllowedCode);
      kwTry:
        Result := BuildTry(Tokens, AllowedCode);
      kwThrow:
        Result := TInstructionThrow.Create(BuildExpression(Tokens, FResolver, FMachine, FContext));
      kwRun:
        Result := TInstructionExecute.Create(BuildExpression(Tokens, FResolver, FMachine, FContext), True);
      kwBind:
        Result := TInstructionBind.Create(BuildExpression(Tokens, FResolver, FMachine, FContext));
      kwLog:
        Result := TInstructionLog.Create(BuildExpression(Tokens, FResolver, FMachine, FContext));
      kwBreak, kwContinue, kwExit:
        Result := BuildControl(T, AllowedCode);
    else
      raise ECodeException.Create(E_UNEXPECTED_KEYWORD, T.Value, T.Index);
    end
  end
  else
    try
      T := Tokens.Token;
      E := BuildExpression(Tokens, FResolver, FMachine, FContext);
      if Eq(Tokens.Token(), sSet) then
      begin
        Tokens.Skip;
        Result := TInstructionAssign.Create(E, BuildExpression(Tokens, FResolver, FMachine, FContext));
      end
      else
        Result := TInstructionExecute.Create(E, False);
    except
      on E: ECodeException do
      begin
        E.SetPos(T.Index);
        raise;
      end;
    end;
end;

function TFuncBuilder.BuildSync(Tokens: TTokenStream; AllowedCode: Byte): TInstructionSync;
const
  ALPHA = '0123456789ABCDEF';
var
  i: Integer;
  E: TExpression;
  Name: string;
begin
  if Eq(Tokens.Token, sBrOp) then
  begin
    Tokens.Test(sBrOp, E_SYNTAX_ERROR);
    i := Tokens.Token.Index;
    E := BuildExpression(Tokens, FResolver, FMachine, FContext);
    if not(E.ResultType is TTypeString) then
      raise ECodeException.Create(E_EXPECTED_STRING, '', i);
    if not E.Immutable then
      raise ECodeException.Create(E_EXPECTED_STATIC, '', i);
    Tokens.Test(sBrCl, E_SYNTAX_ERROR);
    name := AsString(E.Eval.Data);
  end else
    name := TIdentifierRegistry.RandomIdentifier(ALPHA, 16);
  Result := TInstructionSync.Create(name, BuildInstruction(Tokens, AllowedCode));
end;

function TFuncBuilder.BuildTry(Tokens: TTokenStream; AllowedCode: Byte): TInstructionTry;

  function Instructions(Tokens: TTokenStream): TInstructionBlock;
  begin
    Result := TInstructionBlock.Create;
    while not Eq(Tokens.Token, kwEnd) and not Eq(Tokens.Token, kwCatch) do
    begin
      Result.AddInstruction(BuildInstruction(Tokens, AllowedCode));
      Tokens.Test(sSemicolon, E_MISSING_SEMICOLON);
    end;
  end;

var
  E: TExpression;
  T: TType;
  identifier: TToken;
begin
  Result := TInstructionTry.Create(Instructions(Tokens));
  while True do
    if Eq(Tokens.Token, kwCatch) then
    begin
      Tokens.Skip.Test(sBrOp, E_SYNTAX_ERROR);
      identifier := Tokens.Fetch(tkIdentifier);
      Tokens.Test(sColon, E_SYNTAX_ERROR);
      if FRegistry.Add(identifier.Value) then
      begin
        T := BuildType(Tokens, FResolver);
        SaveValue(identifier.Value, TValue.Create(T, T.Default, False), vsLocal);
        E := TExpressionVariable.Create(FContext.Resolve(identifier.Value), T, False);
      end
      else
        raise ECodeException.Create(E_IDENTIFIER_REDECLARED, identifier.Value, identifier.Index);
      Tokens.Test(sBrCl, E_SYNTAX_ERROR);
      if not Result.AddCatchCaluse(E, Instructions(Tokens)) then
        raise ECodeException.Create(E_CATCH_REDECLARED, E.ResultType.Hash, identifier.Index);
      UnregisterValue(identifier.Value);
      Continue;
    end else begin
      Tokens.Skip;
      Break;
    end;
end;

function TFuncBuilder.BuildWhile(Tokens: TTokenStream; AllowedCode: Byte): TInstructionWhile;
var
  cond: TExpression;
begin
  Tokens.Test(sBrOp, E_SYNTAX_ERROR);
  cond := BuildExpression(Tokens, FResolver, FMachine, FContext);
  Tokens.Test(sBrCl, E_SYNTAX_ERROR);
  Result := TInstructionWhile.Create(cond, BuildInstruction(Tokens, AllowedCode));
end;

function TFuncBuilder.BuildWith(Tokens: TTokenStream; AllowedCode: Byte): TInstructionWith;
var
  Value, variable: TExpression;
  identifier: TToken;
  T: TType;
begin
  Tokens.Test(sBrOp, E_SYNTAX_ERROR);
  Value := BuildExpression(Tokens, FResolver, FMachine, FContext);
  Tokens.Test(sArrow, E_SYNTAX_ERROR);
  identifier := Tokens.Fetch(tkIdentifier);
  if FRegistry.Add(identifier.Value) then
  begin
    Tokens.Test(sColon, E_SYNTAX_ERROR);
    T := BuildType(Tokens, FResolver);
    SaveValue(identifier.Value, TValue.Create(T, T.Default, False), vsLocal);
    variable := TExpressionVariable.Create(FContext.Resolve(identifier.Value), T, False);
  end
  else
    raise ECodeException.Create(E_IDENTIFIER_REDECLARED, identifier.Value, identifier.Index);
  Tokens.Test(sBrCl, E_SYNTAX_ERROR);
  Result := TInstructionWith.Create(variable, Value, BuildInstruction(Tokens, AllowedCode));
  UnregisterValue(identifier.Value);
end;

function TFuncBuilder.BuildBlock(Tokens: TTokenStream; AllowedCode: Byte): TInstructionBlock;
begin
  Result := TInstructionBlock.Create;
  repeat
    if Eq(Tokens.Token, kwEnd) then
    begin
      Tokens.Skip;
      Exit;
    end;
    Result.AddInstruction(BuildInstruction(Tokens, AllowedCode));
    Tokens.Test(sSemicolon, E_MISSING_SEMICOLON);
  until False;
end;

function TFuncBuilder.BuildControl(Token: TToken; AllowedCode: Byte): TInstructionControl;

  function KWordToCode(KWord: TKeyword): Byte;
  begin
    case KWord of
      kwBreak:
        Result := CC_BREAK;
      kwContinue:
        Result := CC_CONTINUE;
      kwExit:
        Result := CC_EXIT;
    else
      Result := CC_NEXT;
    end;
  end;

var
  c: Byte;

begin
  try
    c := KWordToCode(ToKword(Token.Value));
    if AllowedCode and c <> c then
      raise ECodeException.Create(E_UNEXPECTED_KEYWORD, Token.Value, Token.Index);
    Result := TInstructionControl.Create(c);
  except
    on E: ECodeException do
    begin
      E.SetPos(Token.Index);
      raise;
    end;
  end;
end;

function TFuncBuilder.BuildEach(Tokens: TTokenStream; AllowedCode: Byte): TInstructionEach;
var
  list, item: TExpression;
  identifier: TToken;
  T: TType;
  f: Boolean;
begin
  Tokens.Test(sBrOp, E_SYNTAX_ERROR);
  list := BuildExpression(Tokens, FResolver, FMachine, FContext);
  Tokens.Test(sArrow, E_SYNTAX_ERROR);
  f := (Tokens.Token().Kind = tkIdentifier) and Eq(Tokens.Token(1), sColon);
  if f then
  begin
    identifier := Tokens.Fetch(tkIdentifier);
    if FRegistry.Add(identifier.Value) then
    begin
      T := BuildType(Tokens.Skip, FResolver);
      SaveValue(identifier.Value, TValue.Create(T, T.Default, False), vsLocal);
      item := TExpressionVariable.Create(FContext.Resolve(identifier.Value), T, False);
    end
    else
      raise ECodeException.Create(E_IDENTIFIER_REDECLARED, identifier.Value, identifier.Index);
  end
  else
    item := BuildExpression(Tokens, FResolver, FMachine, FContext);
  Tokens.Test(sBrCl, E_SYNTAX_ERROR);
  Result := TInstructionEach.Create(list, item, BuildInstruction(Tokens, AllowedCode));
  if f then
    UnregisterValue(identifier.Value);
end;

function TFuncBuilder.BuildIf(Tokens: TTokenStream; AllowedCode: Byte): TInstructionIf;
var
  cond: TExpression;
  ib, eb: TInstruction;
begin
  Tokens.Test(sBrOp, E_SYNTAX_ERROR);
  cond := BuildExpression(Tokens, FResolver, FMachine, FContext);
  Tokens.Test(sBrCl, E_SYNTAX_ERROR);
  ib := BuildInstruction(Tokens, AllowedCode);
  if Eq(Tokens.Token(), kwElse) then
    eb := BuildInstruction(Tokens.Skip, AllowedCode)
  else
    eb := TInstructionBlock.Create;
  Result := TInstructionIf.Create(cond, ib, eb);
end;

procedure TFuncBuilder.BuildHeader(Tokens: TTokenStream);
var
  kw: TKeyword;
begin
  FResolver := FResolver.GoDown(GetValue);
  Tokens.Test(sFunc, E_SYNTAX_ERROR);
  Tokens.Test(sBrOp, E_SYNTAX_ERROR);
  while not Eq(Tokens.Token, sBrCl) do
  begin
    if Tokens.Token.Kind = tkKeyword then
      kw := ToKword(Tokens.Token.Value)
    else
      kw := kwError;
    case kw of
      kwConst:
        ProcessConst(Tokens);
      kwVar, kwIn, kwOut:
        ProcessVariable(Tokens);
      kwUse:
        ProcessClosures(Tokens);
    else
      raise ECodeException.Create(E_SYNTAX_ERROR, Tokens.Token.Value, Tokens.Token.Index);
    end;
  end;
  Tokens.Test(sBrCl, E_SYNTAX_ERROR);
end;

function TFuncBuilder.MakeInterface: TType;
var
  i: Integer;
  a: TArray<TType>;
  p, r: TType;
begin
  SetLength(a, FParamNames.Count);
  for i := 0 to High(a) do
    a[i] := FValues[FParamNames[i]].TypeInfo;
  p := TypeFactory.GetStruct(a, FParamNames.AsArray);
  if FResultNames.Count = 1 then
    r := FValues[FResultNames[0]].TypeInfo
  else
  begin
    if FResultNames.Count = 0 then
      r := TypeFactory.GetVoid
    else
    begin
      SetLength(a, FResultNames.Count);
      for i := 0 to High(a) do
        a[i] := FValues[FResultNames[i]].TypeInfo;
      r := TypeFactory.GetStruct(a, FResultNames.AsArray);
    end;
  end;
  Result := TypeFactory.GetFunc(p, r, FResultNames.Count = 1);
end;

function TFuncBuilder.MakeResolver: IResolver;
begin
  Result := FResolver;
end;

procedure TFuncBuilder.ProcessClosures(Tokens: TTokenStream);
var
  T: TToken;
  V: IValue;
begin
  Tokens.Test(kwUse, E_SYNTAX_ERROR); // Guard, never fails
  repeat
    T := Tokens.Fetch(tkIdentifier);
    try
      if FRegistry.Add(T.Value) then
      begin
        V := FResolver.ResolveClosure(T.Value);
        SaveValue(T.Value, V, vsClosure);
        if not V.Immutable then
        begin
          SetLength(FClosures, Length(FClosures) + 1);
          FClosures[High(FClosures)] := FContext.Keep(V.Data, T.Value, False);
          FClosureNames.Add(T.Value);
        end;
      end
      else
        raise ECodeException.Create(E_IDENTIFIER_REDECLARED, T.Value);
    except
      on E: ECodeException do
      begin
        E.SetPos(T.Index);
        raise;
      end;
    end;

    T := Tokens.Pop;
    if Eq(T, sComma) then
      Continue;
    if Eq(T, sSemicolon) then
      Break
    else
      raise ECodeException.Create(E_MISSING_SEMICOLON, '', T.Index);
  until False;
end;

procedure TFuncBuilder.SaveValue(Name: string; Value: IValue; Section: TValueSection);
begin
  FValues.Put(Name, Value);
  if Section = vsClosure then
    FContext.Keep(Value.Data, Name, True)
  else
    FContext.Keep(Value.Data, Name, False);
  case Section of
    vsParam:
      FParamNames.Add(Name);
    vsResult:
      FResultNames.Add(Name);
  end;
end;

procedure TFuncBuilder.UnregisterValue(Name: string);
begin
  FRegistry.Unregister(Name);
  FContext.UnregisterName(Name);
end;

function TFuncBuilder.Wrap: IData;
begin
  if FValues.Contains(RI_SELF) then
    Result := FValues[RI_SELF].Data
  else
    Result := TDataFunc.Create(FFunc);
end;

end.
