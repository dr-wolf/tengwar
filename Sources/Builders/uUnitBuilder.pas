unit uUnitBuilder;

interface

uses
  uBuilder, uValue, uUnit, uUnitLibrary, uSourceParser, uResolver, uMachine;

type
  TUnitBuilder = class(TBuilder)
  public
    constructor Create(Filename: string; UnitLibrary: TUnitLibrary; Machine: TMachine);
    destructor Destroy; override;
  private
    FTokenizer: TTokenizer;
    FUnitLibrary: TUnitLibrary;
    FUnit: TUnit;
    FLinesCount: Integer;
    procedure ProcessUnit(Tokens: TTokenStream);
    procedure ProcessType(Tokens: TTokenStream);
  protected
    function MakeResolver: IResolver; override;
    procedure SaveValue(Name: string; Value: IValue; Section: TValueSection); override;
  public
    property Application: TUnit read FUnit;
    property LinesCount: Integer read FLinesCount;
  end;

implementation

uses SysUtils, Classes, uExceptions, uParserLogic, uTypeBuilder;

{ TUnitBuilder }

constructor TUnitBuilder.Create(Filename: string; UnitLibrary: TUnitLibrary; Machine: TMachine);
var
  src: TStrings;
  Tokens: TTokenStream;
  t: TToken;
begin
  inherited Create(Machine);
  FTokenizer := TTokenizer.Create;
  FillRuleset(FTokenizer);
  FUnitLibrary := UnitLibrary;
  FUnit := TUnit.Create(Filename);

  try
    Tokens := TTokenStream.Create;

    src := TStringList.Create;
    with src do
    begin
      LoadFromFile(Filename);
      FLinesCount := Count;
      FTokenizer.Process(src, Tokens);
      Free;
    end;

    { -- parsing starts here -- }
    while not Tokens.Empty do
    begin
      t := Tokens.Token();
      if t.Kind = tkKeyword then
        case ToKword(t.Value) of
          kwLoad:
            ProcessUnit(Tokens);
          kwType:
            ProcessType(Tokens);
          kwConst:
            ProcessConst(Tokens);
          kwVar:
            ProcessVariable(Tokens);
        else
          raise ECodeException.Create(E_UNEXPECTED_KEYWORD, t.Value, t.Index);
        end
      else
        raise ECodeException.Create(E_SYNTAX_ERROR, '', t.Index);
    end;
  except
    on e: ECodeException do
    begin
      e.SetFile(Filename);
      raise;
    end;
  end;
end;

destructor TUnitBuilder.Destroy;
begin
  FTokenizer.Free;
  inherited;
end;

function TUnitBuilder.MakeResolver: IResolver;
begin
  Result := TResolver.Create(FUnit.GetType, FUnit.GetValue, nil);
end;

procedure TUnitBuilder.ProcessType(Tokens: TTokenStream);
var
  Index: Integer;
  identifier: TToken;
begin
  Tokens.Test(kwType, E_SYNTAX_ERROR); // Guard, never fails
  identifier := Tokens.Fetch(tkIdentifier);
  Tokens.Test(sColon, E_SYNTAX_ERROR);
  index := Tokens.Token().Index;
  if FRegistry.Add(identifier.Value) then
    try
      FUnit.AddType(identifier.Value, BuildType(Tokens, TResolver.Create(FUnit.GetType, FUnit.GetValue, nil)));
    except
      on e: ECodeException do
      begin
        e.SetPos(index);
        raise;
      end;
    end
  else
    raise ECodeException.Create(E_IDENTIFIER_REDECLARED, identifier.Value, identifier.Index);
  Tokens.Test(sSemicolon, E_MISSING_SEMICOLON);
end;

procedure TUnitBuilder.ProcessUnit(Tokens: TTokenStream);
var
  script: string;
  identifier: TToken;
begin
  Tokens.Test(kwLoad, E_SYNTAX_ERROR); // Guard, never fails
  script := Tokens.Fetch(tkString).Value;
  Tokens.Test(kwAs, E_SYNTAX_ERROR);
  identifier := Tokens.Fetch(tkIdentifier);
  Tokens.Test(sSemicolon, E_MISSING_SEMICOLON);
  if FRegistry.Add(identifier.Value) then
  begin
    try
      FUnit.AddUnit(identifier.Value, FUnitLibrary.Load(script + '.tw', FMachine, ExtractFilePath(FUnit.Filename)));
    except
      on e: ECodeException do
      begin
        e.SetPos(identifier.Index);
        raise;
      end;
    end;
  end
  else
    raise ECodeException.Create(E_IDENTIFIER_REDECLARED, identifier.Value, identifier.Index);
end;

procedure TUnitBuilder.SaveValue(Name: string; Value: IValue; Section: TValueSection);
begin
  FUnit.AddValue(Name, Value);
end;

end.
