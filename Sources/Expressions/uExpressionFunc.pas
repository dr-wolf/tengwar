unit uExpressionFunc;

interface

uses
  uArray, uType, uData, uExpression, uValue, uOp;

type
  TExpressionFunc = class(TExpression)
  public
    constructor Create(Func: IData; FuncType: TType; Closures: TArray<Integer>);
    destructor Destroy; override;
  private
    FFunc: IData;
    FClosures: TArray<Integer>;
  public
    function Compile: TArray<TOp>; override;
    function Eval: IValue; override;
  end;

implementation

uses uOpFunc, uOpVal;

{ TExpressionFunc }

constructor TExpressionFunc.Create(Func: IData; FuncType: TType; Closures: TArray<Integer>);
var
  i: Integer;
begin
  FImmutable := Length(Closures) = 0;
  FResultType := FuncType;
  FFunc := Func;
  SetLength(FClosures, Length(Closures));
  for i := 0 to High(FClosures) do
    FClosures[i] := Closures[i];
end;

destructor TExpressionFunc.Destroy;
begin
  inherited;
end;

function TExpressionFunc.Compile: TArray<TOp>;
begin
  SetLength(Result, 2);
  Result[0] := TOpVal.Create(FFunc);
  Result[1] := TOpFunc.Create(FClosures);
end;

function TExpressionFunc.Eval: IValue;
begin
  Result := TValue.Create(FResultType, FFunc);
end;

end.
