unit uExpressionTest;

interface

uses
  uArray, uExpression, uData, uType, uValue, uOp;

type
  TExpressionTest = class(TExpression)
  public
    constructor Create(Expression: TExpression; TypeInfo: TType);
    destructor Destroy; override;
  private
    FDefault: IData;
    FExpression: TExpression;
  public
    function Compile: TArray<TOp>; override;
    function Eval: IValue; override;
  end;

implementation

uses uExceptions, uTypeFactory, uTypeVoid, uDataBool, uOpCast;

{ TExpressionTest }

constructor TExpressionTest.Create(Expression: TExpression; TypeInfo: TType);
begin
  if Expression.ResultType is TTypeVoid then
    raise ECodeException.Create(E_NO_RESULT);
  FExpression := Expression;
  FImmutable := Expression.Immutable;
  FResultType := TypeFactory.GetBool;
  FDefault := TypeInfo.Default;
end;

destructor TExpressionTest.Destroy;
begin
  FExpression.Free;
  inherited;
end;

function TExpressionTest.Compile: TArray<TOp>;
begin
  SetLength(Result, 0);
  AppendOp(Result, FExpression.Compile);
  AppendOp(Result, [TOpCast.Create(FDefault, True)]);
end;

function TExpressionTest.Eval: IValue;
begin
  try
    FDefault.Assign(FExpression.Eval.Data);
    Result := TValue.Create(FResultType, TDataBool.Create(True));
  except
    Result := TValue.Create(FResultType, TDataBool.Create(False));
  end;
end;

end.
