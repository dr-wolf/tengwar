unit uExpressionUnary;

interface

uses
  uArray, uOperations, uExpression, uValue, uOp;

type
  TExpressionUnary = class(TExpression)
  public
    constructor Create(Expression: TExpression; Op: TUnaryOp);
    destructor Destroy; override;
  private
    FOperation: TUnaryOp;
    FExpression: TExpression;
  public
    function Compile: TArray<TOp>; override;
    function Eval: IValue; override;
  end;

implementation

uses uOpUnary;

{ TExpressionUnary }

constructor TExpressionUnary.Create(Expression: TExpression; Op: TUnaryOp);
begin
  FExpression := Expression;
  FOperation := Op;
  FImmutable := FExpression.Immutable;
  FResultType := FExpression.ResultType.Apply(Op);
end;

destructor TExpressionUnary.Destroy;
begin
  FExpression.Free;
  inherited;
end;

function TExpressionUnary.Compile: TArray<TOp>;
begin
  SetLength(Result, 0);
  AppendOp(Result, FExpression.Compile);
  AppendOp(Result, [TOpUnary.Create(FOperation)]);
end;

function TExpressionUnary.Eval: IValue;
begin
  Result := FExpression.Eval.Apply(FOperation);
end;

end.
