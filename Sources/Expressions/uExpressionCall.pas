unit uExpressionCall;

interface

uses
  uArray, uExpression, uOp;

type
  TExpressionCall = class(TExpression)
  public
    constructor Create(Func: TExpression; Params: TExpression);
    destructor Destroy; override;
  private
    FFunc: TExpression;
    FParams: TExpression;
    FParamMap: TArray<Integer>;
  public
    function Compile: TArray<TOp>; override;
  end;

implementation

uses uStringSet, uTypeStruct, uTypeFunc, uExpressionStruct, uOpCall;

{ TExpressionCall }

constructor TExpressionCall.Create(Func: TExpression; Params: TExpression);
var
  i: Integer;
  p: TStringSet;
  a: TArray<string>;
begin
  FFunc := Func;
  if Params.ResultType is TTypeStruct then
    FParams := Params
  else
    FParams := TExpressionStruct.Create(Params);
  FImmutable := False;
  FResultType := FFunc.ResultType.Call(FParams.ResultType);
  if (Params.ResultType is TTypeStruct) and not(Params.ResultType as TTypeStruct).Nameless then
  begin
    p := TStringSet.Create(((Func.ResultType as TTypeFunc).Params as TTypeStruct).Fields);
    a := (Params.ResultType as TTypeStruct).Fields;
    SetLength(FParamMap, Length(a));
    for i := 0 to High(FParamMap) do
      FParamMap[i] := p.IndexOf(a[i]);
    p.Free;
  end
  else
    SetLength(FParamMap, 0);
end;

destructor TExpressionCall.Destroy;
begin
  FFunc.Free;
  FParams.Free;
  inherited;
end;

function TExpressionCall.Compile: TArray<TOp>;
begin
  SetLength(Result, 0);
  AppendOp(Result, FParams.Compile);
  AppendOp(Result, FFunc.Compile);
  AppendOp(Result, [TOpCall.Create(FParamMap)]);
end;

end.
