unit uExpressionVariable;

interface

uses
  uExpression, uArray, uType, uOp;

type
  TExpressionVariable = class(TExpression)
  public
    constructor Create(VariableId: Integer; TypeInfo: TType; Immutable: Boolean);
  private
    FVariableId: Integer;
  public
    function Compile: TArray<TOp>; override;
  end;

implementation

uses uOpVar;

{ TExpressionVariable }

constructor TExpressionVariable.Create(VariableId: Integer; TypeInfo: TType; Immutable: Boolean);
begin
  FVariableId := VariableId;
  FResultType := TypeInfo;
  FImmutable := Immutable;
end;

function TExpressionVariable.Compile: TArray<TOp>;
begin
  SetLength(Result, 1);
  Result[0] := TOpVar.Create(FVariableId);
end;

end.
