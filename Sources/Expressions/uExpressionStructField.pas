unit uExpressionStructField;

interface

uses
  uArray, uExpression, uValue, uOp;

type
  TExpressionStructField = class(TExpression)
  public
    constructor Create(Struct: TExpression; FieldName: string);
    destructor Destroy; override;
  private
    FStruct: TExpression;
    FFieldName: string;
  public
    function Compile: TArray<TOp>; override;
    function Eval: IValue; override;
  end;

implementation

uses uExceptions, uType, uTypeVoid, uTypeStruct, uOpField;

{ TExpressionStructField }

constructor TExpressionStructField.Create(Struct: TExpression; FieldName: string);
begin
  if Struct.ResultType is TTypeVoid then
    raise ECodeException.Create(E_NO_RESULT);
  FResultType := Struct.ResultType.NestedType(FieldName);
  FStruct := Struct;
  FFieldName := FieldName;
  FImmutable := FStruct.Immutable;
end;

destructor TExpressionStructField.Destroy;
begin
  FStruct.Free;
  inherited;
end;

function TExpressionStructField.Compile: TArray<TOp>;
begin
  SetLength(Result, 0);
  AppendOp(Result, FStruct.Compile);
  AppendOp(Result, [TOpField.Create((FStruct.ResultType as TTypeStruct).IndexOf(FFieldName))]);
end;

function TExpressionStructField.Eval: IValue;
begin
  Result := FStruct.Eval.Sub(FFieldName);
end;

end.
