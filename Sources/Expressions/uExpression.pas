unit uExpression;

interface

uses
  uArray, uType, uValue, uOp;

type
  TExpression = class(TObject)
  protected
    FImmutable: Boolean;
    FResultType: TType;
  public
    property Immutable: Boolean read FImmutable;
    property ResultType: TType read FResultType;
    function Compile: TArray<TOp>; virtual; abstract;
    function Eval: IValue; virtual;
  end;

implementation

uses uExceptions;

{ TExpression }

function TExpression.Eval: IValue;
begin
  raise ECodeException.Create(E_EXPECTED_STATIC);
end;

end.
