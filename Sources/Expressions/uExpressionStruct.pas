unit uExpressionStruct;

interface

uses
  uExpression, uArray, uValue, uOp;

type
  TExpressionStruct = class(TExpression)
  public
    constructor Create(Fields: array of TExpression; Names: array of string); overload;
    constructor Create(Field: TExpression); overload;
    destructor Destroy; override;
  private
    FFields: TArray<TExpression>;
    FNameless: Boolean;
  public
    property Nameless: Boolean read FNameless;
    function Compile: TArray<TOp>; override;
    function Eval: IValue; override;
    function Fields: TArray<string>;
  end;

implementation

uses uExceptions, uTypeFactory, uType, uTypeVoid, uTypeStruct, uData, uDataArray, uOpStack;

{ TExpressionStruct }

constructor TExpressionStruct.Create(Fields: array of TExpression; Names: array of string);
var
  Types: TArray<TType>;
  i: Integer;
begin
  SetLength(FFields, Length(Fields));
  for i := 0 to High(Fields) do
    FFields[i] := Fields[i];
  SetLength(Types, Length(Fields));
  FImmutable := True;
  FNameless := Length(Names) = 0;
  for i := 0 to High(Types) do
  begin
    if Fields[i].ResultType is TTypeVoid then
      raise ECodeException.Create(E_NO_RESULT);
    Types[i] := Fields[i].ResultType;
    FImmutable := FImmutable and Fields[i].Immutable;
  end;
  FResultType := TypeFactory.GetStruct(Types, Names);
end;

constructor TExpressionStruct.Create(Field: TExpression);
var
  Types: TArray<TType>;
begin
  SetLength(FFields, 1);
  FFields[0] := Field;
  FImmutable := Field.Immutable;
  FNameless := True;
  SetLength(Types, 1);
  Types[0] := Field.ResultType;
  FResultType := TypeFactory.GetStruct(Types, []);
end;

destructor TExpressionStruct.Destroy;
var
  i: Integer;
begin
  for i := 0 to High(FFields) do
    FFields[i].Free;
  inherited;
end;

function TExpressionStruct.Fields: TArray<string>;
begin
  Result := (FResultType as TTypeStruct).Fields;
end;

function TExpressionStruct.Compile: TArray<TOp>;
var
  i: Integer;
begin
  SetLength(Result, 0);
  for i := High(FFields) downto 0 do
    AppendOp(Result, FFields[i].Compile);
  AppendOp(Result, [TOpStack.Create(False, Length(FFields))]);
end;

function TExpressionStruct.Eval: IValue;
var
  i: Integer;
  d: TArray<IData>;
begin
  SetLength(d, Length(FFields));
  for i := 0 to High(FFields) do
    d[i] := FFields[i].Eval.Data.Clone;
  Result := TValue.Create(FResultType, TDataArray.Create(d));
end;

end.
