unit uExpressionValue;

interface

uses
  uArray, uExpression, uValue, uOp;

type
  TExpressionValue = class(TExpression)
  public
    constructor Create(Value: IValue);
  private
    FValue: IValue;
  public
    function Eval: IValue; override;
    function Compile: TArray<TOp>; override;
  end;

implementation

uses uOpVal;

{ TExpressionValue }

constructor TExpressionValue.Create(Value: IValue);
begin
  FValue := Value;
  FResultType := FValue.TypeInfo;
  FImmutable := FValue.Immutable;
end;

function TExpressionValue.Compile: TArray<TOp>;
begin
  SetLength(Result, 1);
  Result[0] := TOpVal.Create(FValue.Data);
end;

function TExpressionValue.Eval: IValue;
begin
  Result := FValue;
end;

end.
