unit uExpressionListItem;

interface

uses
  uArray, uExpression, uValue, uOp;

type
  TExpressionListItem = class(TExpression)
  public
    constructor Create(List, ItemIndex: TExpression);
    destructor Destroy; override;
  private
    FList: TExpression;
    FItemIndex: TExpression;
  public
    function Compile: TArray<TOp>; override;
    function Eval: IValue; override;
  end;

implementation

uses uExceptions, uType, uTypeInt, uTypeList, uDataArray, uDataNumeric, uOpItem;

{ TExpressionValue }

constructor TExpressionListItem.Create(List, ItemIndex: TExpression);
begin
  if not(ItemIndex.ResultType is TTypeInt) then
    raise ECodeException.Create(E_EXPECTED_INTEGER);
  if not(List.ResultType is TTypeList) then
    raise ECodeException.Create(E_EXPECTED_LIST);
  FList := List;
  FItemIndex := ItemIndex;
  FImmutable := FList.Immutable and FItemIndex.Immutable;
  FResultType := FList.ResultType.NestedType();
end;

destructor TExpressionListItem.Destroy;
begin
  FList.Free;
  FItemIndex.Free;
  inherited;
end;

function TExpressionListItem.Eval: IValue;
var
  Value: IValue;
begin
  Value := FList.Eval;
  Result := Value.Sub(TranslateIndex(AsInt(FItemIndex.Eval.Data), Value.Data.Count));
end;

function TExpressionListItem.Compile: TArray<TOp>;
begin
  SetLength(Result, 0);
  AppendOp(Result, FList.Compile);
  AppendOp(Result, FItemIndex.Compile);
  AppendOp(Result, [TOpItem.Create]);
end;

end.
