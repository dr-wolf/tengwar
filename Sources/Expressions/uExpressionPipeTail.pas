unit uExpressionPipeTail;

interface

uses
  uArray, uExpression, uOp;

type
  TExpressionPipeTail = class(TExpression)
  public
    constructor Create(Pipe: TExpression);
    destructor Destroy; override;
  private
    FPipe: TExpression;
  public
    function Compile: TArray<TOp>; override;
  end;

implementation

uses uExceptions, uType, uTypePipe, uOpTail;

{ TExpressionPipeTail }

constructor TExpressionPipeTail.Create(Pipe: TExpression);
begin
  if not(Pipe.ResultType is TTypePipe) then
    raise ECodeException.Create(E_EXPECTED_LIST);
  FPipe := Pipe;
  FImmutable := False;
  FResultType := FPipe.ResultType.NestedType();
end;

destructor TExpressionPipeTail.Destroy;
begin
  FPipe.Free;
  inherited;
end;

function TExpressionPipeTail.Compile: TArray<TOp>;
begin
  SetLength(Result, 0);
  AppendOp(Result, FPipe.Compile);
  AppendOp(Result, [TOpTail.Create]);
end;

end.
