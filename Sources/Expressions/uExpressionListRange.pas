unit uExpressionListRange;

interface

uses
  uExpression, uArray, uValue, uOp;

type
  TExpressionListRange = class(TExpression)
  public
    constructor Create(List, ListIndex: TExpression);
    destructor Destroy; override;
  private
    FList: TExpression;
    FListIndex: TExpression;
  public
    function Compile: TArray<TOp>; override;
    function Eval: IValue; override;
  end;

implementation

uses uExceptions, uTypeInt, uTypeList, uData, uDataArray, uDataNumeric, uOpRange;

{ TExpressionListRange }

constructor TExpressionListRange.Create(List, ListIndex: TExpression);
begin
  if not(ListIndex.ResultType is TTypeList) or not(ListIndex.ResultType.NestedType() is TTypeInt) then
    raise ECodeException.Create(E_EXPECTED_ITERABLE);
  if not(List.ResultType is TTypeList) then
    raise ECodeException.Create(E_EXPECTED_LIST);
  FList := List;
  FListIndex := ListIndex;
  FImmutable := FList.Immutable and FListIndex.Immutable;
  FResultType := FList.ResultType;
end;

destructor TExpressionListRange.Destroy;
begin
  FList.Free;
  FListIndex.Free;
  inherited;
end;

function TExpressionListRange.Compile: TArray<TOp>;
begin
  SetLength(Result, 0);
  AppendOp(Result, FList.Compile);
  AppendOp(Result, FListIndex.Compile);
  AppendOp(Result, [TOpRange.Create]);
end;

function TExpressionListRange.Eval: IValue;
var
  Source, Index: IData;
  Items: TArray<IData>;
  i: Integer;
begin
  Source := FList.Eval.Data;
  Index := FListIndex.Eval.Data;
  SetLength(Items, Index.Count);
  for i := 0 to High(Items) do
    Items[i] := Source.Item(TranslateIndex(AsInt(Index.Item(i)), Source.Count));
  Result := TValue.Create(FResultType, TDataArray.Create(Items));
end;

end.
