unit uExpressionCast;

interface

uses
  uArray, uExpression, uType, uValue, uOp;

type
  TExpressionCast = class(TExpression)
  public
    constructor Create(Expression: TExpression; TypeInfo: TType);
    destructor Destroy; override;
  private
    FExpression: TExpression;
  public
    function Compile: TArray<TOp>; override;
    function Eval: IValue; override;
  end;

implementation

uses uExceptions, uTypeFactory, uTypeVoid, uDataBool, uOpCast;

{ TExpressionCast }

constructor TExpressionCast.Create(Expression: TExpression; TypeInfo: TType);
begin
  if Expression.ResultType is TTypeVoid then
    raise ECodeException.Create(E_NO_RESULT);
  FExpression := Expression;
  FImmutable := Expression.Immutable;
  FResultType := TypeInfo;
end;

destructor TExpressionCast.Destroy;
begin
  FExpression.Free;
  inherited;
end;

function TExpressionCast.Compile: TArray<TOp>;
begin
  SetLength(Result, 0);
  AppendOp(Result, FExpression.Compile);
  AppendOp(Result, [TOpCast.Create(FResultType.Default, False)]);
end;

function TExpressionCast.Eval: IValue;
var
  r: IValue;
begin
  r := FExpression.Eval;
  Result := TValue.Create(FResultType);
  Result.Data.Assign(r.Data);
end;

end.
