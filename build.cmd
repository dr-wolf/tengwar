rmdir %TEMP%\tengwar /S /Q
rmdir target /S /Q
xcopy Docs\html\en_US target\docs /s /e /i
xcopy Examples\demo target\demos /s /e /i
xcopy  Examples\libs target\units /s /e /i
mkdir %TEMP%\tengwar
fpc -Mdelphi -Twin32 -Ur -FEtarget -FU%TEMP%\tengwar tengwac.dpr
fpc -Mdelphi -Twin32 -Ur -FEtarget -FU%TEMP%\tengwar tengwar.dpr
pause