#!/bin/sh
rm -rf target
rm -rf /tmp/tengwar
mkdir target
cp -r Docs/html/en_US target/docs
cp -r Examples/demo target/demos
cp -r Examples/libs target/units
mkdir /tmp/tengwar
fpc -Mdelphi -Ur -FEtarget -FU/tmp/tengwar tengwar.dpr
fpc -Mdelphi -Ur -FEtarget -FU/tmp/tengwar tengwac.dpr