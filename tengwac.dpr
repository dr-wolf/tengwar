program tengwac;

{$IFNDEF FPC}
{$APPTYPE CONSOLE}
{$ENDIF}
{$R *.res}

uses
  SysUtils,
  Classes,
  blcksock in 'Libs\Synapse\blcksock.pas',
  synacode in 'Libs\Synapse\synacode.pas',
  synafpc in 'Libs\Synapse\synafpc.pas',
  synaip in 'Libs\Synapse\synaip.pas',
  synautil in 'Libs\Synapse\synautil.pas',
  synsock in 'Libs\Synapse\synsock.pas',
  RegExpr in 'Libs\RegExpr.pas',
  MD5 in 'Libs\MD5.pas',
  uArray in 'Sources\Common\uArray.pas',
  uBuilder in 'Sources\Builders\uBuilder.pas',
  uConfig in 'Sources\uConfig.pas',
  uContext in 'Sources\Functions\uContext.pas',
  uCustomFunc in 'Sources\Functions\uCustomFunc.pas',
  uData in 'Sources\Value\Datas\uData.pas',
  uDataAny in 'Sources\Value\Datas\uDataAny.pas',
  uDataArray in 'Sources\Value\Datas\uDataArray.pas',
  uDataBool in 'Sources\Value\Datas\uDataBool.pas',
  uDataPipe in 'Sources\Value\Datas\uDataPipe.pas',
  uDataLoader in 'Sources\Machine\Cache\uDataLoader.pas',
  uDataNumeric in 'Sources\Value\Datas\uDataNumeric.pas',
  uDataSet in 'Sources\Value\Datas\uDataSet.pas',
  uDataString in 'Sources\Value\Datas\uDataString.pas',
  uDict in 'Sources\Common\uDict.pas',
  uEnvironment in 'Sources\uEnvironment.pas',
  uExceptions in 'Sources\uExceptions.pas',
  uExpression in 'Sources\Expressions\uExpression.pas',
  uExpressionBinary in 'Sources\Expressions\uExpressionBinary.pas',
  uExpressionBuilder in 'Sources\Builders\uExpressionBuilder.pas',
  uExpressionCall in 'Sources\Expressions\uExpressionCall.pas',
  uExpressionTest in 'Sources\Expressions\uExpressionTest.pas',
  uExpressionFunc in 'Sources\Expressions\uExpressionFunc.pas',
  uExpressionList in 'Sources\Expressions\uExpressionList.pas',
  uExpressionPipeTail in 'Sources\Expressions\uExpressionPipeTail.pas',
  uExpressionListRange in 'Sources\Expressions\uExpressionListRange.pas',
  uExpressionSet in 'Sources\Expressions\uExpressionSet.pas',
  uExpressionStruct in 'Sources\Expressions\uExpressionStruct.pas',
  uExpressionStructField in 'Sources\Expressions\uExpressionStructField.pas',
  uExpressionUnary in 'Sources\Expressions\uExpressionUnary.pas',
  uExpressionValue in 'Sources\Expressions\uExpressionValue.pas',
  uExpressionVariable in 'Sources\Expressions\uExpressionVariable.pas',
  uFuncBitAnd in 'Sources\Functions\SDL\Bit\uFuncBitAnd.pas',
  uFuncBitMod in 'Sources\Functions\SDL\Bit\uFuncBitMod.pas',
  uFuncBitNot in 'Sources\Functions\SDL\Bit\uFuncBitNot.pas',
  uFuncBitOr in 'Sources\Functions\SDL\Bit\uFuncBitOr.pas',
  uFuncBitShl in 'Sources\Functions\SDL\Bit\uFuncBitShl.pas',
  uFuncBitShr in 'Sources\Functions\SDL\Bit\uFuncBitShr.pas',
  uFuncBitXor in 'Sources\Functions\SDL\Bit\uFuncBitXor.pas',
  uFuncBuilder in 'Sources\Builders\uFuncBuilder.pas',
  uFuncDateDecode in 'Sources\Functions\SDL\Date\uFuncDateDecode.pas',
  uFuncDateEncode in 'Sources\Functions\SDL\Date\uFuncDateEncode.pas',
  uFuncDateFmt in 'Sources\Functions\SDL\Date\uFuncDateFmt.pas',
  uFuncDateParse in 'Sources\Functions\SDL\Date\uFuncDateParse.pas',
  uFuncFile in 'Sources\Functions\SDL\File\uFuncFile.pas',
  uFuncFileCopy in 'Sources\Functions\SDL\File\uFuncFileCopy.pas',
  uFuncFileGetPos in 'Sources\Functions\SDL\File\uFuncFileGetPos.pas',
  uFuncFileReadFloat in 'Sources\Functions\SDL\File\uFuncFileReadFloat.pas',
  uFuncFileReadInt in 'Sources\Functions\SDL\File\uFuncFileReadInt.pas',
  uFuncFileReadString in 'Sources\Functions\SDL\File\uFuncFileReadString.pas',
  uFuncFileSetPos in 'Sources\Functions\SDL\File\uFuncFileSetPos.pas',
  uFuncFileSize in 'Sources\Functions\SDL\File\uFuncFileSize.pas',
  uFuncFileTrunc in 'Sources\Functions\SDL\File\uFuncFileTrunc.pas',
  uFuncFileWriteFloat in 'Sources\Functions\SDL\File\uFuncFileWriteFloat.pas',
  uFuncFileWriteInt in 'Sources\Functions\SDL\File\uFuncFileWriteInt.pas',
  uFuncFileWriteString in 'Sources\Functions\SDL\File\uFuncFileWriteString.pas',
  uFuncFSAbsPath in 'Sources\Functions\SDL\FS\uFuncFSAbsPath.pas',
  uFuncFSChDir in 'Sources\Functions\SDL\FS\uFuncFSChDir.pas',
  uFuncFSClose in 'Sources\Functions\SDL\FS\uFuncFSClose.pas',
  uFuncFSDelete in 'Sources\Functions\SDL\FS\uFuncFSDelete.pas',
  uFuncFSMkDir in 'Sources\Functions\SDL\FS\uFuncFSMkDir.pas',
  uFuncFSOpen in 'Sources\Functions\SDL\FS\uFuncFSOpen.pas',
  uFuncFSPwd in 'Sources\Functions\SDL\FS\uFuncFSPwd.pas',
  uFuncFSRmDir in 'Sources\Functions\SDL\FS\uFuncFSRmDir.pas',
  uFuncFSScan in 'Sources\Functions\SDL\FS\uFuncFSScan.pas',
  uFuncFSStat in 'Sources\Functions\SDL\FS\uFuncFSStat.pas',
  uFuncIOGet in 'Sources\Functions\SDL\IO\uFuncIOGet.pas',
  uFuncIOPause in 'Sources\Functions\SDL\IO\uFuncIOPause.pas',
  uFuncIOPut in 'Sources\Functions\SDL\IO\uFuncIOPut.pas',
  uFuncMathAbs in 'Sources\Functions\SDL\Math\uFuncMathAbs.pas',
  uFuncMathCos in 'Sources\Functions\SDL\Math\uFuncMathCos.pas',
  uFuncMathFloor in 'Sources\Functions\SDL\Math\uFuncMathFloor.pas',
  uFuncMathFrac in 'Sources\Functions\SDL\Math\uFuncMathFrac.pas',
  uFuncMathLog in 'Sources\Functions\SDL\Math\uFuncMathLog.pas',
  uFuncMathRand in 'Sources\Functions\SDL\Math\uFuncMathRand.pas',
  uFuncMathSin in 'Sources\Functions\SDL\Math\uFuncMathSin.pas',
  uFuncOSExec in 'Sources\Functions\SDL\OS\uFuncOSExec.pas',
  uFuncOSGetEnv in 'Sources\Functions\SDL\OS\uFuncOSGetEnv.pas',
  uFuncOSLsof in 'Sources\Functions\SDL\OS\uFuncOSLsof.pas',
  uFuncOSLsos in 'Sources\Functions\SDL\OS\uFuncOSLsos.pas',
  uFuncOSSetEnv in 'Sources\Functions\SDL\OS\uFuncOSSetEnv.pas',
  uFuncOSSleep in 'Sources\Functions\SDL\OS\uFuncOSSleep.pas',
  uFuncOSTime in 'Sources\Functions\SDL\OS\uFuncOSTime.pas',
  uFuncRex in 'Sources\Functions\SDL\Rex\uFuncRex.pas',
  uFuncRexFind in 'Sources\Functions\SDL\Rex\uFuncRexFind.pas',
  uFuncRexMatch in 'Sources\Functions\SDL\Rex\uFuncRexMatch.pas',
  uFuncRexReplace in 'Sources\Functions\SDL\Rex\uFuncRexReplace.pas',
  uFuncRexSplit in 'Sources\Functions\SDL\Rex\uFuncRexSplit.pas',
  uFuncStrAscii in 'Sources\Functions\SDL\Str\uFuncStrAscii.pas',
  uFuncStrFmt in 'Sources\Functions\SDL\Str\uFuncStrFmt.pas',
  uFuncStrHex in 'Sources\Functions\SDL\Str\uFuncStrHex.pas',
  uFuncStrLen in 'Sources\Functions\SDL\Str\uFuncStrLen.pas',
  uFuncStrLower in 'Sources\Functions\SDL\Str\uFuncStrLower.pas',
  uFuncStrParse in 'Sources\Functions\SDL\Str\uFuncStrParse.pas',
  uFuncStrPos in 'Sources\Functions\SDL\Str\uFuncStrPos.pas',
  uFuncStrSub in 'Sources\Functions\SDL\Str\uFuncStrSub.pas',
  uFuncStrText in 'Sources\Functions\SDL\Str\uFuncStrText.pas',
  uFuncStrUpper in 'Sources\Functions\SDL\Str\uFuncStrUpper.pas',
  uFuncTCP in 'Sources\Functions\SDL\TCP\uFuncTCP.pas',
  uFuncTCPAccept in 'Sources\Functions\SDL\TCP\uFuncTCPAccept.pas',
  uFuncTCPClose in 'Sources\Functions\SDL\TCP\uFuncTCPClose.pas',
  uFuncTCPConnect in 'Sources\Functions\SDL\TCP\uFuncTCPConnect.pas',
  uFuncTCPListen in 'Sources\Functions\SDL\TCP\uFuncTCPListen.pas',
  uFuncTCPReceive in 'Sources\Functions\SDL\TCP\uFuncTCPReceive.pas',
  uFuncTCPRemote in 'Sources\Functions\SDL\TCP\uFuncTCPRemote.pas',
  uFuncTCPSend in 'Sources\Functions\SDL\TCP\uFuncTCPSend.pas',
  uFunction in 'Sources\Functions\uFunction.pas',
  uFunctionLoader in 'Sources\Machine\Cache\uFunctionLoader.pas',
  uFuncUDP in 'Sources\Functions\SDL\UDP\uFuncUDP.pas',
  uFuncUDPBind in 'Sources\Functions\SDL\UDP\uFuncUDPBind.pas',
  uFuncUDPBroadcast in 'Sources\Functions\SDL\UDP\uFuncUDPBroadcast.pas',
  uFuncUDPClose in 'Sources\Functions\SDL\UDP\uFuncUDPClose.pas',
  uFuncUDPReceive in 'Sources\Functions\SDL\UDP\uFuncUDPReceive.pas',
  uFuncUDPSend in 'Sources\Functions\SDL\UDP\uFuncUDPSend.pas',
  uIdentifierRegistry in 'Sources\uIdentifierRegistry.pas',
  uInstruction in 'Sources\Instructions\uInstruction.pas',
  uInstructionAssign in 'Sources\Instructions\uInstructionAssign.pas',
  uInstructionBind in 'Sources\Instructions\uInstructionBind.pas',
  uInstructionControl in 'Sources\Instructions\uInstructionControl.pas',
  uInstructionEach in 'Sources\Instructions\uInstructionEach.pas',
  uInstructionExecute in 'Sources\Instructions\uInstructionExecute.pas',
  uInstructionIf in 'Sources\Instructions\uInstructionIf.pas',
  uInstructionLog in 'Sources\Instructions\uInstructionLog.pas',
  uInstructionThrow in 'Sources\Instructions\uInstructionThrow.pas',
  uInstructionTry in 'Sources\Instructions\uInstructionTry.pas',
  uInstructionWhile in 'Sources\Instructions\uInstructionWhile.pas',
  uMachine in 'Sources\Machine\uMachine.pas',
  uOp in 'Sources\Machine\Ops\uOp.pas',
  uOpAssign in 'Sources\Machine\Ops\uOpAssign.pas',
  uOpBind in 'Sources\Machine\Ops\uOpBind.pas',
  uOpBinary in 'Sources\Machine\Ops\uOpBinary.pas',
  uOpCache in 'Sources\Machine\Cache\uOpCache.pas',
  uOpCall in 'Sources\Machine\Ops\uOpCall.pas',
  uOpCatch in 'Sources\Machine\Ops\uOpCatch.pas',
  uOpCheck in 'Sources\Machine\Ops\uOpCheck.pas',
  uOpCode in 'Sources\Machine\Cache\uOpCode.pas',
  uOperations in 'Sources\Value\uOperations.pas',
  uOpField in 'Sources\Machine\Ops\uOpField.pas',
  uOpFunc in 'Sources\Machine\Ops\uOpFunc.pas',
  uOpItem in 'Sources\Machine\Ops\uOpItem.pas',
  uOpJit in 'Sources\Machine\Ops\uOpJit.pas',
  uOpJmp in 'Sources\Machine\Ops\uOpJmp.pas',
  uOpLoader in 'Sources\Machine\Cache\uOpLoader.pas',
  uOpTail in 'Sources\Machine\Ops\uOpTail.pas',
  uOpPop in 'Sources\Machine\Ops\uOpPop.pas',
  uOpRange in 'Sources\Machine\Ops\uOpRange.pas',
  uOpReturn in 'Sources\Machine\Ops\uOpReturn.pas',
  uOpSplit in 'Sources\Machine\Ops\uOpSplit.pas',
  uOpStack in 'Sources\Machine\Ops\uOpStack.pas',
  uOpCast in 'Sources\Machine\Ops\uOpCast.pas',
  uOpThrow in 'Sources\Machine\Ops\uOpThrow.pas',
  uOpTry in 'Sources\Machine\Ops\uOpTry.pas',
  uOpUnary in 'Sources\Machine\Ops\uOpUnary.pas',
  uOpVal in 'Sources\Machine\Ops\uOpVal.pas',
  uOpVar in 'Sources\Machine\Ops\uOpVar.pas',
  uParserLogic in 'Sources\Parser\uParserLogic.pas',
  uQueue in 'Sources\Common\uQueue.pas',
  uRepositoryFactory in 'Sources\Units\Repositories\uRepositoryFactory.pas',
  uResolver in 'Sources\Builders\uResolver.pas',
  uSectionHolder in 'Sources\Machine\uSectionHolder.pas',
  uSocketRepository in 'Sources\Units\Repositories\uSocketRepository.pas',
  uSourceParser in 'Sources\Parser\uSourceParser.pas',
  uStack in 'Sources\Common\uStack.pas',
  uStandardFunc in 'Sources\Functions\uStandardFunc.pas',
  uState in 'Sources\Machine\uState.pas',
  uStreamRepository in 'Sources\Units\Repositories\uStreamRepository.pas',
  uStringSet in 'Sources\Common\uStringSet.pas',
  uTreeAssembler in 'Sources\Builders\uTreeAssembler.pas',
  uType in 'Sources\Value\Types\uType.pas',
  uTypeAny in 'Sources\Value\Types\uTypeAny.pas',
  uTypeBool in 'Sources\Value\Types\uTypeBool.pas',
  uTypeBuilder in 'Sources\Builders\uTypeBuilder.pas',
  uTypeFactory in 'Sources\Value\uTypeFactory.pas',
  uTypeFloat in 'Sources\Value\Types\uTypeFloat.pas',
  uTypeFunc in 'Sources\Value\Types\uTypeFunc.pas',
  uTypeInt in 'Sources\Value\Types\uTypeInt.pas',
  uTypeSet in 'Sources\Value\Types\uTypeSet.pas',
  uTypeString in 'Sources\Value\Types\uTypeString.pas',
  uTypeStruct in 'Sources\Value\Types\uTypeStruct.pas',
  uUnit in 'Sources\Units\uUnit.pas',
  uUnitBit in 'Sources\Units\SDL\uUnitBit.pas',
  uUnitBuilder in 'Sources\Builders\uUnitBuilder.pas',
  uUnitDate in 'Sources\Units\SDL\uUnitDate.pas',
  uUnitFile in 'Sources\Units\SDL\uUnitFile.pas',
  uUnitFS in 'Sources\Units\SDL\uUnitFS.pas',
  uUnitIO in 'Sources\Units\SDL\uUnitIO.pas',
  uUnitLibrary in 'Sources\Units\uUnitLibrary.pas',
  uUnitMath in 'Sources\Units\SDL\uUnitMath.pas',
  uUnitOS in 'Sources\Units\SDL\uUnitOS.pas',
  uUnitRex in 'Sources\Units\SDL\uUnitRex.pas',
  uUnitStr in 'Sources\Units\SDL\uUnitStr.pas',
  uUnitTCP in 'Sources\Units\SDL\uUnitTCP.pas',
  uUnitUDP in 'Sources\Units\SDL\uUnitUDP.pas',
  uValue in 'Sources\Value\uValue.pas',
  uInstructionBlock in 'Sources\Instructions\uInstructionBlock.pas',
  uTypeVoid in 'Sources\Value\Types\uTypeVoid.pas',
  uInstructionWith in 'Sources\Instructions\uInstructionWith.pas',
  uRunner in 'Sources\Machine\uRunner.pas',
  uOpRun in 'Sources\Machine\Ops\uOpRun.pas',
  uInstructionSync in 'Sources\Instructions\uInstructionSync.pas',
  uOpSync in 'Sources\Machine\Ops\uOpSync.pas',
  uDataFunc in 'Sources\Value\Datas\uDataFunc.pas',
  uTypePipe in 'Sources\Value\Types\uTypePipe.pas',
  uTypeList in 'Sources\Value\Types\uTypeList.pas',
  uExpressionListItem in 'Sources\Expressions\uExpressionListItem.pas',
  uOpLog in 'Sources\Machine\Ops\uOpLog.pas',
  uExpressionCast in 'Sources\Expressions\uExpressionCast.pas';

procedure PrintHelp;
begin
  Writeln('Tengwar Compiler ' + APP_VERSION);
  Writeln('by Taras "Dr.Wolf" Supyk');
  Writeln;
  Writeln('Usage: ' + ExtractFileName(ParamStr(0)) + ' [--param=value] <script>');
  Writeln('  --libpath=<path>: comma or semicolon separated list of pathes with units for include');
  Writeln('  --out=<path>: output filename, script will be executed if no output specified');
  Writeln('  --log: print bytecode listing');
  Writeln('  --help: print help');
end;

var
  Config: TConfig;
  Repositories: TRepositoryFactory;
  UnitLibrary: TUnitLibrary;
  OpCache: TOpCache;
  Machine: TMachine;

begin
  FormatSettings.DecimalSeparator := '.';

  Config := TConfig.Create([cpLibPath, cpOutput, cpLog, cpHelp]);
  if Config.Help then
    PrintHelp
  else
  begin
    Repositories := TRepositoryFactory.Create;
    UnitLibrary := TUnitLibrary.Create(Repositories, Config);
    Machine := TMachine.Create;
    try
      try
        UnitLibrary.Load(Config.Script, Machine).Initialize(Machine);
        Writeln(IntToStr(UnitLibrary.LinesTotal) + ' lines processed');
        if Config.Output = '' then
        begin
          if Config.Log then
          begin
            Machine.Log(ChangeFileExt(Config.Script, ''));
            Writeln('Bytecode logged to ' + ChangeFileExt(Config.Script, '.log'));
          end;
          Machine.Run
        end else begin
          if Config.Log then
          begin
            Machine.Log(Config.Output);
            Writeln('Bytecode logged to ' + Config.Output + '.log');
          end;
          OpCache := TOpCache.Create(Repositories);
          try
            Machine.Serialize(OpCache);
            OpCache.SaveToFile(Config.Output);
            Writeln(Format('%d bytes code, %d bytes data', [OpCache.OpStream.Size, OpCache.DataSize]));
            Writeln('Compiled ' + Config.Output);
          finally
            OpCache.Free;
          end;
        end;
      except
        on e: ECodeException do
          Writeln(e.Message);
        on e: ERuntimeException do
          Writeln('RUNTIME ERROR: ' + e.Message);
        on e: Exception do
          Writeln('UNEXPECTED ERROR: ' + e.Message);
      end;
    finally
      Machine.Free;
      UnitLibrary.Free;
      Repositories.Free;
    end;
  end;
  Config.Free;
end.
