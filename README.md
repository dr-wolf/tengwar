# Tengwar
Tengwar is simple all-purpose programming language. It is opensource and written in Pascal. It was designed for no reasons and can be used for education purposes or just for fun.

## Building

Ensure you have fpc 3.0.0 or higher on your PATH and check out build.sh or build.cmd. Also you can build Tengwar using Delphi 2010 or later.

## Running 

Run it using next command:

    tengwar [--lib-path=some/path] script.tw

Where script.tw is the name of the script you want to run and `--lib-path` is a list of pathes where global units can be found, separated with a `;` (windows) or `:` (linux).

## Licensing

Tengwar is distributed under GNU GPL v3 License, see [LICENSE]

## Thirdparty

Tengwar uses next third-party components, placed in the folder `Lib`. They are distributed under their own licenses.

1. Synapse library, ARARAT.
2. RegExpr, Copyright (c) 1999-2004 Andrey V. Sorokin, St.Petersburg, Russia 
